﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;

namespace Mobile.Models
{
    public class OrderModel
    {
        private string userId;
        public string UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        private string orderId;
        public string OrderId
        {
            get { return orderId; }
            set { orderId = value; }
        }

        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private string sendPerson;
        public string SendPerson
        {
            get { return sendPerson; }
            set { sendPerson = value; }
        }
        private string sendAddress;

        public string SendAddress
        {
            get { return sendAddress; }
            set { sendAddress = value; }
        }

        private string sendCity;

        public string SendCity
        {
            get { return sendCity; }
            set { sendCity = value; }
        }

        private string sendPhone;

        public string SendPhone
        {
            get { return sendPhone; }
            set { sendPhone = value; }
        }
        private string receivePerson;

        public string ReceivePerson
        {
            get { return receivePerson; }
            set { receivePerson = value; }
        }
        private string receiveAddress;

        public string ReceiveAddress
        {
            get { return receiveAddress; }
            set { receiveAddress = value; }
        }

        private string receiveCity;

        public string ReceiveCity
        {
            get { return receiveCity; }
            set { receiveCity = value; }
        }

        private string receivePhone;

        public string ReceivePhone
        {
            get { return receivePhone; }
            set { receivePhone = value; }
        }

        private string productName;

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

        private string sendTime;

        public string SendTime
        {
            get { return sendTime; }
            set { sendTime = value; }
        }

        private string openId;

        public string OpenId
        {
            get { return openId; }
            set { openId = value; }
        }

        private long routeId;

        public long RouteId
        {
            get { return routeId; }
            set { routeId = value; }
        }
        private int from;
        public int From
        {
            get { return from; }
            set { from = value; }
        }
        private string driver;

        public string Driver
        {
            get { return driver; }
            set { driver = value; }
        }

        private NameValueCollection derivrs = new NameValueCollection();
        public NameValueCollection Drivers
        {
            get { return derivrs; }
            set { derivrs = value; }
        }

        private int userSex = 1;
        public int UserSex
        {
            get { return userSex; }
            set { userSex = value; }
        }

        public bool HasUser
        {
            get { return !string.IsNullOrEmpty(phone); }
        }

        private string remark = string.Empty;
        public string Remark
        {
            get { return remark; }
            set { remark = value; }
        }

        public int State { set; get; }

        public string OrderNumber { set; get; }

        public string StateName
        {
            get
            {
                switch (State)
                {
                    case 0:
                        return "新订单";
                    case 1:
                        return "已接受";
                    case 2:
                        return "选定车主";
                    case 3:
                        return "车主已接货";
                    case 4:
                        return "发货中";
                    case 5:
                        return "交易完成";
                    case -6:
                        return "取消";
                    case -7:
                        return "失效";
                    default:
                        return "未知";
                }
            }
            set{}
        }
    }


    public class ProcessResult
    {
        public string OrderId { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class OrderInfo
    {
        public string OrderId { get; set; }
    }



}