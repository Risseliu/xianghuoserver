﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;

namespace Mobile.Models
{
    /// <summary>
    /// 司机基本信息表
    /// </summary>
    public class Driver
    {
        public string ID { get; set; }
        public string UserName { get; set; }
        public int Gender { get; set; }
        public string PhoneNumber { get; set; }

    }
}