﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Models
{
    public class Route
    {
        public Route()
        {
            CreateTime = DateTime.Now;
        }

        public long Id { get; set; }

        public string Start { get; set; }

        public string End { get; set; }

        public string City { get; set; }

        public DateTime CreateTime { get; set; }
    }
}