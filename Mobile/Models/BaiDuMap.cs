﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Models
{
    public class BaiDuMap
    {
       
       
    }
    public class BaiDuAddress
    {
        public string name { set; get; }
        public string city { set; get; }
        public string district { set; get; }
        public string business { set; get; }
        public string cityid { set; get; }
    }
    public class BaiduData
    {
        /// <summary>
        /// 请求返回状态
        /// </summary>
        public int status { set; get; }
        /// <summary>
        /// 请求结果
        /// </summary>
        public string message { set; get; }
        public List<BaiDuAddress> result { set; get; }
    }
}