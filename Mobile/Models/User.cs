﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Mobile.Models
{
    public class User
    {
        public User()
        {
            ID = Guid.NewGuid();
            JoinDate = DateTime.Now;
        }
        [Key]
        public Guid ID { get; set; }

        [StringLength(200)]
        public string UserName { get; set; }

        public int UserNumber { get; set; }

        /// <summary>
        ///短信的验证码
        /// </summary>
        public string MessageValidCode { get; set; }

        [StringLength(200)]

        public string PhoneNumber { get; set; }

        [StringLength(200)]
        public string PhoneID { get; set; }

        [StringLength(200)]
        public string Account { get; set; }

        [StringLength(200)]
        public string RegisterAddress { get; set; }

        [StringLength(200)]
        public string RealAddress { get; set; }
        /// <summary>
        /// 1开通货主,2开通车主,3开通货主和车主
        /// </summary>
        public int UserState { get; set; }

        [StringLength(200)]
        public string Password { get; set; }

        /// <summary>
        /// 人员锁定,0未锁定,1已锁定
        /// </summary>
        public int Locked { get; set; }

        [StringLength(200)]
        public string PKey { get; set; }

        public DateTime? PKeyOverDate { get; set; }

        /// <summary>
        /// Varchar(200) ImagePath,头像图片地址
        /// </summary>
        [StringLength(200)]
        public string ImagePath { get; set; }

        /// <summary>
        /// Int Gender性别,(1男,2女)
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// 身份证是否上传
        /// </summary>
        public int ShenFenZhengUpload { get; set; }
        /// <summary>
        /// 用户的订单报价用于车主列表接口使用
        /// </summary>
        [NotMapped]
        public int OrderQuotation { get; set; }
        /// <summary>
        /// 百度推送UserID
        /// </summary>
        [StringLength(200)]
        public string B_UserID { get; set; }

        /// <summary>
        /// 百度推送Channel
        /// </summary>
        [StringLength(200)]
        public string B_Channel { get; set; }

        /// <summary>
        /// 微信OpenID
        /// </summary>
        [StringLength(200)]
        public string OpenID { get; set; }

        public DateTime? JoinDate { get; set; }
        public virtual Driver DriverInfo { get; set; }

        public virtual Customer CustomerInfo { get; set; }
    }

    /// <summary>
    ///     货主信息表
    /// </summary>
    public class Customer
    {
        public Customer()
        {
            ID = Guid.NewGuid();
        }

        [Key, ForeignKey("UserInfo")]
        public Guid ID { get; set; }

        public int Valid { get; set; }

        public Guid UserID { get; set; }
        /// <summary>
        /// 身份证是否上传
        /// </summary>
        public int ShenFenZhengUpload { get; set; }

        [StringLength(200)]
        public string ShenFenZheng { get; set; }

        [StringLength(500)]
        public string Remark { get; set; }


        /// <summary>
        /// 是否提交验证
        /// </summary>
        public int ValidSubmit { get; set; }

        public DateTime? ValidSubmitDateTime { get; set; }

        [ForeignKey("UserID")]
        public virtual User UserInfo { get; set; }
    }

    public class LoninResult
    {
        public int Success { get; set; }//1成功,2需要再次输入验证码,3失败
        public string PK { get; set; }//私钥
        public string PKOverDate { get; set; }//eg:2008-5-5 23:22:10 私钥过期时间,过期后需要重新登陆

        /// <summary>
        /// 1开通货主,2开通车主,3开通货主和车主
        /// </summary>
        public int UserState { get; set; }

        /// <summary>
        /// 0一般登陆,1自动注册登陆
        /// </summary>
        public int Register { get; set; }
        public String UserID { get; set; }
    }

     /// <summary>
    /// 返回的结果
    /// </summary>
    public class Result
    {
        public string ResultData { get; set; }
        public string ErrorMessage { get; set; }
        public bool Success { get; set; }
        /// <summary>
        /// 100-199登陆问题
        /// 201非法操作
        /// 202收货时,验证码不对
        /// 注册时:ErrorCode=205手机号已经注册,206验证码错误
        /// </summary>
        public int ErrorCode { get; set; }
    }
}