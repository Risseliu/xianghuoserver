﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Models
{
    public class OrderListModel
    {
        private List<OrderModel> _orderList = new List<OrderModel>();

        public List<OrderModel> OrderList { get; set; }
    }
}