﻿using Mobile.Controllers;
using Mobile.Helper;
using Mobile.Service;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Configuration;

namespace Mobile
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        private Logger logger = LogManager.GetCurrentClassLogger();
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();


            PublicVariable.APIURL = ConfigurationManager.AppSettings["APIURL"];
            PublicVariable.CONNECTIONSTR = ConfigurationManager.AppSettings["Connection"];
            //InitCache();

        }

        private void InitCache()
        {
          
            var service = new OrderService();
            IDictionary<string, string> parameter = new Dictionary<string, string>();
            parameter.Add("userName", string.Empty);
            var derives = service.GetDrivers(parameter);
            CacheManager.Add(PublicVariable.DERIVERKEY,
               new CacheItem() { ExperiedTimes = PublicVariable.DERIVEREXPERIEDSECONDS, Vaues = derives });
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var httpContext = ((MvcApplication)sender).Context;

            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));
            var currentController = " ";
            var currentAction = " ";

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }
            var ex = Server.GetLastError();

            logger.Log(LogLevel.Error, " Application_error: ", ex);

            var controller = new ErrorController();
            var routeData = new RouteData();
            var action = "Index";
            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
            httpContext.Response.TrySkipIisCustomErrors = true;
            routeData.Values["controller"] = "Error";
            routeData.Values["action"] = action;
            controller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
            try
            {
                ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
            }
            catch (Exception ex2)
            {
                logger.Log(LogLevel.Error, " Application_error (recursive): ", ex2);
            }
        }
    }
}