﻿using Mobile.Helper;
using Mobile.Models;
using Mobile.Service;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Mobile.Controllers
{
    public class OrderController : Controller
    {
        private string appid = ConfigurationManager.AppSettings["Appid"];
        private string appsecret = ConfigurationManager.AppSettings["Appsecret"];
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public ActionResult Index()
        {
            var model = new OrderModel();
            logger.Info("开始从请求中获取Code");
            if (!string.IsNullOrEmpty(Request.QueryString["code"]))
            {
                var code = Request.QueryString["code"].ToString();
                logger.Info("获取的Code值:" + Request.QueryString["code"].ToString());

                //获得Token  
                var token = GetToken(code);
                logger.Info("获取的OpenId值:" + token.openid);
                model.OpenId = token.openid;
                Session["OrderModel"] = model;
                if (!string.IsNullOrEmpty(model.OpenId))
                {
                    var user = new OrderService().GetUserInfoFromDB(model.OpenId);
                    if (string.IsNullOrEmpty(user.PhoneNumber))
                    {
                        return RedirectToAction("Register");
                    }
                    model.Phone = user.PhoneNumber;
                    model.SendPerson = user.UserName;
                    model.UserSex = user.Gender;
                }

                return View(model);
            }
            else
            {
                model = Session["OrderModel"] as OrderModel;
                if (model == null || string.IsNullOrEmpty(model.OpenId))
                {
                    return RedirectToAction("NoLogin");
                }

                return View(model);
            }

        }

        public ActionResult NoLogin()
        {
            return View();
        }

        public ActionResult List()
        {
            var model = new OrderModel();
            OrderListModel orderlist = new OrderListModel();
            logger.Info("开始从请求中获取Code");
            if (!string.IsNullOrEmpty(Request.QueryString["code"]))
            {
                var code = Request.QueryString["code"].ToString();
                logger.Info("获取的Code值:" + Request.QueryString["code"].ToString());

                //获得Token  
                var token = GetToken(code);
                logger.Info("获取的OpenId值:" + token.openid);
                model.OpenId = token.openid;
                Session["OrderModel"] = model;
                if (!string.IsNullOrEmpty(model.OpenId))
                {
                    var user = new OrderService().GetUserInfoFromDB(model.OpenId);
                    if (string.IsNullOrEmpty(user.PhoneNumber))
                    {
                        return RedirectToAction("Register");
                    }

                    orderlist.OrderList = new OrderService().GetUserOrders(user.PhoneNumber);
                   
                }
            }
            return View(orderlist);
        }

        public ActionResult Register()
        {
            OrderModel model = Session["OrderModel"] as OrderModel;
            if (model == null || string.IsNullOrEmpty(model.OpenId))
            {
                return RedirectToAction("NoLogin");
            }
            return View();
        }

        public ActionResult DeleteOrder(string orderId)
        {
            logger.Info("开始删除订单");
            var sucessed = "true";
            try
            {
                var model = Session["OrderModel"] as OrderModel;
                string OpenId = model.OpenId;
                var service = new OrderService();
                service.DeleteOrder(orderId);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                sucessed = "false";
            }
            return Json(new { Sucess = sucessed }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProcessRegister(string Phone, string UserName, int Gender)
        {
            logger.Info("开始添加用户");
            var sucessed = "true";
            try
            {
                var model = Session["OrderModel"] as OrderModel;
                string OpenId = model.OpenId;
                var service = new OrderService();
                var result = service.Register(Phone, UserName, Gender.ToString(), OpenId);
                logger.Info("添加用户: ErrorCode:" + result.ErrorCode + "ErrorMessage:" + result.ErrorMessage);
                if (!string.IsNullOrEmpty(result.ErrorCode) && result.ErrorCode != "0")
                {
                    sucessed = "false";

                }
                else
                {
                    if (model != null)
                    {
                        model.Phone = Phone;
                        model.SendPerson = UserName;
                        model.UserSex = Gender;
                        Session["OrderModel"] = model;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                sucessed = "false";
            }
            return Json(new { Sucess = sucessed }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 发送 API 请求并返回方案信息。
        /// </summary>
        /// <returns></returns>
        public ActionResult RequestRoute(string q)
        {
            if (!string.IsNullOrEmpty(q))
            {
                var service = new OrderService();
                var routes = service.GetRoutesFromDB(q);

                if (routes.Any())
                {
                    var s = (from r in routes select r.Start).Distinct();
                    var newList = new List<Route>();
                    foreach (var str in s)
                    {
                        newList.Add(new Route() { Id = 0, Start = str });
                    }

                    return new JsonResult { Data = newList, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }

            var d = new Route { Id = 0, Start = "未找到地址", End = "未找到地址", };
            var list = new List<Route>();
            list.Add(d);
            return new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }

        /// <summary>
        /// 发送 API 请求并返回方案信息。
        /// </summary>
        /// <returns></returns>
        public ActionResult RequestEndByStart(string start)
        {
            if (!string.IsNullOrEmpty(start))
            {
                var service = new OrderService();
                var routes = service.GetRoutesFromDBByStart(start);

                if (routes.Any())
                {
                    return new JsonResult { Data = routes, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }

            var d = new Route { Id = 0, Start = "未找到地址", End = "未找到地址", };
            var list = new List<Route>();
            list.Add(d);
            return new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }

        public ActionResult Process(OrderModel model)
        {
            logger.Info("开始处理订单，openId:" + model.OpenId);
            var sucessed = "true";
            string orderId = string.Empty;
            try
            {
                var oldModel = Session["OrderModel"] as OrderModel;
                model.UserSex = oldModel.UserSex;
                model.OpenId = oldModel.OpenId;
                model.Phone = oldModel.Phone;
                model.SendPerson = oldModel.SendPerson;
                model.From = oldModel.From;
                var service = new OrderService();
                var result = service.SaveOrder(model);
                orderId = result.OrderId;
                logger.Info("订单处理结果: ErrorCode:" + result.ErrorCode + "ErrorMessage:" + result.ErrorMessage);
                if (!string.IsNullOrEmpty(result.ErrorCode) && result.ErrorCode != "0")
                {
                    sucessed = "false";

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                sucessed = "false";
            }
            return Json(new { Sucess = sucessed, OrderId = orderId }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RequestDrivers(string q)
        {

            if (!string.IsNullOrEmpty(q))
            {
                var service = new OrderService();
                var drivers = service.GetDriversFromDB(q);

                if (drivers.Any())
                {
                    return new JsonResult { Data = drivers, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }

            var d = new Driver { ID = "", UserName = "抱歉，未找到您要查询的货主", PhoneNumber = "" };
            var list = new List<Driver>();
            list.Add(d);
            return new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        private List<Driver> InitDrivers(string userName)
        {
            var service = new OrderService();
            var drivers = service.GetDriversFromDB(userName);
            CacheManager.Add(PublicVariable.DERIVERKEY,
            new CacheItem() { ExperiedTimes = PublicVariable.DERIVEREXPERIEDSECONDS, Vaues = drivers });
            return drivers;
        }


        //获得Token  
        private OAuthToken GetToken(string code)
        {
            var str = GetJson("https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + appsecret + "&code=" + code + "&grant_type=authorization_code");
            return JsonConvert.DeserializeObject<OAuthToken>(str);
        }
        //刷新Token  
        private OAuthToken RefreshToken(string refreshToken)
        {
            var str = GetJson("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + appid + "&grant_type=refresh_token&refresh_token=" + refreshToken);
            return JsonConvert.DeserializeObject<OAuthToken>(str);
        }
        //获得用户信息  
        private OAuthUser GetUserInfo(string refreshToken, string openId)
        {
            var str = GetJson("https://api.weixin.qq.com/sns/userinfo?access_token=" + refreshToken + "&openid=" + openId);
            return JsonConvert.DeserializeObject<OAuthUser>(str);
        }
        private string GetJson(string url)
        {
            var wc = new WebClient();
            wc.Credentials = CredentialCache.DefaultCredentials;
            wc.Encoding = Encoding.UTF8;
            string returnText = wc.DownloadString(url);

            if (returnText.Contains("errcode"))
            {
                //可能发生错误  
            }

            return returnText;
        }


        /// <summary>
        /// 发送 API 请求并返回方案信息。
        /// </summary>
        /// <returns></returns>
        public ActionResult RequestBaiduApi(string q, int limit, long timestamp)
        {
            var param = new Dictionary<string, string>();
            param.Add("q", q);
            param.Add("limit", "0");
            param.Add("timestamp", "0");
            var service = new OrderService();
            var result = service.RequestMapData(param);
            JsonResult jsonResult = null;

            if (!string.IsNullOrEmpty(result))
            {
                jsonResult = new JsonResult { Data = result, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
            {
                BaiDuAddress address = new BaiDuAddress { city = "", name = "抱歉，未找到您要查询的地址", district = "" };
                List<BaiDuAddress> list = new List<BaiDuAddress>();
                list.Add(address);
                jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

            return jsonResult;
        }
    }
}
