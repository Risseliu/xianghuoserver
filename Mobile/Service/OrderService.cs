﻿using Mobile.Helper;
using Mobile.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Mobile.Service
{
    public class OrderService
    {
        public User GetUserInfoFromDB(string openId)
        {
            var connection = new SqlConnection(PublicVariable.CONNECTIONSTR);
            var sql = @"Select ID,UserName, PhoneNumber,Gender From [Users] u WHERE u.OpenId=@OpenId";
            var command = new SqlCommand(sql, connection);
            var parameter = new SqlParameter("@OpenId", SqlDbType.NVarChar);
            parameter.Value = openId;
            command.Parameters.Add(parameter);
            var user = new User();
            try
            {
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    user.ID = Guid.Parse(dt.Rows[0]["ID"].ToString());
                    user.UserName = dt.Rows[0]["UserName"].ToString();
                    user.PhoneNumber = dt.Rows[0]["PhoneNumber"].ToString();
                }
            }
            catch
            { }
            return user;
        }

        public List<OrderModel> GetUserOrders(string guestPhoneNumber)
        {
            var connection = new SqlConnection(PublicVariable.CONNECTIONSTR);
            var sql = "  Select ID, OrderNumber, startName, EndName,PublishTime,OrderState From [Orders] Where [GuestUserPhoneNumber]=@guestPhoneNumber  And Orderstate >=0 And Orderstate <6";
            var command = new SqlCommand(sql, connection);
            var parameter = new SqlParameter("@guestPhoneNumber", SqlDbType.NVarChar);
            parameter.Value = guestPhoneNumber;
            command.Parameters.Add(parameter);
            var orders = new List<OrderModel>();
            try
            {
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        var order = new OrderModel();
                        order.OrderId = row["ID"].ToString();
                        order.SendAddress = row["StartName"].ToString();
                        order.ReceiveAddress = row["EndName"].ToString();
                        order.SendTime = row["PublishTime"].ToString();
                        order.State = System.Convert.ToInt32(row["OrderState"]);
                        order.OrderNumber = row["OrderNumber"].ToString();
                        orders.Add(order);
                    }
                }
            }
            catch
            { }
            return orders;
        }

        public User GetUserInfo(string openId)
        {
            var url = Path.Combine(PublicVariable.APIURL, "Customer/GetCustomer");
            var parameters = new Dictionary<string, string>();
            parameters.Add("openId", openId);
            url = BuildParameter(parameters, url);
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";
            req.KeepAlive = true;
            req.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
            HttpWebResponse rsp = null;
            try
            {
                rsp = (HttpWebResponse)req.GetResponse();
            }
            catch (Exception ex)
            {
                rsp = null;
            }

            if (rsp != null && rsp.CharacterSet != null)
            {
                Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
                var str = GetResponseAsString(rsp, encoding);
                var result = JsonConvert.DeserializeObject<Result>(str);
                if (result.Success)
                {
                    return JsonConvert.DeserializeObject<User>(result.ResultData);
                }
            }
            return new User();
        }

        public List<Driver> GetDriversFromDB(string userName)
        {
            var drivers = new List<Driver>();
            var connection = new SqlConnection(PublicVariable.CONNECTIONSTR);
            var sql = @"Select d.ID,u.[UserName],u.[PhoneNumber], u.Gender from [Drivers] d
                       join   [Users] u  on d.[ID]=u.[ID]  AND d.Valid=1 and u.UserName like '%{0}%'";
            sql = string.Format(sql, userName);
            var command = new SqlCommand(sql, connection);
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    drivers.Add(new Driver
                    {
                        ID = row["ID"].ToString(),
                        UserName = row["UserName"].ToString(),
                        PhoneNumber = row["PhoneNumber"].ToString(),
                        Gender = System.Convert.ToInt32(row["Gender"])
                    });
                }
            }

            return drivers;
        }

        public List<Route> GetRoutesFromDB(string key)
        {
            var routes = new List<Route>();
            var connection = new SqlConnection(PublicVariable.CONNECTIONSTR);
            var sql = @"Select * from [Routes] r where r.Start like @Start";
            var command = new SqlCommand(sql, connection);
            command.Parameters.Add(new SqlParameter("@Start", "%" + key + "%"));
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    routes.Add(new Route
                    {
                        Id = System.Convert.ToInt64(row["Id"]),
                        Start = row["Start"].ToString(),
                        End = row["End"].ToString()
                    });
                }
            }

            return routes;
        }

        public List<Route> GetRoutesFromDBByStart(string start)
        {
            var routes = new List<Route>();
            var connection = new SqlConnection(PublicVariable.CONNECTIONSTR);
            var sql = @"Select * from [Routes] r where r.Start = @Start";
            var command = new SqlCommand(sql, connection);
            command.Parameters.Add(new SqlParameter("@Start", start));
            var da = new SqlDataAdapter(command);
            var dt = new DataTable();
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    routes.Add(new Route
                    {
                        Id = System.Convert.ToInt64(row["Id"]),
                        Start = row["Start"].ToString(),
                        End = row["End"].ToString()
                    });
                }
            }

            return routes;
        }

        public List<Driver> GetDrivers(IDictionary<string, string> parameters)
        {
            var url = Path.Combine(PublicVariable.APIURL, "Customer/RequestDrivers");
            url = BuildParameter(parameters, url);
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";
            req.KeepAlive = true;
            req.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
            HttpWebResponse rsp = null;
            try
            {
                rsp = (HttpWebResponse)req.GetResponse();
            }
            catch (Exception ex)
            {
                rsp = null;
            }

            if (rsp != null && rsp.CharacterSet != null)
            {
                Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
                var str = GetResponseAsString(rsp, encoding);
                var result = JsonConvert.DeserializeObject<Result>(str);
                if (result.Success)
                {
                    return JsonConvert.DeserializeObject<List<Driver>>(result.ResultData);
                }
            }
            return new List<Driver>();
            //return null;
        }

        public void DeleteOrder(string orderId)
        {
            var routes = new List<Route>();
            var connection = new SqlConnection(PublicVariable.CONNECTIONSTR);
            var sql = @"Delete from Order where o.ID = @ID";
            var command = new SqlCommand(sql, connection);
            var parameter = new SqlParameter("@ID", SqlDbType.UniqueIdentifier);
            parameter.Value = new Guid(orderId);
            command.Parameters.Add(parameter);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();

          
        }

        public ProcessResult Register(string phoneNumber, string userName,string gender,string openId)
        {
            var url = Path.Combine(PublicVariable.APIURL, "Customer/CheckCustomer");
            var param = new Dictionary<string, string>();
            param.Add("phoneNumber", phoneNumber);
            param.Add("userName", userName);
            param.Add("userSex", gender);
            param.Add("defaultAdd", "1");
            param.Add("openId", openId);
            url = BuildParameter(param, url);
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";
            req.KeepAlive = true;
            req.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
            HttpWebResponse rsp = null;
            try
            {
                rsp = (HttpWebResponse)req.GetResponse();
            }
            catch (Exception ex)
            {
                return new ProcessResult { ErrorCode = "Unknow", ErrorMessage = ex.Message };
            }

            if (rsp != null && rsp.CharacterSet != null)
            {
                Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
                var str = GetResponseAsString(rsp, encoding);
                try
                {
                    var result = JsonConvert.DeserializeObject<Result>(str);
                    if (result.Success)
                    {
                        return new ProcessResult { ErrorCode ="", ErrorMessage = result.ErrorMessage };
                    }
                    else
                    {
                        return new ProcessResult { ErrorCode = result.ErrorCode.ToString(), ErrorMessage = result.ErrorMessage };
                    }

                }
                catch (Exception ex)
                {
                    return new ProcessResult { ErrorCode = "Unknow", ErrorMessage = ex.Message };
                }

            }
            return new ProcessResult { ErrorCode = "Unknow", ErrorMessage = "请求添加用户失败" };

        }

        public ProcessResult SaveOrder(OrderModel model)
        {
            string url = Path.Combine(PublicVariable.APIURL, "Customer/ServerAddOrder");
            var param = new Dictionary<string, string>();
            param.Add("phoneNumber", model.Phone);
            param.Add("routeId", model.RouteId.ToString());
            param.Add("goodsName", model.ProductName);
            param.Add("sendDate", model.SendTime);
            param.Add("isMianyi", "1");
            param.Add("defaultComplete", "0");
            param.Add("goodsDriverId", model.Driver);
            param.Add("goodsMemo", "---来自微信客户端订单---"+model.Remark);
            param.Add("goodsPrice", "");
            param.Add("From",model.From+"");
            url = BuildParameter(param, url);
            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";
            req.KeepAlive = true;
            req.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
            HttpWebResponse rsp = null;
            try
            {
                rsp = (HttpWebResponse)req.GetResponse();
            }
            catch (Exception ex)
            {
                return new ProcessResult { ErrorCode = "Unknow", ErrorMessage = ex.Message };
            }

            if (rsp != null && rsp.CharacterSet != null)
            {
                Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
                var str = GetResponseAsString(rsp, encoding);
                try
                {
                    var result = JsonConvert.DeserializeObject<Result>(str);
                    if (result.Success)
                    {
                        var lr = JsonConvert.DeserializeObject<OrderInfo>(result.ResultData);
                        
                        return new ProcessResult { ErrorCode = result.ErrorCode.ToString(), OrderId=lr.OrderId, ErrorMessage = result.ErrorMessage };
                    }
                   
                }
                catch (Exception ex)
                {
                    return new ProcessResult { ErrorCode = "Unknow", ErrorMessage = ex.Message };
                }

            }

            return new ProcessResult { ErrorCode = "Unknow", ErrorMessage = "请求添加订单失败" };
        }

        public string RequestMapData(IDictionary<string, string> parameters)
        {
            var url = Path.Combine(PublicVariable.APIURL, "Customer/RequestBaiduApi");

            url = BuildParameter(parameters, url);

            var req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";
            req.KeepAlive = true;
            req.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
            HttpWebResponse rsp = null;
            try
            {
                rsp = (HttpWebResponse)req.GetResponse();
            }
            catch (Exception ex)
            {
                rsp = null;
            }

            if (rsp != null && rsp.CharacterSet != null)
            {
                Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
                return GetResponseAsString(rsp, encoding);
            }
            return string.Empty;
        }

        private static string BuildParameter(IDictionary<string, string> parameters, string url)
        {
            if (parameters != null && parameters.Count > 0)
            {
                if (url.Contains("?"))
                {
                    url = url + "&" + BuildPostData(parameters);
                }
                else
                {
                    url = url + "?" + BuildPostData(parameters);
                }
            }
            return url;
        }

        public string RequestMapData(string url, IDictionary<string, string> parameters)
        {
            if (parameters != null && parameters.Count > 0)
            {
                if (url.Contains("?"))
                {
                    url = url + "&" + BuildPostData(parameters);
                }
                else
                {
                    url = url + "?" + BuildPostData(parameters);
                }
            }

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.ServicePoint.Expect100Continue = false;
            req.Method = "GET";
            req.KeepAlive = true;
            req.UserAgent = "Test";
            req.ContentType = "application/x-www-form-urlencoded;charset=utf-8";

            HttpWebResponse rsp = null;
            try
            {
                rsp = (HttpWebResponse)req.GetResponse();
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.Timeout)
                {
                    rsp = null;
                }
            }

            if (rsp != null)
            {
                if (rsp.CharacterSet != null)
                {
                    Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
                    return GetResponseAsString(rsp, encoding);
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 把响应流转换为文本。
        /// </summary>
        /// <param name="rsp">响应流对象</param>
        /// <param name="encoding">编码方式</param>
        /// <returns>响应文本</returns>
        private static string GetResponseAsString(HttpWebResponse rsp, Encoding encoding)
        {
            StringBuilder result = new StringBuilder();
            Stream stream = null;
            StreamReader reader = null;

            try
            {
                // 以字符流的方式读取HTTP响应
                stream = rsp.GetResponseStream();
                reader = new StreamReader(stream, encoding);

                // 每次读取不大于256个字符，并写入字符串
                char[] buffer = new char[256];
                int readBytes = 0;
                while ((readBytes = reader.Read(buffer, 0, buffer.Length)) > 0)
                {
                    result.Append(buffer, 0, readBytes);
                }
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.Timeout)
                {
                    result = new StringBuilder();
                }
            }
            finally
            {
                // 释放资源
                if (reader != null) reader.Close();
                if (stream != null) stream.Close();
                if (rsp != null) rsp.Close();
            }

            return result.ToString();
        }

        /// <summary>
        /// 组装普通文本请求参数。
        /// </summary>
        /// <param name="parameters">Key-Value形式请求参数字典。</param>
        /// <returns>URL编码后的请求数据。</returns>
        private static string BuildPostData(IDictionary<string, string> parameters)
        {
            StringBuilder postData = new StringBuilder();
            bool hasParam = false;

            IEnumerator<KeyValuePair<string, string>> dem = parameters.GetEnumerator();
            while (dem.MoveNext())
            {
                string name = dem.Current.Key;
                string value = dem.Current.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                {
                    if (hasParam)
                    {
                        postData.Append("&");
                    }

                    postData.Append(name);
                    postData.Append("=");
                    postData.Append(Uri.EscapeDataString(value));
                    hasParam = true;
                }
            }
            return postData.ToString();
        }
    }
}