﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Helper
{
    public class CacheItem
    {
        public CacheItem()
        {
            CacheTime = DateTime.Now;
        }
        private DateTime CacheTime { get; set; }
        public int ExperiedTimes { get; set; }
        public Object Vaues { get; set; }
        public bool IsExperied()
        {
            return CacheTime.AddSeconds(ExperiedTimes) < DateTime.Now;

        }
    }
}