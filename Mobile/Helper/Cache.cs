﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobile.Helper
{
    public static class CacheManager
    {
        public static Dictionary<string, CacheItem> list = new Dictionary<string, CacheItem>();
        public static void Add(string key, CacheItem value)
        {
            if (list.ContainsKey(key))
            {
                list.Remove(key);
            }

            list.Add(key, value);
        }

        public static CacheItem Get(string key)
        {
            if (list.ContainsKey(key))
            {
                var value = list[key];
                if (value.IsExperied())
                {
                    list.Remove(key);
                }
                else
                {
                    return list[key];
                }
            }

            return null;
        }

        public static bool Contains(string key)
        {
            return list.ContainsKey(key);
        }
    }
}