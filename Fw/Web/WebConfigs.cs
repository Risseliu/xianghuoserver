﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Fw.Web
{
    public class WebConfigs
    {
        public static string GetConfig(string configName)
        {
            return ConfigurationManager.AppSettings[configName];
        }
    }
}
