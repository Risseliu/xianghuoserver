﻿namespace Fw.Reflection
{
    /// <summary>
    /// 类的属性类型
    /// </summary>
    public enum PropertyType
    {
        /// <summary>
        /// 基本的数据类型(int,string等)
        /// </summary>
        BaseType,
        /// <summary>
        /// 用户定义的类型
        /// </summary>
        CustomType
    }
}
