namespace Fw.FileUpload
{
    /// <summary>
    /// 上传文件完成代理
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void FileUploadCompletedEvent(object sender, FileUploadCompletedEventArgs args);
}