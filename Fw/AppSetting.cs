﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw
{
    public static class AppSetting
    {
        private const string _FileTempPath = "Resourse/Temp";

        /// <summary>
        /// c:\wwwroot\site1
        /// </summary>
        public static string SiteRoot;

        public static string FileTempPhysicalPath
        {
            get { return ToFilePhysicalPath(_FileTempPath); }
        }
        private  static  string ToFilePhysicalPath(string path)
        {
            return string.Empty;
        }


        public static string IPFilterFileName { get; set; }

        public static string IPFilterPath { get; set; }
        public static string Pdf2SWF_Path { get; set; }
        public static string FFMPEGFilePath { get; set; }
    }
}
