﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerFw.Extends
{
    public static class DateTimeExtends
    {
        public static string ToLongString(this DateTime time)
        {
            return time.ToString("yyyy-MM-dd HH:mm:ss");
        }
    }
}
