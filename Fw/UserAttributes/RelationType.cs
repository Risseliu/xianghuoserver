namespace Fw.UserAttributes
{
    /// <summary>
    /// 字典的关联类型
    /// </summary>
    public enum RelationType
    {
        Single,
        Multi
    }
}