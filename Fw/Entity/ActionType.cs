namespace Fw.Entity
{
    public static class ActionType
    {
        public const string select = "select";
        public const string update = "update";
        public const string delete = "delete";
        public const string insert = "insert";
        public const string storedProcedure = "storedProcedure";
    }
}