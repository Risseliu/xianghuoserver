﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Entity
{
    public class StoredProcedureParams
    {
        public string StoredProcedureName { get; set; }
        public List<string> ParamsName { get; set; }
        public List<string> ParamsValue { get; set; }
    }
}
