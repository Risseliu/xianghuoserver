﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Entity
{
    /// <summary>
    /// 接收客户端发来的命令
    /// </summary>
    public class ActionCmd
    {
        /// <summary>
        /// 是否是自定义命令(自定义命令是指执行服务器程序中的一个方法)
        /// </summary>
        public bool IsCustomCmd { get; set; }
        /// <summary>
        /// 自定义命令(自定义命令是指执行服务器程序中的一个方法)
        /// </summary>
        public CustomCmd CustomCmd { get; set; }
        /// <summary>
        /// 操作类型(查询、删除、修改)select,update,delete,insert,storedProcedure
        /// </summary>
        public string ActionType { get; set; }
        /// <summary>
        /// 存储过程参数
        /// </summary>
        public StoredProcedureParams StoredProcedureParams { get; set; }
        /// <summary>
        /// 一般查询时使用的参数(select,update,delete,insert)
        /// </summary>
        public SelectAcionParams SelectAcionParams { get; set; } //

        /// <summary>
        /// 操作对象的实体类名称
        /// </summary>
        public string ActionObjectName { get; set; } // 

        /// <summary>
        /// 操作对象的序列化字符串(update,insert的对象或delete的主键)
        /// </summary>
        public string ActionObjectEntryStr { get; set; } // 

        /// <summary>
        /// 字段过虑
        /// </summary>
        public FieldFilter FieldFilter { get; set; }
    }
    /// <summary>
    /// 选择的时候指定列
    /// </summary>
    public class FieldFilter
    {
        /// <summary>
        /// 如果黑名单存在,则黑名单里的字段不搜索
        /// </summary>
        public string[] BlackNames { get; set; }

        /// <summary>
        /// 如果白名单存在,则只搜索白名单字段,如果一个字段(x)同时存在于白名单和黑名单中,则以黑名单为准不搜索该字段
        /// </summary>
        public string[] WhiteNames { get; set; }
    }
}
