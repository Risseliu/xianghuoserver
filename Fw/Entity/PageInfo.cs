﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Entity
{
    public class PageInfo
    {
        public int IsPaging { get; set; }
        /// <summary>
        /// 总记录数
        /// </summary>
        public int TotalRecord { get; set; }
        /// <summary>
        /// 总页
        /// </summary>
        public int TotalPage { get; set; }
        /// <summary>
        /// 当前页
        /// </summary>
        public int PageIndex { get; set; }
        public int StartRecord { get; set; }
        public int EndRecord { get; set; }
        /// <summary>
        /// 页面记录数
        /// </summary>
        public int PageSize { get; set; }
    }
}
