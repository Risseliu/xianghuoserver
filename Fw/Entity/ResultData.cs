namespace Fw.Entity
{
    /// <summary>
    /// 返回的结果
    /// </summary>
    public class ResultData
    {
        public bool HaveError = false;
        public string ErrorMsg; //错误信息
        public string DetailErrorMsg;
        public string FriendlyErrorMsg;//友好错误信息
        public int AllRecord; //所有记录数
        public int Record; //受影响记录数
        public string ObjectEntryStr; //实体对象数据
        public PageInfo PageInfo { get; set; }
    }
}