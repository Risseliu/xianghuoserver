﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Aspose.Cells;
using Aspose.Slides;
using Aspose.Slides.Export;
using Aspose.Words;
using SaveFormat = Aspose.Words.SaveFormat;


namespace ServerFw.Doc
{
    public class DocHelper
    {
        public static bool ConvertToSWF(string exePath,string sourcePath, string targetPath = null)
        {
            string _targetPath;
            try
            {
                _targetPath = sourcePath.ToLower();
                if (!_targetPath.EndsWith("pdf"))
                {
                    _targetPath = ConvertToPDF(sourcePath);
                }
                //            }
                //            catch (Exception eee)
                //            {
                //
                //            }
                if (targetPath == null)
                {
                    FileInfo fi = new FileInfo(sourcePath);
                    targetPath = Path.Combine(fi.Directory.FullName, fi.Name.Replace(fi.Extension, "") + ".swf");
                }
                string sourcePath1 = @"""" + _targetPath + @"""";
                string argsStr = "  -t " + sourcePath1 + " -s flashversion=9 -G -f -s poly2bitmap -o " + "\"" + targetPath + "\"";
                ExcutedCmd(exePath, argsStr);
            }
            catch (Exception eeeee)
            {
                
            }
            if (!File.Exists(targetPath))
            {
                File.Delete(sourcePath);
                return false;
            }
            
            return true;
        }
        public static void ExcutedCmd(string cmd, string args)
        {
            using (Process p = new Process())
            {

                ProcessStartInfo psi = new ProcessStartInfo(cmd, args);
                psi.CreateNoWindow = true;
                p.StartInfo = psi;
                p.Start();
                p.WaitForExit();
            }
        }

        public static string ConvertToPDF(string sourcePath, string targetPath = null)
        {
            var s = sourcePath.ToLower();
            if (s.EndsWith("doc") || s.EndsWith("docx"))
            {
                return ConvertWordToPdf(sourcePath, targetPath);
            }
            else if (s.EndsWith("ppt") || s.EndsWith("pptx"))
            {
                return ConvertPPTToPdf(sourcePath, targetPath);
                
            }
            else if (s.EndsWith("xls") || s.EndsWith("xlsx"))
            {
                return ConvertExcelToPdf(sourcePath, targetPath);

            }
            throw new Exception("未识别的文档类型:" + s);
        }
        private static string ConvertPPTToPdf(string sourcePath, string targetPath)
        {
            if (targetPath == null)
            {
                FileInfo fi = new FileInfo(sourcePath);
                targetPath = Path.Combine(fi.Directory.FullName, fi.Name.Replace(fi.Extension, "") + ".pdf");
            }
            Presentation d = new Presentation(sourcePath);
            try
            {
                d.Save(targetPath, Aspose.Slides.Export.SaveFormat.Pdf);
            }
            catch (Exception eeee)
            {
                Thread.Sleep(3000);
            }

            return targetPath;
        }

        private static string ConvertExcelToPdf(string sourcePath, string targetPath)
        {
            if (targetPath == null)
            {
                FileInfo fi = new FileInfo(sourcePath);
                targetPath = Path.Combine(fi.Directory.FullName, fi.Name.Replace(fi.Extension, "") + ".pdf");
            }
            Workbook d = new Workbook(sourcePath);
            d.Save(targetPath, Aspose.Cells.SaveFormat.Pdf);
           
            return targetPath;
        }
        private static string ConvertWordToPdf(string sourcePath, string targetPath)
        {
            if (targetPath == null)
            {
                FileInfo fi = new FileInfo(sourcePath);
                targetPath = Path.Combine(fi.Directory.FullName, fi.Name.Replace(fi.Extension, "") + ".pdf");
            }
            Document d = new Document(sourcePath);
            d.Save(targetPath, SaveFormat.Pdf);
            return targetPath;
        }
    }
}
