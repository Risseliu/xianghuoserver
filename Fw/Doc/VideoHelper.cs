﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerFw.Doc
{
    public class VideoHelper
    {
        /// <summary>
        /// 截图
        /// </summary>
        /// <param name="exePath">ffmpeg地址</param>
        /// <param name="videoPath">视频地址</param>
        /// <param name="imagePath">截图地址</param>
        /// <param name="time">时间00:00:11,</param>
        public static void SaveImage(string exePath, string videoPath, string imagePath, string time)
        {
            //ffmpeg -i aaa.f4v -ss 00:00:11 -f image2 output.jpg

            string args;
            args = string.Format(@" -ss 00:00:11 -i ""{0}"" -f image2 ""{1}""", videoPath,imagePath);
            DocHelper.ExcutedCmd(exePath,args);
        }
    }
}
