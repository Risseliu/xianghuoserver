﻿using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;


namespace Fw.DataAccess
{
    public class SqlFactoryBase
    {
        
        protected virtual IDbCommand GetCommand(string sql = null, params DbParameter[] paras)
        {
            SqlCommand comm = new SqlCommand();
            if (sql != null)
            {
                comm.CommandText = sql;
            }
            if (paras != null)
            {
                paras.ToList().ForEach(w => comm.Parameters.Add(w));
            }
            return comm;
        }
        protected virtual DbParameter GetDbParameter(string name, object value)
        {
            return new SqlParameter(name, value);
        }
    }
}
