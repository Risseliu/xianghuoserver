﻿namespace Fw.DataAccess
{
    /// <summary>
    /// 操作类型
    /// </summary>
    public enum SqlOperType
    {
        Insert,
        Update,
        Delete,
        Sql
    }
}