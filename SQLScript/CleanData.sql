  Declare @cleanDateTime dateTime
  Declare @cleanPushDateTime dateTime
  set  @cleanDateTime='2015-03-20'
  set @cleanPushDateTime='2015-03-20'

  Create table #tempOrder
  (
	  OrderID uniqueidentifier
  )
  Create table #tempDriver
  (
	  ID uniqueidentifier
  )

  INSERT INTO #tempOrder(OrderID) SELECT ID FROM [Orders] WHERE Orders.[PublishTime]<@cleanDateTime

  INSERT INTO #tempDriver(ID)
  SELECT 
		DISTINCT [ID]
  FROM [DriverRelOrders] do
	 inner join #tempOrder tr ON do.OrderID =tr.OrderID AND
	 (do.[EnsureState]=0 Or do.[OrderState]=0 Or do.[OrderState]=-1 Or do.[OrderState]=-3 Or do.[OrderState]=-7) AND
	 do.[EnsureState]<>1
  
  DELETE  [DriverRelOrders] FROM [DriverRelOrders] do inner join #tempDriver d on do.ID=d.ID
  DELETE FROM [BPushes] WHERE [CreateTime]<@cleanPushDateTime

  DROP Table #tempOrder
  DROP Table #tempDriver
  

