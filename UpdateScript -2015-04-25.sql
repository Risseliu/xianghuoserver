USE[tqtest]

if Not exists(select * from syscolumns where id=object_id('Customers') and name='IntroducePersonId') 
ALTER TABLE [Customers] ADD  IntroducePersonId uniqueidentifier null

if Not exists(select * from syscolumns where id=object_id('Admins') and name='RoleId') 
ALTER TABLE [Admins] ADD  RoleId uniqueidentifier null

if Not exists(select * from syscolumns where id=object_id('Routes') and name='AreaCode') 
ALTER TABLE [Routes] ADD  AreaCode nvarchar(50) null

if Not exists(select * from syscolumns where id=object_id('Orders') and name='AreaCode') 
ALTER TABLE [Orders] ADD  AreaCode nvarchar(50) null


IF EXISTS ( select * FROM  sysobjects WHERE name = 'Area' and type = 'U')
DROP TABLE Area
CREATE TABLE [dbo].[Area](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Remark] [nvarchar](200) NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


IF EXISTS ( select * FROM  sysobjects WHERE name = 'AgentArea' and type = 'U')
DROP TABLE AgentArea
CREATE TABLE [dbo].[AgentArea](
	[Id] [int] NOT NULL,
	[AccountId] [uniqueidentifier] NOT NULL,
	[AreaCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AgentArea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

