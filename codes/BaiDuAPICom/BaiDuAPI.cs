﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Fw.Serializer;

namespace BaiDuAPICom
{
    //689a2fc6697dbe72282ab94f792604a6
    public class BaiDuAPI
    {
        //方法地图服务使用
        public static string AK = "8YTqvW3ynu2GWd1dI3QVVdlo";
        public static bool GetLatLngByName(string name,out double lat1, out double lng1,string city = null)
        {
            lat1 = 0;
            lng1 = 0;
            name = HttpUtility.UrlEncode(name);
            //http://api.map.baidu.com/geocoder/v2/?address=百度大厦&output=json&ak=689a2fc6697dbe72282ab94f792604a6
            //http://api.map.baidu.com/geocoder/v2/?address=百度大厦&output=json&ak=eoRGNxb3Ks31vsjSjeLPj2Kp
            string url = string.Format("http://api.map.baidu.com/geocoder/v2/?address={0}&city={2}&output=json&ak={1}"
                , name, AK, city ?? "");
            WebClient wc = new WebClient();
            var str = wc.DownloadString(url);
            try
            {
                var re = JsonHelper.FastJsonDeserialize<BaiDuJingWeiDuResult>(str);
                lat1 = re.result.location.lat;
                lng1 = re.result.location.lng;
            }
            catch
            {
                
            }
            return true;
        }
        
        public static string GetSiteName(double lat1, double lng1)
        {
            return "";
        }
        /**
	 * 计算经纬度之间的距离
	 * @param lat1 纬度1
	 * @param lng1 经度1
	 * @param lat2 纬度2
	 * @param lng2经度2
	 * @return 米
	 */
        public static double GetDistance(double lat1, double lng1, double lat2,
                double lng2)
        {
            double radLat1 = rad(lat1);
            double radLat2 = rad(lat2);
            double a = radLat1 - radLat2;
            double b = rad(lng1) - rad(lng2);
            double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2)
                    + Math.Cos(radLat1) * Math.Cos(radLat2)
                    * Math.Pow(Math.Sin(b / 2), 2)));
            s = s * EARTH_RADIUS;
            s = Math.Round(s * 10000) / 10000;
            return s;
        }

        private static double EARTH_RADIUS = 6378.137 * 1000;//地球半径 

        private static double rad(double d)
        {
            return d * Math.PI / 180.0;
        }

        
    }

    public class BaiDuJingWeiDuResult
    {
        public int status { get; set; }
        public BaiDuJingWeiDuResult1 result { get; set; }
    }

    public class BaiDuJingWeiDu
    {
        public double lng { get; set; }
        public double lat { get; set; }
    }
    public class BaiDuJingWeiDuResult1
    {
        public BaiDuJingWeiDu location { get; set; }
        public int precise { get; set; }
        public int confidence { get; set; }
        public string level { get; set; }
    }
}
