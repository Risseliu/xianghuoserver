﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace HuoYunBLL
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public abstract class ApiBaseAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            var c = request["C"]; //随机字符串
            var r = request["R"]; //加密结果

            var success = true;
            var p = GetKey(filterContext,c); //私钥
            if (string.IsNullOrEmpty(p))
            {
                success = false;
            }
            else
            {
                var result = FormsAuthentication.HashPasswordForStoringInConfigFile(c + p, "MD5");
                if (result != null) success = result.Equals(r, StringComparison.OrdinalIgnoreCase);
            }

            if (!success)   
            {
                filterContext.Result = Result.GetResult(false, "未验证的数据", null);
            }
        }

        protected abstract string GetKey(AuthorizationContext filterContext,string c);
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return false;
            return base.AuthorizeCore(httpContext);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //            filterContext.Result = 
            base.OnAuthorization(filterContext);
        }
    }
}