﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Entity;

namespace HuoYunBLL
{
    public class LoninResult
    {
        public int Success { get; set; }//1成功,2需要再次输入验证码,3失败
        public string PK { get; set; }//私钥
        public string PKOverDate { get; set; }//eg:2008-5-5 23:22:10 私钥过期时间,过期后需要重新登陆

        /// <summary>
        /// 1开通货主,2开通车主,3开通货主和车主
        /// </summary>
        public int UserState { get; set; }

        /// <summary>
        /// 0一般登陆,1自动注册登陆
        /// </summary>
        public int Register { get; set; }
        public String UserID { get; set; }
    }
    public class ListPageModel<T>
    {
        public List<T> List { get; set; }
        public PageInfo Page { get; set; }
        public object Tag1 { get; set; }
    }
}
