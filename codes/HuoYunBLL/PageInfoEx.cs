﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Fw.Entity;


namespace HuoYunBLL
{
    public static class PageInfoEx
    {
        public static MvcHtmlString GetPageHtml(this PageInfo owner)
        {
            var temp = @"<div class=""panelBar"">
        <div class=""pages"">
            <span>显示</span>
            <select class=""combox"" name=""numPerPage"" onchange=""navTabPageBreak({{numPerPage:this.value}})"">
                <option value=""20"">20</option>
                <option value=""50"">50</option>
                <option value=""100"">100</option>
                <option value=""200"">200</option>
            </select>
            <span>条，共 {0} 条记录</span>
        </div>

        <div class=""pagination"" targettype=""navTab"" totalcount=""{0}"" numperpage=""{1}"" pagenumshown=""10"" currentpage=""{2}""></div>
    </div>";
            return MvcHtmlString.Create(string.Format(temp, owner.TotalRecord, owner.PageSize, owner.PageIndex));
        }
    }
}
