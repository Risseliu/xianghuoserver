﻿using System;
using System.Web;

namespace HuoYunBLL
{
    public static class RequestEx
    {
        public static bool IsSelect(this HttpRequestBase owner)
        {
            string value = owner["Select"];

            return !String.IsNullOrEmpty(value);
        }
        //返回是否为列表选择页面
        public static bool IsSingleSelect(this HttpRequestBase owner)
        {
            string value = owner["Select"];

            return !String.IsNullOrEmpty(value) && value == "1";
        }
        public static bool IsMultiSelect(this HttpRequestBase owner)
        {
            string value = owner["Select"];
            return !String.IsNullOrEmpty(value) && value == "2";
        }

        public static string GetOrderFieldClass(this HttpRequestBase owner, string orderField)
        {
            var str = owner.GetOrderField(orderField);
            if (str.EndsWith(" desc", StringComparison.OrdinalIgnoreCase))
            {
                return " desc";
            }
            else if (str.EndsWith(" asc", StringComparison.OrdinalIgnoreCase))
            {
                return "asc";
            }
            return "";
        }
    }
}