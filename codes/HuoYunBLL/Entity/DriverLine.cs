﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.UserAttributes;

namespace HuoYunBLL.Entity
{
    /// <summary>
    /// 司机行驶路线
    /// </summary>
    public class DriverLine
    {
    }

    public class Admin
    {
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        public Guid ID { get; set; }
        public string UserName { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public int IsEnabled { get; set; }
    }
    public class DriverRelOrder
    {
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        public Guid ID { get; set; }
        /// <summary>
        /// 司机ID
        /// </summary>
        public Guid UserID { get; set; }
        /// <summary>
        /// Int OrderState 状态(0新订单,2正在进行中,3完成,4已评价,-1错过)
        /// </summary>
        public int OrderState { get; set; }
        /// <summary>
        /// GUID OrderID 订单ID
        /// </summary>
        public Guid OrderID { get; set; }
    }

    public class Order
    {
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        public Guid ID { get; set; }
        public string OrderNumber { get; set; }
        public string StartName { get; set; }

        public string Password { get; set; }
        public int IsEnabled { get; set; }


        public double StartPointX { get; set; }
        public double StartPointY { get; set; }
        public double EndPointX { get; set; }
        public double EndPointY { get; set; }
        public string EndName { get; set; }
        /// <summary>
        /// Varchar(200) Lading 货物名
        /// </summary>
        public string Lading { get; set; }
        /// <summary>
        /// Varchar(200) Weight重量
        /// </summary>
        public string Weight { get; set; }
        public DateTime PublishTime { get; set; }
        public DateTime DeliverTimeStart { get; set; }
        public DateTime DeliverTimeEnd { get; set; }
        public Guid GuestUserID { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        public double Price  { get; set; }
        /// <summary>
        /// 接收人
        /// </summary>
        public string ReciveUserName { get; set; }
        public string ReciveUserPhoneNumber { get; set; }
        /// <summary>
        /// Double PingJia1Score 货主对司机的评分
        /// </summary>
        public double PingJia1Score { get; set; }
        /// <summary>
        /// Int PingJia1_2指定地点接送(货主对司机的)
        /// </summary>
        public int PingJia1_2 { get; set; }
        /// <summary>
        /// Int PingJia1_3货物完好无损(货主对司机的)
        /// </summary>
        public int PingJia1_3 { get; set; }
        /// <summary>
        /// Int PingJia1_4规定时间送到(货主对司机的)
        /// </summary>
        public int PingJia1_4 { get; set; }
        /// <summary>
        /// Int PingJia1Content 评价内容(货主对司机的)
        /// </summary>
        public int PingJia1Content { get; set; }
        /// <summary>
        /// Double PingJia2Score司机对货主的评分
        /// </summary>
        public double PingJia2Score { get; set; }
        /// <summary>
        /// Varchar(200) PingJia2Content 评价内容(司机对货主的)
        /// </summary>
        public string PingJia2Content { get; set; }
        /// <summary>
        /// Varchar(200) OrderRemark(货主编写的备注);
        /// </summary>
        public string OrderRemark { get; set; }
    }
    public class DriverGather
    {
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
        /// <summary>
        /// Int BargainTime送货次数
        /// </summary>
        public int BargainTime { get; set; }
        /// <summary>
        /// double BargainMoney 累计收入
        /// </summary>
        public double BargainMoney { get; set; }
        /// <summary>
        /// double Grade货主评价
        /// </summary>
        public double Grade { get; set; }
    }
    /// <summary>
    /// 货主信息表
    /// </summary>
    public class Customer
    {
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        public Guid ID { get; set; }
        public int Valid { get; set; }
        public string ShenFenZheng { get; set; }
        public string Remark { get; set; }
    }
    public class Driver
    {
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        public Guid ID { get; set; }
        public int UserNumber { get; set; }
        public string ShenFenZheng { get; set; }
        public string JiaZhao { get; set; }
        public string XingShiZheng { get; set; }
        /// <summary>
        /// 车辆正面照图片地址
        /// </summary>
        public string CheLiang1 { get; set; }
        /// <summary>
        /// 车辆45度照图片地址
        /// </summary>
        public string CheLiang2 { get; set; }
        /// <summary>
        /// Varchar(200) ImagePath,头像图片地址
        /// </summary>
        public string ImagePath { get; set; }
        public string CheXing { get; set; }
        public string ChePai { get; set; }
        public string Remark { get; set; }

    }


    public class User
    {
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        public Guid ID { get; set; }
        public string UserName { get; set; }
        public int UserNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneID { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// Varchar(200) ImagePath,头像图片地址
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// Int Gender性别,(1男,2女)
        /// </summary>
        public int Gender { get; set; }
    }
}
