﻿using System.Web.Routing;

namespace HuoYunBLL
{
    public static class RouteDataEx
    {
        public static string GetController(this RouteData owner)
        {
            return owner.Values["controller"].ToString();
        }


    }
}