﻿using System;
using System.Text;
using System.Web.Mvc;
using Fw.Extends;
using ServerFw.Extends;

namespace HuoYunBLL
{
    /// <summary>
    /// 返回的结果
    /// </summary>
    public class Result
    {
        public string ResultData { get; set; }
        public string ErrorMessage { get; set; }
        public bool Success { get; set; }
        /// <summary>
        /// 100-199登陆问题
        /// 201非法操作
        /// 202收货时,验证码不对
        /// 注册时:ErrorCode=205手机号已经注册,206验证码错误
        /// </summary>
        public int ErrorCode { get; set; }

        /// <summary>
        /// 返回登陆结果
        /// </summary>
        /// <param name="success"></param>
        /// <param name="message"></param>
        /// <param name="errorCode"></param>
        /// <param name="pk"></param>
        /// <param name="pkoverDateTime"></param>
        /// <returns></returns>
        public static JsonResult GetLoginResult(bool success,string message,int errorCode,string pk,DateTime pkoverDateTime)
        {
            return GetResult(true, message,
                new LoninResult { PK = pk, PKOverDate = pkoverDateTime.ToLongString(), Success = errorCode }
                    .ToJson(), errorCode);
        }

        public static JsonResult GetError(string message, int errorCode = 1)
        {
            return GetResult(false, message, null, errorCode);
        }

        public static JsonResult GetSuccess()
        {
            return GetResult(true, null, null);
        }
        /// <summary>
        /// 返回无结果的成功
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static JsonResult GetSuccess<T>(T t) 
        {
            return GetResult(true, null, t.ToJson());
        }
        
        public static JsonResult GetResult(bool success,string message,string resultData,int errorCode = 0)
        {
            var r = new Result { ResultData = resultData, ErrorMessage = message, Success = success, ErrorCode = errorCode };
            return new JsonResult {Data = r, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }
    }
}