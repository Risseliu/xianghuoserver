﻿using System;
using System.Web;
using Fw.Extends;

namespace HuoYunBLL
{
    public static class StringEx
    {
        public static IHtmlString Raw(this string owner)
        {
            return new HtmlString(owner);
        }

        /// <summary>
        /// 返回排序字符串
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="orderField"></param>
        /// <returns></returns>
        public static string GetOrderField(this HttpRequestBase owner, string orderField)
        {
            var req = owner["orderField"];
            if (req != null)
            {
                var vl = req.Split(' ');
                if (vl[0].Equals(orderField, StringComparison.OrdinalIgnoreCase))
                {
                    if (vl.Length == 0)
                    {
                        return orderField + " desc";
                    }
                    else
                    {
                        if (req.EndsWith("desc", StringComparison.OrdinalIgnoreCase))
                        {
                            return orderField + " asc";
                        }
                        else
                        {
                            return orderField + " desc";
                        }
                    }
                }
            }
            return orderField;
        }

        /// <summary>
        /// 返回两个字符串是否相等,兼容null不会发生异常
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool FixEquals(this string owner, string other)
        {
            return string.Equals(owner, other);
        }
        /// <summary>
        /// 返回两个字符串是否相等,兼容null不会发生异常
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static string LeftClip(this string owner, int len)
        {
            if (owner != null && len < owner.Length)
            {
                var leftClip = owner.Left(len);
                return leftClip;
            }
            return owner ?? "";
        }
    }
}