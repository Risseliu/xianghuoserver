﻿using System.Web.Mvc;

namespace HuoYunBLL
{
    public static class ViewContextEx
    {
        public static string GetController(this ViewContext owner)
        {
            return owner.RouteData.Values["controller"].ToString();
        }public static string GetAction(this ViewContext owner)
        {
            return owner.RouteData.Values["action"].ToString();
        }
        public static string GetControllerAndAction(this ViewContext owner)
        {
            return owner.RouteData.Values["controller"] + "/" + owner.RouteData.Values["action"];
        }
    }
}