﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web.Mvc;
//
//namespace HuoYunBLL
//{
//    [Authorization]
//    public class BaseController<T> : Controller where T : class , new()
//    {
//        //
//        // GET: /Material/
//        public ActionResult Create(Guid? id = null)
//        {
//            T sMaterial = new T { };
//            sMaterial = NewAfter(sMaterial);
//            if (id.HasValue && id != Guid.Empty)
//            {
//                sMaterial = DataAccessFactory.Select<T>().FirstOrDefault(w => w.ID == id);
//                if (object.Equals(sMaterial, null))
//                {
//                    sMaterial = new T();
//                }
//            }
//            return View(sMaterial);
//        }
//
//        protected virtual T NewAfter(T t)
//        {
//            return t;
//        }
//        protected virtual T PostSaveAfter(T t)
//        {
//            return t;
//        }
//        [HttpPost]
//        public JsonResult Create(T ma)
//        {
//            ma = PostSaveAfter(ma);
//            if (ma.ID != Guid.Empty)
//            {
//                var r = DataAccessFactory.Select<T>().FirstOrDefault(w => w.ID == ma.ID);
//                if (r != null)
//                {
//                    ma = UpdateBefore(ma);
//                    DataAccessFactory.UpdateAction(ma);
//                }
//            }
//            else
//            {
//                ma.ID = Guid.NewGuid();
//                DataAccessFactory.InsertAction(ma);
//            }
//            //            var c = RouteData.GetController();
//            return getDefaultJsonResult();
//
//            //            return new JsonResult { Data = new { statusCode = 200, message = "保存成功", callbackType = "closeCurrent", navTabId = c } };
//        }
//
//        protected virtual T UpdateBefore(T ma)
//        {
//            return ma;
//        }
//
//        public ActionResult Index(
//                int pageNum = 0,
//                int numPerPage = 20,
//                string orderField = "",
//                string status = "")
//        {
//            ListPageModel<T> p = GetPagedList<T>(pageNum, numPerPage, orderField);
//            p = ListViewAfter(p);
//            return View(p);
//        }
//        /// <summary>
//        /// 通用分页方法
//        /// </summary>
//        /// <typeparam name="TEntity"></typeparam>
//        /// <param name="pageNum"></param>
//        /// <param name="numPerPage"></param>
//        /// <param name="orderField"></param>
//        /// <returns></returns>
//        protected ListPageModel<TEntity> GetPagedList<TEntity>(int pageNum, int numPerPage, string orderField)
//        {
//            PageInfo pi = new PageInfo();
//            pi.PageIndex = pageNum;
//            if (pi.PageIndex <= 0) pi.PageIndex = 1;
//            pi.PageSize = numPerPage;
//
//            if (pi.PageSize <= 1) pi.PageSize = 1;
//            //            if (pi.PageSize <= 20) pi.PageSize = 20;
//            List<SearchEntry> ses = new List<SearchEntry>();
//            var type = typeof(TEntity);
//            ses =
//                GetSearchEntryByBaseProperty(ReflectionHelper.GetBaseTypeProperty(type, PropertyType.BaseType),
//                    ReflectionHelper.GetTableName(type)).ToList();
//            ses = GetSearchEntity(ses);
//            var list = DataAccessFactory.Select<TEntity>(ses, orderField, ref pi);
//            ListPageModel<TEntity> listPageModel = new ListPageModel<TEntity> { List = list, Page = pi };
//            return listPageModel;
//
//        }
//
//        protected virtual List<SearchEntry> GetSearchEntity(List<SearchEntry> ses)
//        {
//            return ses;
//        }
//
//        protected virtual ListPageModel<T> ListViewAfter(ListPageModel<T> t)
//        {
//            return t;
//        }
//        #region 搜索条件
//
//        /// <summary>
//        /// 返回基本属性列表的搜索条件
//        /// </summary>
//        /// <param name="getBaseTypeProperty"></param>
//        /// <param name="uiElement"></param>
//        /// <param name="tableName"></param>
//        /// <returns></returns>
//        public IEnumerable<SearchEntry> GetSearchEntryByBaseProperty(List<PropertyInfo> getBaseTypeProperty, string tableName)
//        {
//            return getBaseTypeProperty.Select(w => GetGetSearchEntryItem(w, tableName)).Where(w => w != null).ToList();
//        }
//        private SearchEntry GetGetSearchEntryItem(PropertyInfo propertyInfo, string tableName)
//        {
//            SearchEntry re = null;
//            var val = GetUIElementValue(propertyInfo.Name) ?? "";
//            val = val.Replace("'", "").Replace(";", "");
//            if (!string.IsNullOrEmpty(val))
//            {
//                re = new SearchEntry
//                {
//                    ColumnName = tableName + "." + ReflectionHelper.GetPropertyField(propertyInfo),
//                    Flag = "=",
//                    value = val
//                };
//                if (propertyInfo.PropertyType == typeof(string) || propertyInfo.PropertyType == typeof(Guid) ||
//                    propertyInfo.PropertyType == typeof(Guid?))
//                {
//                    re.value = string.Format("'{0}'", re.value);
//                }
//
//            }
//            val = GetUIElementValue(propertyInfo.Name + "_Like");
//            if (!string.IsNullOrEmpty(val))
//            {
//                re = new SearchEntry
//                {
//                    ColumnName = tableName + "." + ReflectionHelper.GetPropertyField(propertyInfo),
//                    Flag = "=",
//                    value = val
//                };
//                if (propertyInfo.PropertyType == typeof(string))
//                {
//                    re.Flag = "like";
//                    re.value = string.Format("'%{0}%'", re.value);
//                }
//            }
//            val = GetUIElementValue(propertyInfo.Name + "_Start");
//            if (!string.IsNullOrEmpty(val))
//            {
//                re = new SearchEntry
//                {
//                    ColumnName = ReflectionHelper.GetPropertyField(propertyInfo),
//                    Flag = ">=",
//                    value = val
//                };
//
//            }
//            val = GetUIElementValue(propertyInfo.Name + "_End");
//            if (!string.IsNullOrEmpty(val))
//            {
//                re = new SearchEntry
//                {
//                    ColumnName = ReflectionHelper.GetPropertyField(propertyInfo),
//                    Flag = "<=",
//                    value = val
//                };
//            }
//            if (re != null && propertyInfo.PropertyType == typeof(DateTime))
//            {
//                re.value = string.Format("'{0}'", re.value);
//            }
//            val = GetUIElementValue(propertyInfo.Name + "_In");
//            if (!string.IsNullOrEmpty(val))
//            {
//                var values = val.Split(',');
//                re = new SearchEntry
//                {
//                    ColumnName = ReflectionHelper.GetPropertyField(propertyInfo),
//                    Flag = "in",
//                    //value = val
//                };
//                if (propertyInfo.PropertyType == typeof(string) || propertyInfo.PropertyType == typeof(DateTime))
//                {
//                    re.value = string.Format("({0})", values.Select(w => string.Format("'{0}'", w)).ToList().Serialize(","));
//                }
//            }
//            return re;
//        }
//        private string GetUIElementValue(string name)
//        {
//            return Request[name] ?? "";
//        }
//        #endregion
//        public JsonResult Delete(Guid id)
//        {
//            var item = DataAccessFactory.Select<T>().FirstOrDefault(w => w.ID == id);
//            if (item != null) DataAccessFactory.Delete<T>(new List<string> { id.ToString().Include("'") });
//            return getDefaultJsonResult(callbackType: "123");
//            //            var c = RouteData.GetController();
//            //            return new JsonResult { Data = new { statusCode = 200, message = "保存成功", callbackType = "closeCurrent1", navTabId = c } };
//        }
//
//        JsonResult getDefaultJsonResult(string navTabId = null, string callbackType = null)
//        {
//            var c = navTabId ?? Request["NT"] ?? RouteData.GetController();
//            return new JsonResult
//            {
//                Data = new
//                {
//                    statusCode = 200,
//                    message = "保存成功",
//                    callbackType = callbackType ?? "closeCurrent",
//                    navTabId = c
//                }
//            };
//        }
//    }
//}
