namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Users_JoinDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "JoinDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "JoinDate");
        }
    }
}
