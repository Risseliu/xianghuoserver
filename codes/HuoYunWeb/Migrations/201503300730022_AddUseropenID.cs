namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUseropenID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "openID", c => c.String(maxLength: 200));
        }

        public override void Down()
        {
            DropColumn("dbo.Users", "openID");
        }
    }
}
