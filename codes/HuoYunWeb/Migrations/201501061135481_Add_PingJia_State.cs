namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_PingJia_State : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "GuestAppraised", c => c.Int(nullable: false));
            AddColumn("dbo.Orders", "DriverAppraised", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "DriverAppraised");
            DropColumn("dbo.Orders", "GuestAppraised");
        }
    }
}
