namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class User_UserState : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "UserState", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "UserState");
        }
    }
}
