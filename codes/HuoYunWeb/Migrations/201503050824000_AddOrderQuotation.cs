namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderQuotation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DriverRelOrders", "OrderQuotation", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DriverRelOrders", "OrderQuotation");
        }
    }
}
