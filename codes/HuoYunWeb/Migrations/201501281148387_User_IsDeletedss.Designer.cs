// <auto-generated />
namespace HuoYunWeb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class User_IsDeletedss : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(User_IsDeletedss));
        
        string IMigrationMetadata.Id
        {
            get { return "201501281148387_User_IsDeletedss"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
