namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class User_IsDeletedss : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Orders", "GuestUserID");
            AddForeignKey("dbo.Orders", "GuestUserID", "dbo.Users", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "GuestUserID", "dbo.Users");
            DropIndex("dbo.Orders", new[] { "GuestUserID" });
        }
    }
}
