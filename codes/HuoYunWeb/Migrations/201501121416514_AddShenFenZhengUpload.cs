namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShenFenZhengUpload : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "ShenFenZhengUpload", c => c.Int(nullable: false));
            AddColumn("dbo.Customers", "ShenFenZhengUpload", c => c.Int(nullable: false));
            AddColumn("dbo.DriverLines", "PublishTime", c => c.DateTime());
            AddColumn("dbo.DriverRelOrderDetails", "Gender", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DriverRelOrderDetails", "Gender");
            DropColumn("dbo.DriverLines", "PublishTime");
            DropColumn("dbo.Customers", "ShenFenZhengUpload");
            DropColumn("dbo.Users", "ShenFenZhengUpload");
        }
    }
}
