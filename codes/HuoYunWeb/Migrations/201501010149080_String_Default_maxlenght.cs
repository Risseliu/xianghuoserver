namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class String_Default_maxlenght : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AUsers", "Name", c => c.String(maxLength: 200));
            AlterColumn("dbo.ABooks", "Name", c => c.String(maxLength: 200));
            AlterColumn("dbo.AUserRoles", "Name", c => c.String(maxLength: 200));
            AlterColumn("dbo.AUserTypes", "Name", c => c.String(maxLength: 200));
            AlterColumn("dbo.Users", "MessageValidCode", c => c.String(maxLength: 200));
            AlterColumn("dbo.Orders", "GuestUserPhoneNumber", c => c.String(maxLength: 200));
            AlterColumn("dbo.Orders", "PingJia1Content", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "PingJia1Content", c => c.String());
            AlterColumn("dbo.Orders", "GuestUserPhoneNumber", c => c.String());
            AlterColumn("dbo.Users", "MessageValidCode", c => c.String());
            AlterColumn("dbo.AUserTypes", "Name", c => c.String());
            AlterColumn("dbo.AUserRoles", "Name", c => c.String());
            AlterColumn("dbo.ABooks", "Name", c => c.String());
            AlterColumn("dbo.AUsers", "Name", c => c.String());
        }
    }
}
