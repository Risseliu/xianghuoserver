namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Customer_ValidSubmit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "ValidSubmit", c => c.Int(nullable: false));
            AddColumn("dbo.Customers", "ValidSubmitDateTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "ValidSubmitDateTime");
            DropColumn("dbo.Customers", "ValidSubmit");
        }
    }
}
