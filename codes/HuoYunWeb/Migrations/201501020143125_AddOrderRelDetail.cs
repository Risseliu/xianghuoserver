namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderRelDetail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DriverRelOrderDetails",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OrderState = c.Int(nullable: false),
                        ChangedUser = c.Int(nullable: false),
                        UserName = c.String(maxLength: 200),
                        AtThatTime = c.DateTime(),
                        DriverRelOrderID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.DriverRelOrders", t => t.DriverRelOrderID, cascadeDelete: true)
                .Index(t => t.DriverRelOrderID);
            
            AddColumn("dbo.DriverRelOrders", "EnsureState", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DriverRelOrderDetails", "DriverRelOrderID", "dbo.DriverRelOrders");
            DropIndex("dbo.DriverRelOrderDetails", new[] { "DriverRelOrderID" });
            DropColumn("dbo.DriverRelOrders", "EnsureState");
            DropTable("dbo.DriverRelOrderDetails");
        }
    }
}
