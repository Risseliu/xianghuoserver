namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewOrderTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "DeliverTimeStart2", c => c.DateTime());
            AddColumn("dbo.Orders", "DeliverTimeEnd2", c => c.DateTime());
            AddColumn("dbo.DriverGathers", "NewOrderCount", c => c.Double(nullable: false));
            AddColumn("dbo.DriverGathers", "ProcessOrderCount", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DriverGathers", "ProcessOrderCount");
            DropColumn("dbo.DriverGathers", "NewOrderCount");
            DropColumn("dbo.Orders", "DeliverTimeEnd2");
            DropColumn("dbo.Orders", "DeliverTimeStart2");
        }
    }
}
