namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsMianYi : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "IsMianYi", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "IsMianYi");
        }
    }
}
