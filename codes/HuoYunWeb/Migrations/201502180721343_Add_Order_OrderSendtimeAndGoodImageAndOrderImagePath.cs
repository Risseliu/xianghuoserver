namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Order_OrderSendtimeAndGoodImageAndOrderImagePath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "OrderSendtime", c => c.DateTime());
            AddColumn("dbo.Orders", "OrderImagePath", c => c.String(maxLength: 200));
            AddColumn("dbo.Orders", "GoodImage", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "OrderSendtime");
            DropColumn("dbo.Orders", "OrderImagePath");
            DropColumn("dbo.Orders", "GoodImage");
        }
    }
}
