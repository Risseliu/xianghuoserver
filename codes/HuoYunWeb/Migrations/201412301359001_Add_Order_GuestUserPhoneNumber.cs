namespace HuoYunWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Order_GuestUserPhoneNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "GuestUserPhoneNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "GuestUserPhoneNumber");
        }
    }
}
