﻿$(function () {

	window.setInterval(GetData, 180000); //循环执行！！

});

function GetData()
{
	//var ajaxbg = $("#background,#progressBar");
	//ajaxbg.hide();
	//$.getJSON("Order/HasNoResponseOrders", null, function (data) {
	//	alert('ddd');
	//});
	var ajaxbg = $("#background,#progressBar");
	ajaxbg.hide();
	$.ajax({
		dataType: "json",
		url: "Order/HasNoResponseOrders",
		data: null,
		cache: false,
		success: function (data, textStatus) {
			if (data.Count > 0)
			{
				ShowMessage();
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			
		}
	});
}

function ShowMessage() {
	alertMsg.confirm("有未处理的订单，请及时处理！", {
		okName: '去处理',
		okCall: function () {
			navTab.openTab("NoResponseList", "Order/NoResponseList", { title: "未响应订单", fresh: true, data: {} });
		}
	});
}