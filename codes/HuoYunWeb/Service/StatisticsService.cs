﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using HuoYunWeb.Models;

namespace HuoYunWeb.Service
{
	public class StatisticsService
	{
		private string connStr = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
		public List<StatisticArea> GetAreaSendReceive(string area)
		{
			var connection = new SqlConnection(connStr);
			var sql = @"dbo.[Public_AreaReport]";
			var command = new SqlCommand(sql, connection);
			command.CommandType = CommandType.StoredProcedure;
			var parameter = new SqlParameter("@AreaName", SqlDbType.NVarChar);
			parameter.Value = area;
			command.Parameters.Add(parameter);
			List<StatisticArea> list = new List<StatisticArea>();
		
			try
			{
				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				da.Fill(dt);
				if (dt != null && dt.Rows.Count > 0)
				{
					foreach(DataRow row in dt.Rows)
					{
						var model = new StatisticArea();
						model.Area = row["Area"].ToString();
						model.SendCount =System.Convert.ToInt64(row["SendCount"]);
						model.ReceiveCount = System.Convert.ToInt64(row["ReceiveCount"]);
						list.Add(model);
					}
					
				}
			}
			catch
			{ }
			return list;
		}
        /// <summary>
        /// 根据区域，路线，订单起始时间，订单截止时间统计
        /// </summary>
        /// <param name="area"></param>
        /// <param name="route"></param>
        /// <param name="orderStartDate"></param>
        /// <param name="orderEndDate"></param>
        /// <returns></returns>
        public List<StatisticArea> GetAreaRouteSendReceive(string area,string route,DateTime? orderStartDate, DateTime? orderEndDate)
	    {
	        var connection = new SqlConnection(connStr);
            var sql = @"dbo.[Public_AreaRouteReport]";
			var command = new SqlCommand(sql, connection);
			command.CommandType = CommandType.StoredProcedure;
			var parameter = new SqlParameter("@AreaName", SqlDbType.NVarChar);
			parameter.Value = area;
			command.Parameters.Add(parameter);
            var routeParam = new SqlParameter("@RouteName",SqlDbType.NVarChar);
            routeParam.Value = route;
            command.Parameters.Add(routeParam);
            var orderStartDateParam = new SqlParameter("@OrderStartDate", SqlDbType.DateTime);
            orderStartDateParam.Value = orderStartDate;
            command.Parameters.Add(orderStartDateParam);
            var orderEndDateParam = new SqlParameter("@OrderEndDate", SqlDbType.DateTime);
            orderEndDateParam.Value = orderEndDate;
            command.Parameters.Add(orderEndDateParam);
			List<StatisticArea> list = new List<StatisticArea>();
		
			try
			{
				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				da.Fill(dt);
				if (dt != null && dt.Rows.Count > 0)
				{
					foreach(DataRow row in dt.Rows)
					{
						var model = new StatisticArea();
						model.Area = row["Area"].ToString();
						model.SendCount =System.Convert.ToInt64(row["SendCount"]);
						model.ReceiveCount = System.Convert.ToInt64(row["ReceiveCount"]);
						list.Add(model);
					}
					
				}
			}
			catch
			{ }
			return list;
	    }

	    public List<Activity> GetActivityCount(string userName, string phoneNumber, string area, string route,
	        DateTime? orderStartDate, DateTime? orderEndDate)
	    {
            var connection = new SqlConnection(connStr);
            var sql = @"dbo.[public_ActivityAreaDateUserReport]";
            var command = new SqlCommand(sql, connection);
            command.CommandType = CommandType.StoredProcedure;
            var paraUserName = new SqlParameter("@UserName", SqlDbType.NVarChar);
            var paraPhone = new SqlParameter("@PhoneNumber", SqlDbType.NVarChar);
	        var paraArea = new SqlParameter("@Area",SqlDbType.VarChar);
	        var paraRoute = new SqlParameter("@Route",SqlDbType.VarChar);
	        var paraStartDate = new SqlParameter("@OrderStartDate", SqlDbType.DateTime);
	        var paraEndDate = new SqlParameter("@OrderEndDate",SqlDbType.DateTime);
	        paraArea.Value = area;
	        paraRoute.Value = route;
	        paraStartDate.Value = orderStartDate;
	        paraEndDate.Value = orderEndDate;
            paraUserName.Value = userName;
            paraPhone.Value = phoneNumber;
            command.Parameters.Add(paraUserName);
            command.Parameters.Add(paraPhone);
	        command.Parameters.Add(paraArea);
	        command.Parameters.Add(paraRoute);
	        command.Parameters.Add(paraStartDate);
	        command.Parameters.Add(paraEndDate);
            List<Activity> list = new List<Activity>();

            try
            {
                var da = new SqlDataAdapter(command);
                var dt = new DataTable();
                da.Fill(dt);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        var model = new Activity();
                        model.UserID = row["UserID"].ToString();
                        model.UserName = row["UserName"].ToString();
                        model.PhoneNumber = row["PhoneNumber"].ToString();
                        model.ActivityCount = System.Convert.ToInt32(row["ActivityCount"]);
                        list.Add(model);
                    }

                }
            }
            catch
            { }
            return list;
	    }
		public List<Activity> GetActivityCount(string userName,string phoneNumber)
		{
			var connection = new SqlConnection(connStr);
			var sql = @"dbo.[public_ActivityUserReport]";
			var command = new SqlCommand(sql, connection);
			command.CommandType = CommandType.StoredProcedure;
			var paraUserName = new SqlParameter("@UserName", SqlDbType.NVarChar);
			var paraPhone = new SqlParameter("@PhoneNumber", SqlDbType.NVarChar);
			paraUserName.Value = userName;
			paraPhone.Value = phoneNumber;
			command.Parameters.Add(paraUserName);
			command.Parameters.Add(paraPhone);
			List<Activity> list = new List<Activity>();

			try
			{
				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				da.Fill(dt);
				if (dt != null && dt.Rows.Count > 0)
				{
					foreach (DataRow row in dt.Rows)
					{
						var model = new Activity();
						model.UserID = row["UserID"].ToString();
						model.UserName = row["UserName"].ToString();
						model.PhoneNumber = row["PhoneNumber"].ToString();
						model.ActivityCount = System.Convert.ToInt32(row["ActivityCount"]);
						list.Add(model);
					}

				}
			}
			catch
			{ }
			return list;
		}

		public List<IntrouduceGroupCount> GetGroupIntroduceCount()
		{
			var connection = new SqlConnection(connStr);
			var sql = @"dbo.[Public_GroupIntroduceReport]";
			var command = new SqlCommand(sql, connection);
			command.CommandType = CommandType.StoredProcedure;
			List<IntrouduceGroupCount> list = new List<IntrouduceGroupCount>();
			try
			{
				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				da.Fill(dt);
				if (dt != null && dt.Rows.Count > 0)
				{
					foreach (DataRow row in dt.Rows)
					{
						var model = new IntrouduceGroupCount();
						model.GroupName = row["GroupName"].ToString();
						model.IntroduceCount = System.Convert.ToInt32(row["IntroduceCount"]);
						list.Add(model);
					}

				}
			}
			catch
			{ }

			return list;
		}

		public List<IntrouducePersonCount> GetIntroducePersonCount(string userName,string phoneNumber)
		{
			var connection = new SqlConnection(connStr);
			var sql = @"SELECT 
							p.UserName,
							p.PhoneNumber,
							Count(c.ID) IntroduceCount
						FROM
							[IntroducePerson] p
							Left JOIN [Customers] c
							ON c.[IntroducePersonId] = p.ID
                       WHERE p.UserName like @UserName And p.PhoneNumber like @PhoneNumber
						Group BY p.UserName,p.PhoneNumber";
			var command = new SqlCommand(sql, connection);

			command.CommandType = CommandType.Text;
			var paraUserName = new SqlParameter("@UserName", SqlDbType.NVarChar);
			var paraPhone = new SqlParameter("@PhoneNumber", SqlDbType.NVarChar);
			paraUserName.Value = "%" + userName + "%";
			paraPhone.Value = "%" + phoneNumber + "%";
			command.Parameters.Add(paraUserName);
			command.Parameters.Add(paraPhone);
			List<IntrouducePersonCount> list = new List<IntrouducePersonCount>();
			try
			{
				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				da.Fill(dt);
				if (dt != null && dt.Rows.Count > 0)
				{
					foreach (DataRow row in dt.Rows)
					{
						var model = new IntrouducePersonCount();
						model.UserName = row["UserName"].ToString();
						model.PhoneNumber = row["PhoneNumber"].ToString();
						model.IntroduceCount = System.Convert.ToInt32(row["IntroduceCount"]);
						list.Add(model);
					}
				}
			}
			catch
			{ }

			return list;
		}

		public List<AreaTrendCount> GetAreaTrendCount(string area, DateTime date)
		{
			var connection = new SqlConnection(connStr);
			var sql = @"dbo.[Public_AreaTrendReport]";
			var command = new SqlCommand(sql, connection);
			command.CommandType = CommandType.StoredProcedure;
			var parea = new SqlParameter("@AreaName", SqlDbType.NVarChar);
			var pdate = new SqlParameter("@BeginDate", SqlDbType.DateTime);
			parea.Value = area;
			pdate.Value = date;
			command.Parameters.Add(parea);
			command.Parameters.Add(pdate);
			List<AreaTrendCount> list = new List<AreaTrendCount>();
			try
			{
				var da = new SqlDataAdapter(command);
				var dt = new DataTable();
				da.Fill(dt);
				if (dt != null && dt.Rows.Count > 0)
				{
					foreach (DataRow row in dt.Rows)
					{
						var model = new AreaTrendCount();
						model.Date= System.Convert.ToDateTime(row["OrderDate"]).ToString("yyyy-MM-dd");
						model.SendCount = System.Convert.ToInt32(row["SendCount"]);
						model.ReceiveCount = System.Convert.ToInt32(row["ReceiveCount"]);
						list.Add(model);
					}
				}
			}
			catch
			{ }

			return list;
		}
	}
}