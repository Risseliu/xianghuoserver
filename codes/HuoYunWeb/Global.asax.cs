﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
//using System.Web.Optimization;
using System.Web.Routing;
using Fw.Extends;
using Fw.Web;
using HuoYunWeb.App_Code;
using HuoYunWeb.Migrations;
using HuoYunWeb.Models;

namespace HuoYunWeb
{
	// 注意: 有关启用 IIS6 或 IIS7 经典模式的说明，
	// 请访问 http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : HttpApplication
	{
		public static string M_SN = null;
		public static string M_PWD = null;
		public static string M_Province = null;
		public static string M_City = null;
		public static string M_Trade = null;
		public static string M_Entname = null;
		public static string M_Linkman = null;
		public static string M_Phone = null;
		public static string M_Mobile = null;
		public static string M_Email = null;
		public static string M_Fax = null;
		public static string M_Address = null;
		public static string M_Postcode = null;
		public static string M_Sign = null;
		public static string M_Template = null;
		public static string M_Template1 = null;
		public static string M_SendSuccessTemplate = null;
		public static string BaiduServerApi = null;
		public static string CurrentCity = null;
		public static string DriverVersion = null;
		public static string DriverUpdatePath = null;
		public static string DriverUpdateLog = null;
		public static string CustomerVersion = null;
		public static string CustomerUpdatePath = null;
		public static string CustomerUpdateLog = null;
		public static double Range = 0;
		public static int OverTime = 0;
		public static string RootPath;
		public static string PublicValidCode = string.Empty;
		public static int ExpirateEndMinutes = 5;
		public static int ExpirateStartMinutes = 1440;
		public static string M_ConfirmDriverTemplate = null;
		public static string M_PublishTemplate = null;
		public static string M_CancelTemplate = null;
		public static string IOSApiKey = null;
		public static string IOSSecretKey = null;
		protected void Application_Start()
		{
			PushService.Start(WebConfigs.GetConfig("P_ApiKey"), WebConfigs.GetConfig("P_SecretKey"));
			Range = WebConfigs.GetConfig("Range").ToInt(50 * 1000);
			OverTime = WebConfigs.GetConfig("OverTime").ToInt(3);
			M_SN = WebConfigs.GetConfig("M_SN");
			BaiduServerApi = WebConfigs.GetConfig("Baidu_Server_ApiKey");
			IOSApiKey = WebConfigs.GetConfig("IOSApiKey");
			IOSSecretKey = WebConfigs.GetConfig("IOSSecretKey");
			CurrentCity = WebConfigs.GetConfig("Current_City");
			DriverVersion = WebConfigs.GetConfig("DriverVersion");
			DriverUpdatePath = WebConfigs.GetConfig("DriverUpdatePath");
			DriverUpdateLog = WebConfigs.GetConfig("DriverUpdateLog");
			CustomerVersion = WebConfigs.GetConfig("CustomerVersion");
			CustomerUpdatePath = WebConfigs.GetConfig("CustomerUpdatePath");
			CustomerUpdateLog = WebConfigs.GetConfig("CustomerUpdateLog");
			M_Template = WebConfigs.GetConfig("M_Template");
			M_Template1 = WebConfigs.GetConfig("M_Template1");
			M_SendSuccessTemplate = WebConfigs.GetConfig("M_SendSuccessTemplate");
			M_ConfirmDriverTemplate = WebConfigs.GetConfig("M_ConfirmDriverTemplate");
			M_PublishTemplate = WebConfigs.GetConfig("M_PublishTemplate");
			M_CancelTemplate = WebConfigs.GetConfig("M_CancelTemplate");
			M_PWD = WebConfigs.GetConfig("M_PWD");
			M_Province = WebConfigs.GetConfig("M_Province");
			M_City = WebConfigs.GetConfig("M_City");
			M_Trade = WebConfigs.GetConfig("M_Trade");
			M_Entname = WebConfigs.GetConfig("M_Entname");
			M_Linkman = WebConfigs.GetConfig("M_Linkman");
			M_Phone = WebConfigs.GetConfig("M_Phone");
			M_Mobile = WebConfigs.GetConfig("M_Mobile");
			M_Email = WebConfigs.GetConfig("M_Email");
			M_Fax = WebConfigs.GetConfig("M_Fax");
			M_Address = WebConfigs.GetConfig("M_Address");
			M_Postcode = WebConfigs.GetConfig("M_Postcode");
			M_Sign = WebConfigs.GetConfig("M_Sign");
			M_Sign = WebConfigs.GetConfig("M_Sign");
			PublicValidCode = WebConfigs.GetConfig("PublicValidCode");
			string exm =  WebConfigs.GetConfig("ExpirateEndMinutes");
			if (!string.IsNullOrEmpty(exm))
			{
				ExpirateEndMinutes = System.Convert.ToInt32(exm);
			}

			exm = WebConfigs.GetConfig("ExpirateStartMinutes");
			if (!string.IsNullOrEmpty(exm))
			{
				ExpirateStartMinutes = System.Convert.ToInt32(exm);
			}
			RootPath = Server.MapPath("~/");

			//Database.SetInitializer(new MigrateDatabaseToLatestVersion<SYSContext, Configuration>());
			// 关闭对数据库的修改
			Database.SetInitializer<SYSContext>(null);
			AreaRegistration.RegisterAllAreas();
			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			//            BundleConfig.RegisterBundles(BundleTable.Bundles);
			log4net.Config.XmlConfigurator.Configure();
			AuthConfig.RegisterAuth();
			ValidCodeAPI.UnRegister();
		}
	}
}