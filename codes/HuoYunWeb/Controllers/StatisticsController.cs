﻿using Fw.Entity;
using HuoYunBLL;
using HuoYunWeb.Models;
using HuoYunWeb.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace HuoYunWeb.Controllers
{
	public class StatisticsController : Controller
	{
		//
		// GET: /Statistics/

		public ActionResult Area()
		{
			return View();
		}

		public ActionResult AreaTrend(string name)
		{
			ViewBag.ArearName = name;
			return View();
		}

        public ActionResult SearchArea(string area, string route, DateTime? orderStartDate, DateTime? orderEndDate)
		{
			var service = new StatisticsService();

			//var result = service.GetAreaSendReceive(area);
            string tempArea = string.IsNullOrWhiteSpace(area) ? "" : (area.Equals("全部")?"":area);
            string tempRoute = string.IsNullOrWhiteSpace(route) ? "" : (route.Equals("全部")?"":route);
            var result = service.GetAreaRouteSendReceive(tempArea, tempRoute, orderStartDate, orderEndDate);
			return new JsonResult { Data = new { Success = true, Items = result }, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

	    public ActionResult GetAllArea()
	    {
            using (var a = new SYSContext())
            {
                var areas = a.Areas.ToList();
                return Result.GetSuccess(areas);
            }
	    }

	    public ActionResult GetRoutesByAreaCode(string areaCode)
	    {
	        using (var a = new SYSContext())
	        {
	            var startRoutes = (from c in a.Routes.Where(w => w.AreaCode == areaCode) select c.Start).Distinct().ToList<string>();
                var endRoutes = (from c in a.Routes.Where(w => w.AreaCode == areaCode) select c.End).Distinct().ToList<string>();
	            var list = new List<Route>();
	            var strList = new List<string>();
                for (int i = 0; i < startRoutes.Count; i++)
	            {
                    Route route = new Route { Start = startRoutes[i] };
	                strList.Add(startRoutes[i]);
	                list.Add(route);
	            }
	            for (int i = 0; i < endRoutes.Count; i++)
	            {
	                if (!strList.Contains(endRoutes[i]))
	                {
	                    Route route = new Route {Start = endRoutes[i]};
	                    list.Add(route);
	                }
	            }
                return Result.GetSuccess(list);
	        }
	    }
		public ActionResult Activity(
		  string userName,
		  string phoneNumber,
          string area,
          string route,
          DateTime? orderStartDate,
          DateTime? orderEndDate,
		  int pageNum = 1,
		  int numPerPage = 20)
		{
			var service = new StatisticsService();
			var activitys = service.GetActivityCount(userName, phoneNumber,area,route,orderStartDate,orderEndDate);

			var filter = activitys.Skip((pageNum - 1) * numPerPage)
					.Take(numPerPage)
					.ToList();

			var total = activitys.Count();
			var result = new ListPageModel<Activity>();

			result.List = filter;
			result.Page = new PageInfo
			{
				TotalRecord = total,
				PageIndex = pageNum,
				PageSize = numPerPage,
				TotalPage = (total + numPerPage - 1) / numPerPage
			};

			return View(result);
		}

		public ActionResult Introduce()
		{
			return View();
		}

		public ActionResult GetGroupIntroduceCount()
		{
			var service = new StatisticsService();
			var list = service.GetGroupIntroduceCount();
			string[] groupName = new string[list.Count];
			int[] count = new int[list.Count];
			for (int i = 0; i < list.Count; i++)
			{
				groupName[i] = list[i].GroupName;
				count[i] = list[i].IntroduceCount;
			}
			return new JsonResult { Data = new { Success = true, GroupNames = groupName, Counts = count }, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public ActionResult GetIntroducePersonCount(string userName, string phoneNumber)
		{
			var service = new StatisticsService();
			var result = service.GetIntroducePersonCount(userName, phoneNumber);
			return new JsonResult { Data = new { Success = true, Items = result }, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public ActionResult GetAreaTrendCount(string areaName)
		{
			var service = new StatisticsService();

			var result = service.GetAreaTrendCount(areaName, DateTime.Now.Date);

			string[] date = new string[result.Count];
			int[] sendCount = new int[result.Count];
			int[] receiveCount = new int[result.Count];
			for (int i = 0; i < result.Count; i++)
			{
				date[i] = result[i].Date;
				sendCount[i] = result[i].SendCount;
				receiveCount[i] = result[i].ReceiveCount;
			}
			return new JsonResult { Data = new { Success = true, Dates = date, SendCount = sendCount, ReceiveCount = receiveCount }, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}


	}
}
