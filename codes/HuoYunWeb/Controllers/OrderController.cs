﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DAL.Control;
using EntityFramework.Extensions;
using Fw.ActionMethod;
using Fw.Entity;
using Fw.Serializer;
using HuoYunWeb.App_Code;
using HuoYunWeb.Models;
using System.Collections.ObjectModel;
using System.IO;
using NPOI;

namespace HuoYunWeb.Controllers
{

	[WebAuthorize]
	public class OrderController : BaseControllerx<Order>
	{
		public ActionResult List(
		   string userName,
		   string phoneNumber,
           DateTime? orderStartDate,
           DateTime? orderEndDate,
           string areaCode="",
           string routeName="",
		   int orderTime = 0,
           int orderFrom=0,
		   int pageNum = 1,
			   int numPerPage = 20,
			   string orderField = "",
			   string driverName = "",
			   string status = "")
		{
			return SYSContext.Run(w =>
			{
				w.Configuration.LazyLoadingEnabled = false;
				w.Configuration.ProxyCreationEnabled = false;
				var queryable = w.Orders.AsQueryable();
				if (!string.IsNullOrEmpty(driverName))
				{
					queryable = (from a in w.Orders
								 from b in a.DriverRelOrders
								 where b.Driver.User.PhoneNumber.Contains(driverName) ||
									   b.Driver.User.UserName.Contains(driverName)
								 select a).Distinct();
				}
			    if (!string.IsNullOrEmpty(areaCode)) queryable = queryable.Where(z=>z.AreaCode==areaCode);
			    if (!string.IsNullOrEmpty(routeName)) queryable = queryable.Where(z=>z.StartName.Contains(routeName)||z.EndName.Contains(routeName));
				if (userName != null) queryable = queryable.Where(z => z.GuestUser.UserName.Contains(userName));
				if (phoneNumber != null) queryable = queryable.Where(z => z.GuestUser.PhoneNumber.Contains(phoneNumber));
                if (orderStartDate != null) queryable = queryable.Where(z => z.PublishTime >= orderStartDate);
                if (orderEndDate != null) queryable = queryable.Where(z=>z.PublishTime<=orderEndDate);
                if (orderFrom > 0) queryable = queryable.Where(z=>z.From==orderFrom);
				DateTime dateNow = DateTime.Now;
				DateTime queryStartTime = dateNow.AddMinutes(-10);
				DateTime queryEndTime = dateNow.AddMinutes(-5);

				if (orderTime == 1)
				{
					queryable = queryable.Where(z => z.PublishTime > queryEndTime);
				}
				else if (orderTime == 2)
				{
					queryable = queryable.Where(z => z.PublishTime >= queryStartTime && z.PublishTime <= queryEndTime);
				}
				else if (orderTime == 3)
				{
					queryable = queryable.Where(z => z.PublishTime < queryStartTime);
				}

				if (string.IsNullOrEmpty(orderField)) queryable = queryable.OrderByDescending(z => z.PublishTime);
				else
				{
					if (orderField.IndexOf(" ", System.StringComparison.Ordinal) == -1)
					{
						orderField += " asc";
					}
					var strs = orderField.Split(' ');
					if (strs.Length == 2)
					{
						queryable = GetOrderBy(queryable, new OrderEntity[]
                        {
                            new OrderEntity
                            {
                                OrderType = strs[1].ToLower() == "desc" ? OrderType.Desc : OrderType.Asc,
                                PropName = strs[0],
                            }
                        });
					}
					else
					{
						queryable = queryable.OrderByDescending(z => z.PublishTime);
					}
				}
				var list = queryable

					.Skip((pageNum - 1) * numPerPage)
					.Take(numPerPage)
					.Select(z => new OrderListItem
					{
						ID = z.ID,
						OrderNumber = z.OrderNumber,
						UserName = z.GuestUser.UserName,
						Price = z.Price,
						PhoneNumber = z.GuestUser.PhoneNumber,
						PublishTime = z.PublishTime,
						GuestUserID = z.GuestUser.ID,
						OrderState = z.OrderState,
                        OrderFrom=(int)(z.From==null?0:z.From),
						Drivers = z.DriverRelOrders.Select(x => new OrderDriverItem
						{
							PhoneNumber = x.Driver.User.PhoneNumber,
							UserName = x.Driver.User.UserName,
							OrderState = x.OrderState,
							EnsureState = x.EnsureState,
						}).ToList(),
					});
				var total = queryable.FutureCount();//(z => z.ValidSubmit == 1);
				var re = new HuoYunBLL.ListPageModel<OrderListItem>();
				var orderListItems = list.Future();
				re.List = orderListItems.ToList();
				re.Page = new PageInfo
				{
					TotalRecord = total,
					PageIndex = pageNum,
					PageSize = numPerPage,
					TotalPage = (total + numPerPage - 1) / numPerPage
				};
			    ViewBag.ExportData = true;
				return View(re);
			});
		}
        public ActionResult Export(
		   string userName,
		   string phoneNumber,
           DateTime? orderStartDate,
           DateTime? orderEndDate,
           string areaCode="",
           string routeName="",
		   int orderTime = 0,
           int orderFrom=0,
			   string orderField = "",
			   string driverName = "",
			   string status = "")
		{
            using (var w = new SYSContext())
            {
                 w.Configuration.LazyLoadingEnabled = false;
			    w.Configuration.ProxyCreationEnabled = false;
			    var queryable = w.Orders.AsQueryable();
			    if (!string.IsNullOrEmpty(driverName))
			    {
			        queryable = (from a in w.Orders
			            from b in a.DriverRelOrders
			            where b.Driver.User.PhoneNumber.Contains(driverName) ||
			                  b.Driver.User.UserName.Contains(driverName)
			            select a).Distinct();
			    }
			    if (!string.IsNullOrEmpty(areaCode)) queryable = queryable.Where(z => z.AreaCode == areaCode);
			    if (!string.IsNullOrEmpty(routeName))
			        queryable = queryable.Where(z => z.StartName.Contains(routeName) || z.EndName.Contains(routeName));
			    if (userName != null) queryable = queryable.Where(z => z.GuestUser.UserName.Contains(userName));
			    if (phoneNumber != null) queryable = queryable.Where(z => z.GuestUser.PhoneNumber.Contains(phoneNumber));
			    if (orderStartDate != null) queryable = queryable.Where(z => z.PublishTime >= orderStartDate);
			    if (orderEndDate != null) queryable = queryable.Where(z => z.PublishTime <= orderEndDate);
			    if (orderFrom > 0) queryable = queryable.Where(z => z.From == orderFrom);
			    DateTime dateNow = DateTime.Now;
			    DateTime queryStartTime = dateNow.AddMinutes(-10);
			    DateTime queryEndTime = dateNow.AddMinutes(-5);

			    if (orderTime == 1)
			    {
			        queryable = queryable.Where(z => z.PublishTime > queryEndTime);
			    }
			    else if (orderTime == 2)
			    {
			        queryable = queryable.Where(z => z.PublishTime >= queryStartTime && z.PublishTime <= queryEndTime);
			    }
			    else if (orderTime == 3)
			    {
			        queryable = queryable.Where(z => z.PublishTime < queryStartTime);
			    }

			    if (string.IsNullOrEmpty(orderField)) queryable = queryable.OrderByDescending(z => z.PublishTime);
			    else
			    {
			        if (orderField.IndexOf(" ", System.StringComparison.Ordinal) == -1)
			        {
			            orderField += " asc";
			        }
			        var strs = orderField.Split(' ');
			        if (strs.Length == 2)
			        {
			            queryable = GetOrderBy(queryable, new OrderEntity[]
			            {
			                new OrderEntity
			                {
			                    OrderType = strs[1].ToLower() == "desc" ? OrderType.Desc : OrderType.Asc,
			                    PropName = strs[0],
			                }
			            });
			        }
			        else
			        {
			            queryable = queryable.OrderByDescending(z => z.PublishTime);
			        }
			    }

			    var list = queryable.Include(x=>x.GuestUser).ToList();
                //创建Excel文件的对象
                NPOI.HSSF.UserModel.HSSFWorkbook book = new NPOI.HSSF.UserModel.HSSFWorkbook();
                //添加一个sheet
                NPOI.SS.UserModel.ISheet sheet1 = book.CreateSheet("Sheet1");
                //给sheet1添加第一行的头部标题
                NPOI.SS.UserModel.IRow row1 = sheet1.CreateRow(0);
                row1.CreateCell(0).SetCellValue("订单编号");
                row1.CreateCell(1).SetCellValue("起始地");
                row1.CreateCell(2).SetCellValue("目的地");
                row1.CreateCell(3).SetCellValue("货物");
                row1.CreateCell(4).SetCellValue("价格");
                row1.CreateCell(5).SetCellValue("下单方式");
                row1.CreateCell(6).SetCellValue("货主");
                row1.CreateCell(7).SetCellValue("货主电话");
                row1.CreateCell(8).SetCellValue("订单状态");
                row1.CreateCell(9).SetCellValue("发布时间");
                for (int i = 0; i < list.Count; i++)
                {
                    NPOI.SS.UserModel.IRow rowtemp = sheet1.CreateRow(i + 1);
                    rowtemp.CreateCell(0).SetCellValue(list[i].OrderNumber);
                    rowtemp.CreateCell(1).SetCellValue(list[i].StartName);
                    rowtemp.CreateCell(2).SetCellValue(list[i].EndName);
                    rowtemp.CreateCell(3).SetCellValue(list[i].Lading);
                    if(list[i].IsMianYi==1)rowtemp.CreateCell(4).SetCellValue("面议");
                    else rowtemp.CreateCell(4).SetCellValue(list[i].Price+"元");
                    if (list[i].From == null) list[i].From = 0;
                    rowtemp.CreateCell(5).SetCellValue(GetOrderFromName(list[i].From));
                    rowtemp.CreateCell(6).SetCellValue(list[i].GuestUser.UserName);
                    rowtemp.CreateCell(7).SetCellValue(list[i].GuestUser.PhoneNumber);
                    rowtemp.CreateCell(8).SetCellValue(GetOrderStateName(list[i].OrderState));
                    DateTime tempDate = (DateTime)(list[i].PublishTime==null?DateTime.Now:list[i].PublishTime);
                    rowtemp.CreateCell(9).SetCellValue(tempDate.ToString("yyyy-MM-dd HH:mm"));
                }
                string xlsFileName = DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
               // using (FileStream fs = System.IO.File.OpenWrite(Path.Combine(MvcApplication.RootPath, "download") + "/" + xlsFileName)) //打开一个xls文件，如果没有则自行创建，如果存在myxls.xls文件则在创建是不要打开该文件！
               // {
               //     book.Write(fs);   //向打开的这个xls文件中写入mySheet表并保存。
               //     return  File(Path.Combine(MvcApplication.RootPath, "download") + "/" + xlsFileName);
               // }
                // 写入到客户端 
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                book.Write(ms);
                ms.Seek(0, SeekOrigin.Begin);

               return File(ms, "application/vnd.ms-excel", xlsFileName);
               return new EmptyResult();
            }
            
		}
        private  string GetOrderStateName(int orderState)
        {
            var state = "--";
            switch (orderState)
            {
                case 0: state = "新订单"; break;
                case 1: state = "已接受"; break;
                case 2: state = "选定车主"; break;
                case 3: state = "车主已接货"; break;
                case 4: state = "发货中"; break;
                case 5: state = "交易完成"; break;
                case 7: state = "车主已接货"; break;
                case -6: state = "取消"; break;
                case -7: state = "失效"; break;
            }
            return state;
        }

        private  string GetOrderFromName(int? from)
        {
            var state = "--";
            switch (from)
            {
                case 1: state = "手机下单"; break;
                case 2: state = "微信下单"; break;
                case 3: state = "电话下单"; break;
            }
            return state;
        }
        public ActionResult AgentOrderList(
           string userName,
           string phoneNumber,
           DateTime? orderStartDate,
           DateTime? orderEndDate,
           int orderFrom = 0,
           int pageNum = 1,
               int numPerPage = 20,
               string orderField = "",
               string driverName = "",
               string status = "")
        {
            return SYSContext.Run(w =>
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                var queryable = w.Orders.AsQueryable();
                var currentUser = UserHelper.CurrentUser();
                if (currentUser != null)
                {
                    var areaList = w.AgentAreas.Where(z => z.AccountId == currentUser.ID).Select(z => z.AreaCode).ToList();
                    if (areaList != null&&areaList.Count>0) queryable = queryable.Where(z => areaList.Contains(z.AreaCode));
                    
                }
               // queryable = queryable.Where(z=>z.);
                if (!string.IsNullOrEmpty(driverName))
                {
                    queryable = (from a in w.Orders
                                 from b in a.DriverRelOrders
                                 where b.Driver.User.PhoneNumber.Contains(driverName) ||
                                       b.Driver.User.UserName.Contains(driverName)
                                 select a).Distinct();
                }
                if (userName != null) queryable = queryable.Where(z => z.GuestUser.UserName.Contains(userName));
                if (phoneNumber != null) queryable = queryable.Where(z => z.GuestUser.PhoneNumber.Contains(phoneNumber));
                if (orderStartDate != null) queryable = queryable.Where(z => z.PublishTime >= orderStartDate);
                if (orderEndDate != null) queryable = queryable.Where(z => z.PublishTime <= orderEndDate);
                if (orderFrom > 0) queryable = queryable.Where(z => z.From == orderFrom);

                if (string.IsNullOrEmpty(orderField)) queryable = queryable.OrderByDescending(z => z.PublishTime);
                else
                {
                    if (orderField.IndexOf(" ", System.StringComparison.Ordinal) == -1)
                    {
                        orderField += " asc";
                    }
                    var strs = orderField.Split(' ');
                    if (strs.Length == 2)
                    {
                        queryable = GetOrderBy(queryable, new OrderEntity[]
                        {
                            new OrderEntity
                            {
                                OrderType = strs[1].ToLower() == "desc" ? OrderType.Desc : OrderType.Asc,
                                PropName = strs[0],
                            }
                        });
                    }
                    else
                    {
                        queryable = queryable.OrderByDescending(z => z.PublishTime);
                    }
                }
                var list = queryable

                    .Skip((pageNum - 1) * numPerPage)
                    .Take(numPerPage)
                    .Select(z => new OrderListItem
                    {
                        ID = z.ID,
                        OrderNumber = z.OrderNumber,
                        UserName = z.GuestUser.UserName,
                        Price = z.Price,
                        PhoneNumber = z.GuestUser.PhoneNumber,
                        PublishTime = z.PublishTime,
                        GuestUserID = z.GuestUser.ID,
                        OrderState = z.OrderState,
                        OrderFrom = (int)(z.From == null ? 0 : z.From),
                        Drivers = z.DriverRelOrders.Select(x => new OrderDriverItem
                        {
                            PhoneNumber = x.Driver.User.PhoneNumber,
                            UserName = x.Driver.User.UserName,
                            OrderState = x.OrderState,
                            EnsureState = x.EnsureState,
                        }).ToList(),
                    });
                var total = queryable.FutureCount();//(z => z.ValidSubmit == 1);
                var re = new HuoYunBLL.ListPageModel<OrderListItem>();
                var orderListItems = list.Future();
                re.List = orderListItems.ToList();
                re.Page = new PageInfo
                {
                    TotalRecord = total,
                    PageIndex = pageNum,
                    PageSize = numPerPage,
                    TotalPage = (total + numPerPage - 1) / numPerPage
                };
                return View(re);
            });
        }
		public ActionResult HasNoResponseOrders()
		{
			using (var a = new SYSContext())
			{
				DateTime dateNow = DateTime.Now;
				DateTime queryEndTime = dateNow.AddMinutes(-MvcApplication.ExpirateEndMinutes);
				DateTime queryStartTime = dateNow.AddMinutes(-MvcApplication.ExpirateStartMinutes);
				var count = a.Orders.Count(z => z.PublishTime > queryStartTime && z.PublishTime < queryEndTime && z.OrderState == 0);
				return new JsonResult { Data = new { Count=count}, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
			}
		}

		public ActionResult NoResponseList(
		   int pageNum = 1,
			   int numPerPage = 20,
			   string orderField = "")
		{
			return SYSContext.Run(w =>
			{
				w.Configuration.LazyLoadingEnabled = false;
				w.Configuration.ProxyCreationEnabled = false;
				var queryable = w.Orders.AsQueryable();
				DateTime dateNow = DateTime.Now;
				DateTime queryEndTime = dateNow.AddMinutes(-MvcApplication.ExpirateEndMinutes);
				DateTime queryStartTime = dateNow.AddMinutes(-MvcApplication.ExpirateStartMinutes);
				queryable = queryable.Where(z => z.PublishTime > queryStartTime && z.PublishTime < queryEndTime && z.OrderState == 0);

				if (string.IsNullOrEmpty(orderField))
				{
					queryable = queryable.OrderByDescending(z => z.PublishTime);
				}
				else
				{
					if (orderField.IndexOf(" ", System.StringComparison.Ordinal) == -1)
					{
						orderField += " asc";
					}
					var strs = orderField.Split(' ');
					if (strs.Length == 2)
					{
						queryable = GetOrderBy(queryable, new OrderEntity[]
						{
							new OrderEntity
							{
								OrderType = strs[1].ToLower() == "desc" ? OrderType.Desc : OrderType.Asc,
								PropName = strs[0],
							}
						});
					}
					else
					{
						queryable = queryable.OrderByDescending(z => z.PublishTime);
					}
				}

				var list = queryable

					.Skip((pageNum - 1) * numPerPage)
					.Take(numPerPage)
					.Select(z => new OrderListItem
					{
						ID = z.ID,
						OrderNumber = z.OrderNumber,
						UserName = z.GuestUser.UserName,
						Price = z.Price,
						PhoneNumber = z.GuestUser.PhoneNumber,
						PublishTime = z.PublishTime,
						GuestUserID = z.GuestUser.ID,
						OrderState = z.OrderState,
						Drivers = z.DriverRelOrders.Select(x => new OrderDriverItem
						{
							PhoneNumber = x.Driver.User.PhoneNumber,
							UserName = x.Driver.User.UserName,
							OrderState = x.OrderState,
							EnsureState = x.EnsureState,
						}).ToList(),
					});
				var total = queryable.FutureCount();//(z => z.ValidSubmit == 1);
				var re = new HuoYunBLL.ListPageModel<OrderListItem>();
				var orderListItems = list.Future();
				re.List = orderListItems.ToList();
				re.Page = new PageInfo
				{
					TotalRecord = total,
					PageIndex = pageNum,
					PageSize = numPerPage,
					TotalPage = (total + numPerPage - 1) / numPerPage
				};
				return View(re);
			});
		}

		public ActionResult Detail(Guid OrderID)
		{
			return SYSContext.Run(w =>
			{
				w.Configuration.LazyLoadingEnabled = false;
				w.Configuration.ProxyCreationEnabled = false;
				var item = w.Orders.Include(z => z.GuestUser).FirstOrDefault(z => z.ID == OrderID);
				ViewBag.customerName = item.GuestUser.UserName;
				return View(item);
			});
		}
		public ActionResult EditView(Guid OrderID)
		{
			return SYSContext.Run(w =>
			{
				w.Configuration.LazyLoadingEnabled = false;
				w.Configuration.ProxyCreationEnabled = false;
				var item = w.Orders.Include(z => z.GuestUser).Include(z => z.DriverRelOrders).FirstOrDefault(z => z.ID == OrderID);
				if (item.OrderState > 1)
				{
					var rel = item.DriverRelOrders.Where(z => z.EnsureState == 1).FirstOrDefault();
					var user = w.Users.Where(z => z.ID == rel.DriverID).FirstOrDefault();
					ViewBag.DriverName = user.UserName;
					ViewBag.DriverID = user.ID;
				}
				else
				{
					ViewBag.DriverName = "";
					ViewBag.DriverID = "";
				}
				ViewBag.customerName = item.GuestUser.UserName;
				return View(item);
			});
		}
		[HttpPost]
		public ActionResult Edit(Guid? ID, Guid? driverID, int changeOrderState)
		{
			using (var a = new SYSContext())
			{
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				var item = a.Orders.Include(z => z.DriverRelOrders).Where(w => w.ID == ID).FirstOrDefault();
				if (driverID != null)
				{
					var rel = item.DriverRelOrders.Where(w => w.DriverID == driverID).FirstOrDefault();
					var driver = a.Drivers.Where(w => w.ID == driverID).FirstOrDefault();
					item.OrderState = 2;
					if (changeOrderState == 2) item.OrderState = 5;
					else if (changeOrderState == 3) item.OrderState = -7;
					if (rel == null)
					{
						if (item.DriverRelOrders == null)
						{
							item.DriverRelOrders = new Collection<DriverRelOrder>();
							item.DriverRelOrders.Add(new DriverRelOrder
							{
								ID = Guid.NewGuid(),
								Driver = driver,
								DriverID = driver.ID,
								OrderID = item.ID,
								Order = item,
								OrderState = item.OrderState,
								EnsureState = 1
							});
						}
						else
						{
							var relList = item.DriverRelOrders.Where(w => w.DriverID != driverID).ToList();
							if (changeOrderState == 1)
							{
								relList.ForEach(z =>
								{
									z.EnsureState = 0;
									z.OrderState = -3;
								});
							}
							else if (changeOrderState == 2)
							{
								relList.ForEach(z =>
								{
									z.EnsureState = 0;
									z.OrderState = -3;
								});
							}
							else if (changeOrderState == 3)
							{
								relList.ForEach(z =>
								{
									z.EnsureState = 0;
									z.OrderState = -7;
								});
							}
							if (item.OrderState >= 0 && item.OrderState < 2)
							{
								item.DriverRelOrders.Add(new DriverRelOrder
								{
									ID = Guid.NewGuid(),
									Driver = driver,
									DriverID = driver.ID,
									OrderID = item.ID,
									Order = item,
									OrderState = item.OrderState,
									EnsureState = 1
								});
							}
							else if (item.OrderState >= 2)
							{
								var driverRel = item.DriverRelOrders.Where(w => w.DriverID == driverID).FirstOrDefault();
								if (driverRel == null)
								{
									item.DriverRelOrders.Add(new DriverRelOrder
									{
										ID = Guid.NewGuid(),
										Driver = driver,
										DriverID = driver.ID,
										OrderID = item.ID,
										Order = item,
										OrderState = item.OrderState,
										EnsureState = 1
									});
								}
								else
								{
									driverRel.OrderState = item.OrderState;
									driverRel.Order = item;
									driverRel.EnsureState = 1;
								}

							}
						}

					}
					else
					{
						var relList = item.DriverRelOrders.Where(w => w.DriverID != driverID).ToList();
						relList.ForEach(z =>
						{
							z.EnsureState = 0;
							z.OrderState = -3;
						});
						rel.OrderState = item.OrderState;
						rel.EnsureState = 1;
					}
				}
				else
				{
					if (changeOrderState == 2) return HuoYunBLL.Result.GetError("完成订单必须指定车主");
					if (changeOrderState == 3)
					{
						var relList = item.DriverRelOrders.ToList();
						if (relList != null) relList.ForEach(z =>
							{
								z.EnsureState = 0;
								z.OrderState = -7;
							});
						item.OrderState = -7;
					}
				}
				a.SaveChanges();
				return HuoYunBLL.Result.GetSuccess();
			}
		}
		[HttpPost]
		public ActionResult Detail(Guid orderID, String guestUserPhoneNumber)
		{
			using (var a = new SYSContext())
			{
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				var item = a.Orders.Include(z => z.DriverRelOrders).FirstOrDefault(z => z.ID == orderID);

				a.SaveChanges();
				return getDefaultJsonResult("Drivers");
			}
		}
		public JsonResult getDefaultJsonResult(string navTabId = null, string callbackType = null, string message = "保存成功", int statusCode = 200)
		{
			var c = navTabId ?? Request["NT"] ?? RouteData.GetController();
			return new JsonResult
			{
				Data = new
				{
					statusCode = statusCode,
					message = message,
					callbackType = callbackType ?? "closeCurrent",
					navTabId = c
				}
			};
		}
		public ActionResult Add()
		{
			return View();
		}
		[HttpPost]
		public ActionResult Add(string userPhone, string order, string selectPhone)
		{
			return View();
		}

	}
	public class OrderDriverItem
	{
		public string UserName { get; set; }
		public string PhoneNumber { get; set; }
		public int EnsureState { get; set; }
		public int OrderState { get; set; }

	}
	public class OrderListItem
	{
		public string UserName { get; set; }
		public string PhoneNumber { get; set; }
		public DateTime? PublishTime { get; set; }
		public Guid GuestUserID { get; set; }
		public int OrderState { get; set; }
		public double Price { get; set; }
		public List<OrderDriverItem> Drivers { get; set; }

		public string OrderNumber { get; set; }
		public Guid ID { get; set; }
        public int OrderFrom { get; set; }
	}
}
