﻿using HuoYunWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using EntityFramework.Extensions;
using Fw.Entity;
using HuoYunBLL;

namespace HuoYunWeb.Controllers
{
    public class RouteController :Controller
	{
		//
		// GET: /Route/

		public ActionResult Add()
		{
			return View();
		}

	    public ActionResult AreaAdd()
	    {
	        return View();
	    }

        public ActionResult AreaEdit(int Id)
        {
            return SYSContext.Run(w =>
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                var item = w.Areas.FirstOrDefault(z=>z.Id==Id);
                return View(item);
            });
        }
        public ActionResult EditArea(int ID, string Name, string Remark)
        {
            using (var w = new SYSContext())
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                var item = w.Areas.FirstOrDefault(z=>z.Id==ID);
                if (item != null)
                {
                    item.Name = Name;
                    item.Remark = Remark;
                }
                w.SaveChanges();
                return HuoYunBLL.Result.GetSuccess();
            }
        }
        public JsonResult getDefaultJsonResult(string navTabId = null, string callbackType = null, string message = "保存成功", int statusCode = 200)
        {
            var c = navTabId ?? Request["NT"] ?? RouteData.GetController();
            return new JsonResult
            {
                Data = new
                {
                    statusCode = statusCode,
                    message = message,
                    callbackType = callbackType ?? "closeCurrent",
                    navTabId = c
                }
            };
        }
        public ActionResult AreaList(
               string areaName,
               int pageNum = 1,
               int numPerPage = 20
               )
	    {
            using (var a = new SYSContext())
            {
                a.Configuration.LazyLoadingEnabled = false;
                a.Configuration.ProxyCreationEnabled = false;
                var queryable = a.Areas.AsQueryable();
                if (!string.IsNullOrWhiteSpace(areaName)) queryable = queryable.Where(w => w.Name.Contains(areaName));
                queryable = queryable.OrderByDescending(z => z.CreateTime);
                var filter = queryable.Skip((pageNum - 1) * numPerPage)
                    .Take(numPerPage)
                    .ToList();
                var total = queryable.Count();
                var result = new ListPageModel<Area>();

                result.List = filter;
                result.Page = new PageInfo
                {
                    TotalRecord = total,
                    PageIndex = pageNum,
                    PageSize = numPerPage,
                    TotalPage = (total + numPerPage - 1) / numPerPage
                };
                return View(result);
            }
	    }
        public ActionResult AddArea(string areaCode,string areaName, string areaRemark)
	    {
            if (string.IsNullOrEmpty(areaName))
            {
                return GetJsonResult(false,"请输入区域名称");
            }
            if (string.IsNullOrEmpty(areaCode))
            {
                return GetJsonResult(false,"请输入区域编码");
            }
            using (var a = new SYSContext())
            {
                var find = a.Areas.FirstOrDefault(x => x.Name == areaName);
                if (find != null)
                {
                    return GetJsonResult(false,"区域名称已经存在");
                }
                find = a.Areas.FirstOrDefault(x => x.Code == areaCode);
                if (find != null)
                {
                    return GetJsonResult(false,"区域编码已经存在");
                }
                var area = new Area() { Name = areaName, Remark = areaRemark,Code=areaCode, CreateTime = DateTime.Now };
                a.Areas.Add(area);
                a.SaveChanges();
                return GetJsonResult(true,string.Empty);
            }
	    }
        public ActionResult AddRoute(string start, string end, string areaCode)
		{
			if (string.IsNullOrEmpty(start))
			{
				return GetJsonResult(false, "请输入起始地");
			}

			if (string.IsNullOrEmpty(end))
			{
				return GetJsonResult(false, "请输入目的地");
			}

			using (var a = new SYSContext())
			{
				var find = a.Routes.FirstOrDefault(x => x.Start == start && x.End == end);
				if (find != null)
				{
					return GetJsonResult(false, "该路线已经存在");
				}
                var route = new Route() { Start = start, End = end, AreaCode = areaCode, City = MvcApplication.CurrentCity};
				a.Routes.Add(route);
				a.SaveChanges();
				return GetJsonResult(true, string.Empty);
			}
		}
		private JsonResult GetJsonResult(bool success, string message)
		{
			return new JsonResult { Data = new { Success = success, Message = message }, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
		}

		public ActionResult DriverRoute(string start, string end)
		{
			return View();
		}

		public ActionResult AddDriverRoute(long routeId, string driverId)
		{
			if (routeId == 0)
			{
				return GetJsonResult(false, "请选择路线");
			}

			if (string.IsNullOrEmpty(driverId))
			{
				return GetJsonResult(false, "请选择车主");
			}
            var driverIDs = driverId.Split(',').Select(w => w.ToString()).ToList();
		    if (driverIDs.Count == 0) return GetJsonResult(false,"请选择车主");
			using (var a = new SYSContext())
			{
			    foreach (var tempDriverId in driverIDs)
			    {
                    var find = a.DriverRoutes.FirstOrDefault(x => x.RouteId == routeId && x.DriverId == new Guid(tempDriverId));
                    if (find != null)
                    {
                        var tempUser = a.Users.FirstOrDefault(w => w.ID == new Guid(tempDriverId));
                        if(tempUser!=null)
                        return GetJsonResult(false, "该路线已经添加车主:"+tempUser.UserName+(tempUser.Gender==1?"先生":"女士"));
                        else return GetJsonResult(false, "该路线已经添加所选车主");
                        
                    }
                    var dr = new DriverRoute() { DriverId = new Guid(tempDriverId), RouteId = routeId };
                    a.DriverRoutes.Add(dr);
			    }
                
				a.SaveChanges();
				return GetJsonResult(true, string.Empty);
			}
		}

	}
}
