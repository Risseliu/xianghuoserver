﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using BaiDuAPICom;
using Fw.Extends;
using Fw.Serializer;
using HuoYunBLL;
using HuoYunWeb.App_Code;
using HuoYunWeb.App_Code.Push;
using HuoYunWeb.Models;
using ServerFw.Extends;
using WebGrease.Css.Extensions;
using System.IO;
using HuoYunWeb;
using System.Text;
using System.Web;
using System.Data.SqlClient;


namespace HuoYunWeb.Controllers
{
	//    [APIAuthorize]
	public class CustomerController : UserBaseController
	{
		/// <summary>
		/// 添加货主
		/// </summary>
		/// <param name="phoneNumber">电话</param>
		/// <param name="userName">姓名</param>
		/// <param name="userSex">性别</param>
		/// <param name="defaultAdd">若不存在，是否默认添加 1 默认添加 0 默认不添加</param>
		/// <returns></returns>
		/// 
		public ActionResult CheckCustomer(String phoneNumber, String userName, int userSex, int defaultAdd = 0, string openId = "")
		{
			lock (RegisterFlag)
			{
				string pk = new Random().Next(1000000, 9999999).ToString();
				DateTime pKeyOverDate = DateTime.Now.AddDays(1);
				int Register = 0;
				using (var a = new SYSContext())
				{
					var user = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (user != null)
					{
						if (user.Locked == 1) return (Result.GetLoginResult(false, "账号冻结", 102, null, DateTime.Now));
						user.PKey = pk;
						user.PKeyOverDate = pKeyOverDate;
						if (user.CustomerInfo == null)
						{
							user.CustomerInfo = new Customer
							{
								ID = user.ID,
								UserID = user.ID,
								Valid = 1
							};
							Register = 1;
						}

						if (string.IsNullOrEmpty(user.OpenID))
						{ user.OpenID = openId; }

						user.AddCustomer();
						a.SaveChanges();
						return Result.GetSuccess(new LoninResult { PK = pk, Register = Register, UserID = user.ID.ToString(), UserState = user.UserState, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 });
					}

					if (defaultAdd == 1)
					{
						var newGuid = Guid.NewGuid();
						var entity = new User
						{
							ID = newGuid,
							Account = phoneNumber,
							PhoneNumber = phoneNumber,
							UserName = userName,
							Gender = userSex,
							PKey = pk,
							PKeyOverDate = pKeyOverDate,
							OpenID = openId,
							CustomerInfo = new Customer
							{
								ID = newGuid,
								UserID = newGuid,
								Valid = 1
							}
						};

						var numbers = a.Users.Select(z => z.UserNumber).ToList();
						if (numbers.Any())
						{
							entity.UserNumber = numbers.Max() + 1;
						}
						else
						{
							entity.UserNumber = 10000;
						}
						entity.AddCustomer();
						a.Users.Add(entity);
						a.SaveChanges();
						return Result.GetSuccess(new LoninResult { PK = pk, Register = 1, UserID = entity.ID.ToString(), UserState = entity.UserState, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 });
					}

				}
				return Result.GetResult(false, "请先添加该用户为货主，然后再添加订单", "", 201);

			}
		}
		/// <summary>
		/// 检查版本更新
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		[AllowAnonymous]
		public ActionResult CheckVersion(string phoneID, string phoneNumber)
		{
			var v = MvcApplication.CustomerVersion;
			var p = MvcApplication.CustomerUpdatePath;
			var u = MvcApplication.CustomerUpdateLog;
			return Result.GetSuccess(new
			{
				versionCode = v,
				downloadUrl = p,
				updateLog = u

			});
		}

		public ActionResult ClientRequestRoute(string phoneID, string phoneNumber, string key)
		{
			var list = new List<Route>();
			try
			{
				using (var c = new SYSContext())
				{
					var result = c.Routes.Where(r => r.Start.Contains(key) || r.End.Contains(key)).ToList();
					list = result.Select(x => new Route() { Id = x.Id, Start = x.Start, End = x.End }).ToList();
				}
			}
			catch
			{

			}
			return Result.GetSuccess(list);
		}
        public ActionResult RequestRoute3(string q)
        {
            var list = new List<Route>();
            JsonResult jsonResult = null;
            try
            {
                using (var c = new SYSContext())
                {
                    list = c.Routes.Where(r => r.Start.Contains(q) || r.End.Contains(q)).ToList();

                    if (list.Any())
                    {
                        jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                    }
                    else
                    {
                        var address = new Route { Id = 0, Start = "未找到地址", End = "未找到地址" };
                        list.Add(address);
                        jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                    }
                }
            }
            catch
            {
                var address = new Route { Id = 0, Start = "未找到地址", End = "未找到地址" };
                list.Add(address);
                jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            }

            return jsonResult;
        }
		/// <summary>
		/// 发送 API 请求并返回方案信息。
		/// </summary>
		/// <returns></returns>
		public ActionResult RequestRoute2(string q)
		{
			var list = new List<Route>();
			JsonResult jsonResult = null;
			try
			{
				using (var c = new SYSContext())
				{
					var qr = (from r in c.Routes where r.Start.Contains(q) select r.Start).Distinct();
					var tlist = qr.ToList();
					foreach (var str in tlist)
					{
						list.Add(new Route() { Start = str });
					}

					if (list.Any())
					{
						jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
					}
					else
					{
						var address = new Route { Id = 0, Start = "未找到地址", End = "未找到地址" };
						list.Add(address);
						jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
					}
				}
			}
			catch
			{
				var address = new Route { Id = 0, Start = "未找到地址", End = "未找到地址" };
				list.Add(address);
				jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

			}

			return jsonResult;
		}

		/// <summary>
		/// 获取地址
		/// </summary>
		/// <param name="q"></param>
		/// <param name="limit"></param>
		/// <param name="timestamp"></param>
		/// <param name="isStart"></param>
		/// <returns></returns>
		public ActionResult RequestRoute(string key)
		{
			var list = new List<Route>();
			JsonResult jsonResult = null;
			try
			{
				using (var c = new SYSContext())
				{
					var result = c.Routes.Where(r => r.Start.Contains(key) || r.End.Contains(key)).ToList();
					list = result.Select(x => new Route() { Id = x.Id, Start = x.Start, End = x.End }).ToList();


					if (list.Any())
					{
						jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
					}
					else
					{
						var address = new Route { Id = 0, Start = "抱歉，未找到您要查询的地址", End = "" };
						list.Add(address);
						jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
					}
				}
			}
			catch
			{
				var address = new Route { Id = 0, Start = "抱歉，未找到您要查询的地址", End = "" };
				list.Add(address);
				jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

			}

			return jsonResult;
		}
		/// <summary>
		/// 发送 API 请求并返回方案信息。
		/// </summary>
		/// <returns></returns>
		public ActionResult RequestBaiduApi(string q, int limit, long timestamp)
		{
			string apiUrl = "http://api.map.baidu.com/place/v2/suggestion";
			string apiKey = MvcApplication.BaiduServerApi; //
			string output = "json";
			IDictionary<string, string> param = new Dictionary<string, string>();
			param.Add("ak", apiKey);
			param.Add("output", output);
			param.Add("region", MvcApplication.CurrentCity);
			param.Add("query", q.Trim());
			string result = string.Empty;
			JsonResult jsonResult = null;
			try
			{
				//以 Get 形式请求 Api 地址
				result = HuoYunWeb.BaiDuMapAPI.DoGet(apiUrl, param);
				result.Replace("\n", "");
				BaiduData baiDuData = JsonHelper.JsonDeserialize<BaiduData>(result);
				if (baiDuData != null && baiDuData.status == 0 && baiDuData.result != null && baiDuData.result.Count > 0)
				{
					jsonResult = new JsonResult { Data = baiDuData.result, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
				}
				else
				{
					BaiDuAddress address = new BaiDuAddress { city = "", name = "抱歉，未找到您要查询的地址", district = "" };
					List<BaiDuAddress> list = new List<BaiDuAddress>();
					list.Add(address);
					jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
				}
			}
			catch (Exception)
			{
				BaiDuAddress address = new BaiDuAddress { city = "", name = "抱歉，未找到您要查询的地址", district = "" };
				List<BaiDuAddress> list = new List<BaiDuAddress>();
				list.Add(address);
				jsonResult = new JsonResult { Data = list, ContentEncoding = Encoding.UTF8, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
			}

			return jsonResult;
		}
		/// <summary>
		/// 返回订单的执行流
		/// 订单操作
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public ActionResult GetOrderFlow(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			using (var a = new SYSContext())
			{
				var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
				//未找到货主或订单
				if (order == null) return (Result.GetError("未找到订单", 201));
				if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
				var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (custom == null) return (Result.GetError("未找到人员", 201));

				//货主和订单对不上
				if (order.GuestUserID != custom.ID) return (Result.GetError("无法操作该订单", 201));
				var driverRelOrders = order.DriverRelOrders.ToList();
				//var rel = driverRelOrders.FirstOrDefault(z => z.EnsureState == 1);
				var relIDs = driverRelOrders.Select(z => z.ID).ToList();
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				var reldetails = a.DriverRelOrderDetails.Where(z => relIDs.Contains(z.DriverRelOrderID)).OrderBy(z => z.AtThatTime).ToList();
				reldetails.ForEach(z => z.DriverRelOrder = null);
				order.DriverRelOrders = null;
				return Result.GetSuccess(new
				{
					Order = order,
					Detail = reldetails,
				});
			}
		}
		/// <summary>
		/// 获取指定订单中,所有接收的车主列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="line"></param>
		/// <returns>bool</returns>
		public ActionResult GetAcceptDriverListByOrderID(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			using (var a = new SYSContext())
			{
				var list = from driverRelOrder in a.DriverRelOrders
						   where driverRelOrder.OrderState == 1
						   select driverRelOrder.Driver;
				a.SaveChanges();
				List<Driver> drivers = list.ToList();
				return Result.GetResult(true, null, drivers.ToJson());
			}
		}

		/// <summary>
		/// 加价
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult AddPrice(string phoneID, string phoneNumber, string orderNumber, double? price)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			Para.Required("price", price);
			using (var a = new SYSContext())
			{
				if (!price.HasValue || price <= 0) return (Result.GetError("加价的金额不正确", 201));
				var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (user == null) return (Result.GetError("未找到人员", 201));
				var order = a.Orders.Include(z => z.DriverRelOrders).FirstOrDefault(z => z.OrderNumber == orderNumber);
				if (order == null) return (Result.GetError("未找到订单", 201));
				if (order.GuestUserID != user.ID) return (Result.GetError("不能操作别人的订单", 201));
				if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
				var rel = order.DriverRelOrders.FirstOrDefault(z => z.EnsureState == 1);
				if (order.OrderState >= 2) return (Result.GetError("该订单状态无法加价", 201));
				order.Price += price.Value;

				//当货主加价时,系统会把车主状态为 不接受 ,接受后取消的 订单状态更新为初始状态 0 
				var ds = order.DriverRelOrders.Where(z =>
					z.OrderState == -2 ||
					z.OrderState == -4).ToList();
				ds.ForEach(z =>
				{
					z.OrderState = 0;
					z.AddDetail(0, "", 3, user.PhoneNumber, user.Gender);
				});
				//通知 之前的所有车主,订单加价
				PushDriverHelper.SendAddPrice(order.DriverRelOrders.Select(w => w.DriverID).ToList(), order.OrderNumber, price.Value, user);

				var queryable = a.Drivers.Where(w => w.User.ID != user.ID && w.User.Locked != 1 && w.RecivedPush == 1);
				if (MyConfigManage.Current.ValidDriverEnabled == 0)
				{
					queryable = queryable.Where(z => z.Valid == 1);
				}
				var driver = queryable.Include(z => z.DriverLines).ToList();
				//                var driver = a.Drivers.Where(w => w.Valid == 1 && w.RecivedPush == 1).ToList();
				//获取最近距离的30车主
				var list = new BaiDuMapAPI().GetRangeResult50KM(driver, order.StartPointX, order.StartPointY, order.EndPointX, order.EndPointY, MvcApplication.Range);
				//                order.DriverRelOrders = new Collection<DriverRelOrder>();
				var newDriver = new List<Guid>();//新添加的车主ID列表

				//添加新找到的车主列表
				list.ForEach(w =>
				{
					if (order.DriverRelOrders.All(z => z.DriverID != w.Driver.ID))
					{
						newDriver.Add(w.Driver.ID);
						order.DriverRelOrders.Add(new DriverRelOrder
						{
							ID = Guid.NewGuid(),
							Driver = w.Driver,
							DriverID = w.Driver.ID,
							OrderID = order.ID,
							Order = order,
						});
					}
				});
				a.SaveChanges();
				PushDriverHelper.SendNewOrder(newDriver, order.OrderNumber, user);
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				return Result.GetSuccess(order);
			}
		}

		class comp : IEqualityComparer<DriverRelOrder>
		{
			public bool Equals(DriverRelOrder x, DriverRelOrder y)
			{
				return x.DriverID == y.DriverID;
			}

			public int GetHashCode(DriverRelOrder obj)
			{
				return obj.DriverID.GetHashCode();
			}
		}
		/// <summary>
		/// 服务器端添加订单
		/// </summary>
		/// <param name="phoneNumber"></param>
		/// <param name="startAddress"></param>
		/// <param name="startCity"></param>
		/// <param name="endAddress"></param>
		/// <param name="endCity"></param>
		/// <param name="goodsName"></param>
		/// <param name="sendDate"></param>
		/// <param name="goodsPrice"></param>
		/// <param name="isMianyi"></param>
		/// <param name="goodsDriverId"></param>
		/// <param name="goodsMemo"></param>
		/// <returns></returns>
		public ActionResult ServerAddOrderOld(string phoneNumber, string startAddress, string startCity, string endAddress, string endCity, string goodsName, string sendDate, int isMianyi, int defaultComplete, string goodsDriverId, string goodsMemo, double goodsPrice = 0)
		{
			var order1 = new Order();
			using (var a = new SYSContext())
			{

				order1.ID = Guid.NewGuid();
				var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (user == null) return (Result.GetError("未找到人员", 201));
				if (user.Locked == 1) return (Result.GetError("账号冻结", 201));
				if (user.CustomerInfo == null) return (Result.GetError("未开通货主功能.", 201));
				order1.GuestUserID = user.ID;
				order1.StartName = HttpUtility.UrlDecode(startAddress);
				order1.StartCityName = HttpUtility.UrlDecode(startCity);
				order1.EndName = HttpUtility.UrlDecode(endAddress);
				order1.EndCityName = HttpUtility.UrlDecode(endCity);
				order1.IsEnabled = 1;
				order1.Lading = goodsName;
				order1.OrderSendtime = Convert.ToDateTime(sendDate);
				order1.Price = goodsPrice;
				order1.IsMianYi = isMianyi;
				order1.OrderRemark = goodsMemo;
				order1.PublishTime = DateTime.Now;
				var random = new Random();
				order1.PKey = random.Next(100000, 999999).ToString();
				order1.OrderNumber = DateTime.Now.ToString("yyyyMMddHHmmss") + random.Next(1000, 9999);
				order1.OverTime = DateTime.Now.AddDays(MvcApplication.OverTime);
				if ((order1.EndPointX == 0 || order1.EndPointY == 0) && !string.IsNullOrEmpty(order1.EndName))
				{
					double x, y;
					BaiDuAPI.GetLatLngByName(order1.EndName, out x, out y, order1.EndCityName);
					order1.EndPointX = x;
					order1.EndPointY = y;
				}
				if ((order1.StartPointX == 0 || order1.StartPointY == 0) && !string.IsNullOrEmpty(order1.StartName))
				{
					double x, y;
					BaiDuAPI.GetLatLngByName(order1.StartName, out x, out y, order1.StartCityName);
					order1.StartPointX = x;
					order1.StartPointY = y;
				}
				order1.GuestUserPhoneNumber = user.PhoneNumber;
				string userImagePath = "";
				if (!string.IsNullOrEmpty(order1.GoodImage))
				{
					ImageManage.SaveUserImage(order1.ID, order1.GoodImage, 0);
					userImagePath = Path.Combine(ImageManage.GetUserImageUrl(order1.ID), ImageManage.GetUserImageName(0));
				}
				order1.GoodImage = "";
				order1.OrderImagePath = userImagePath;
				//未选择车主
				if (string.IsNullOrWhiteSpace(goodsDriverId))
				{
					a.Orders.Add(order1);
					//找出数据库中所有的车主
					var queryable = a.Drivers.Where(w => w.User.ID != user.ID && w.User.Locked != 1 && w.RecivedPush == 1);
					if (MyConfigManage.Current.ValidDriverEnabled == 0)
					{
						queryable = queryable.Where(z => z.Valid == 1);
					}
					var driver = queryable.ToList();
					//获取最近距离的30车主
					var list = new BaiDuMapAPI().GetRangeResult50KM(driver, order1.StartPointX, order1.StartPointY, order1.EndPointX,
						order1.EndPointY, MvcApplication.Range);
					order1.DriverRelOrders = new Collection<DriverRelOrder>();
					list.ForEach(w =>
					{
						order1.DriverRelOrders.Add(new DriverRelOrder
						{
							ID = Guid.NewGuid(),
							Driver = w.Driver,
							DriverID = w.Driver.ID,
							OrderID = order1.ID,
							Order = order1,
						});
					});
					a.SaveChanges();
					PushDriverHelper.SendNewOrder(list.Select(w => w.Driver.ID).ToList(), order1.OrderNumber, user);
					a.Configuration.LazyLoadingEnabled = false;
					a.Configuration.ProxyCreationEnabled = false;
				}
				//已经选择了车主
				else
				{
					if (defaultComplete == 1) order1.OrderState = 5;
					else order1.OrderState = 2;
					a.Orders.Add(order1);
					Guid selectDirverID = new Guid(goodsDriverId);
					var driver = a.Drivers.FirstOrDefault(w => w.ID == selectDirverID);
					order1.DriverRelOrders = new Collection<DriverRelOrder>();
					order1.DriverRelOrders.Add(new DriverRelOrder
					{
						ID = Guid.NewGuid(),
						Driver = driver,
						DriverID = driver.ID,
						OrderID = order1.ID,
						Order = order1,
						OrderState = order1.OrderState,
						EnsureState = 1
					});

					a.SaveChanges();
					if (order1.OrderState == 2) PushDriverHelper.EnsureOrderDriver(driver.ID, order1.OrderNumber, user);
					a.Configuration.LazyLoadingEnabled = false;
					a.Configuration.ProxyCreationEnabled = false;
				}
				return Result.GetSuccess(new { OrderId = order1.OrderNumber });
			}
		}

		/// <summary>
		/// 服务器端添加订单
		/// </summary>
		/// <param name="phoneNumber"></param>
		/// <param name="startAddress"></param>
		/// <param name="startCity"></param>
		/// <param name="endAddress"></param>
		/// <param name="endCity"></param>
		/// <param name="goodsName"></param>
		/// <param name="sendDate"></param>
		/// <param name="goodsPrice"></param>
		/// <param name="isMianyi"></param>
		/// <param name="goodsDriverId"></param>
		/// <param name="goodsMemo"></param>
		/// <returns></returns>
		public ActionResult ServerAddOrder(string phoneNumber, string goodsName, string sendDate, int isMianyi, int defaultComplete, string goodsDriverId, string goodsMemo, long routeId, double goodsPrice = 0,int orderFrom=0)
		{
			var order1 = new Order();
			using (var a = new SYSContext())
			{

				order1.ID = Guid.NewGuid();
				var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (user == null) return (Result.GetError("未找到人员", 201));
				if (user.Locked == 1) return (Result.GetError("账号冻结", 201));
				if (user.CustomerInfo == null) return (Result.GetError("未开通货主功能.", 201));
				var route = a.Routes.FirstOrDefault(x => x.Id == routeId);
				if (route == null)
				{
					return (Result.GetError("未找到线路.", 201));
				}
				order1.GuestUserID = user.ID;
				order1.StartName = route.Start;
				order1.EndName = route.End;
				order1.RouteId = routeId;
			    order1.AreaCode = route.AreaCode;
			    order1.StartCityName = route.City;
			    order1.EndCityName = route.City;
				order1.IsEnabled = 1;
				order1.Lading = goodsName;
				order1.OrderSendtime = Convert.ToDateTime(sendDate);
				order1.Price = goodsPrice;
				order1.IsMianYi = isMianyi;
				order1.OrderRemark = goodsMemo;
				order1.PublishTime = DateTime.Now;
                order1.From = orderFrom;
				var random = new Random();
				order1.PKey = random.Next(100000, 999999).ToString();
				order1.OrderNumber = DateTime.Now.ToString("yyyyMMddHHmmss") + random.Next(1000, 9999);
				order1.OverTime = DateTime.Now.AddDays(MvcApplication.OverTime);
				order1.GuestUserPhoneNumber = user.PhoneNumber;
				string userImagePath = "";
				if (!string.IsNullOrEmpty(order1.GoodImage))
				{
					ImageManage.SaveUserImage(order1.ID, order1.GoodImage, 0);
					userImagePath = Path.Combine(ImageManage.GetUserImageUrl(order1.ID), ImageManage.GetUserImageName(0));
				}
				order1.GoodImage = "";
				order1.OrderImagePath = userImagePath;
				//未选择车主
				if (string.IsNullOrWhiteSpace(goodsDriverId))
				{
					a.Orders.Add(order1);
					//找出数据库中所有的车主
					var driverIds = a.DriverRoutes.Where(d => d.RouteId == routeId).Select(x => x.DriverId).ToList();
					var alldrivers = a.Drivers.Where(x => driverIds.Contains(x.ID));
					order1.DriverRelOrders = new Collection<DriverRelOrder>();
					alldrivers.ForEach(w =>
					{
						order1.DriverRelOrders.Add(new DriverRelOrder
						{
							ID = Guid.NewGuid(),
							Driver = w,
							DriverID = w.ID,
							OrderID = order1.ID,
							Order = order1,
						});
					});
					a.SaveChanges();
					PushDriverHelper.SendNewOrder(driverIds, order1.OrderNumber, user);
					a.Configuration.LazyLoadingEnabled = false;
					a.Configuration.ProxyCreationEnabled = false;
				}
				//已经选择了车主
				else
				{
					if (defaultComplete == 1) order1.OrderState = 5;
					else order1.OrderState = 2;
					a.Orders.Add(order1);
					Guid selectDirverID = new Guid(goodsDriverId);
					var driver = a.Drivers.FirstOrDefault(w => w.ID == selectDirverID);
					order1.DriverRelOrders = new Collection<DriverRelOrder>();
					order1.DriverRelOrders.Add(new DriverRelOrder
					{
						ID = Guid.NewGuid(),
						Driver = driver,
						DriverID = driver.ID,
						OrderID = order1.ID,
						Order = order1,
						OrderState = order1.OrderState,
						EnsureState = 1
					});

					a.SaveChanges();
					if (order1.OrderState == 2) PushDriverHelper.EnsureOrderDriver(driver.ID, order1.OrderNumber, user);
					a.Configuration.LazyLoadingEnabled = false;
					a.Configuration.ProxyCreationEnabled = false;
				}
				// 发短信
				//var message = string.Format(MvcApplication.M_PublishTemplate, user.UserName, user.Gender.GetGenderStr(), DateTime.Now.ToString("yyyy-MM-dd"), route.Start, route.End);
				//ValidCodeAPI.SendMessage(user.PhoneNumber, message);

				return Result.GetSuccess(new { OrderId = order1.OrderNumber });
			}
		}

		/// <summary>
		/// 启动一个订单
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult AddNewOrder(string phoneID, string phoneNumber, string order)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);

			var order1 = JsonHelper.FastJsonDeserialize<Order>(order);
			Para.Required("order", order1);
			using (var a = new SYSContext())
			{

				order1.ID = Guid.NewGuid();
				var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (user == null) return (Result.GetError("未找到人员", 201));
				if (user.Locked == 1) return (Result.GetError("账号冻结", 201));
				if (user.CustomerInfo == null) return (Result.GetError("未开通货主功能.", 201));
				var route = a.Routes.FirstOrDefault(x => x.Id == order1.RouteId);
				if (route == null)
				{
					return (Result.GetError("未找到线路.", 201));
				}
				order1.GuestUserID = user.ID;
				var random = new Random();
				order1.PKey = random.Next(100000, 999999).ToString();
				order1.OrderNumber = DateTime.Now.ToString("yyyyMMddHHmmss") + random.Next(1000, 9999);
				order1.OverTime = DateTime.Now.AddDays(MvcApplication.OverTime);
				order1.GuestUserPhoneNumber = user.PhoneNumber;
				string userImagePath = "";
				if (!string.IsNullOrEmpty(order1.GoodImage))
				{
					ImageManage.SaveUserImage(order1.ID, order1.GoodImage, 0);
					userImagePath = Path.Combine(ImageManage.GetUserImageUrl(order1.ID), ImageManage.GetUserImageName(0));
				}
				order1.GoodImage = "";
				order1.OrderImagePath = userImagePath;
                //order1.StartName = route.Start;
                //order1.EndName = route.End;

				a.Orders.Add(order1);

				//找出数据库中所有的车主
				var driverIds = a.DriverRoutes.Where(d => d.RouteId == order1.RouteId).Select(x => x.DriverId).ToList();
				var alldrivers = a.Drivers.Where(x => driverIds.Contains(x.ID));
				order1.DriverRelOrders = new Collection<DriverRelOrder>();
				alldrivers.ForEach(w =>
				{
					order1.DriverRelOrders.Add(new DriverRelOrder
					{
						ID = Guid.NewGuid(),
						Driver = w,
						DriverID = w.ID,
						OrderID = order1.ID,
						Order = order1,
					});
				});
				a.SaveChanges();
				PushDriverHelper.SendNewOrder(driverIds, order1.OrderNumber, user);
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;

				// 发短信
				//var message = string.Format(MvcApplication.M_PublishTemplate, user.UserName, user.Gender.GetGenderStr(), DateTime.Now.ToString("yyyy-MM-dd"), route.Start, route.End);
				//ValidCodeAPI.SendMessage(user.PhoneNumber, message);

				return Result.GetSuccess(order1);
			}
		}
		/// <summary>
		/// 启动一个订单
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult AddNewOrderOld(string phoneID, string phoneNumber, string order)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);

			var order1 = JsonHelper.FastJsonDeserialize<Order>(order);
			Para.Required("order", order1);
			using (var a = new SYSContext())
			{

				order1.ID = Guid.NewGuid();
				var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (user == null) return (Result.GetError("未找到人员", 201));
				if (user.Locked == 1) return (Result.GetError("账号冻结", 201));
				if (user.CustomerInfo == null) return (Result.GetError("未开通货主功能.", 201));
				order1.GuestUserID = user.ID;
				var random = new Random();
				order1.PKey = random.Next(100000, 999999).ToString();
				order1.OrderNumber = DateTime.Now.ToString("yyyyMMddHHmmss") + random.Next(1000, 9999);
				order1.OverTime = DateTime.Now.AddDays(MvcApplication.OverTime);
				if ((order1.EndPointX == 0 || order1.EndPointY == 0) && !string.IsNullOrEmpty(order1.EndName))
				{
					double x, y;
					BaiDuAPI.GetLatLngByName(order1.EndName, out x, out y, order1.EndCityName);
					order1.EndPointX = x;
					order1.EndPointY = y;
				}
				if ((order1.StartPointX == 0 || order1.StartPointY == 0) && !string.IsNullOrEmpty(order1.StartName))
				{
					double x, y;
					BaiDuAPI.GetLatLngByName(order1.StartName, out x, out y, order1.StartCityName);
					order1.StartPointX = x;
					order1.StartPointY = y;
				}
				order1.GuestUserPhoneNumber = user.PhoneNumber;
				string userImagePath = "";
				if (!string.IsNullOrEmpty(order1.GoodImage))
				{
					ImageManage.SaveUserImage(order1.ID, order1.GoodImage, 0);
					userImagePath = Path.Combine(ImageManage.GetUserImageUrl(order1.ID), ImageManage.GetUserImageName(0));
				}
				order1.GoodImage = "";
				order1.OrderImagePath = userImagePath;
				a.Orders.Add(order1);

				//找出数据库中所有的车主
				//                var driver = a.Drivers.Where(z=>z.User.Locked != 1).ToList();//&& w.Valid == 1 
				var queryable = a.Drivers.Where(w => w.User.ID != user.ID && w.User.Locked != 1 && w.RecivedPush == 1);
				if (MyConfigManage.Current.ValidDriverEnabled == 0)
				{
					queryable = queryable.Where(z => z.Valid == 1);
				}
				var driver = queryable.ToList();
				//获取最近距离的30车主
				var list = new BaiDuMapAPI().GetRangeResult50KM(driver, order1.StartPointX, order1.StartPointY, order1.EndPointX,
					order1.EndPointY, MvcApplication.Range);
				order1.DriverRelOrders = new Collection<DriverRelOrder>();
				list.ForEach(w =>
				{
					order1.DriverRelOrders.Add(new DriverRelOrder
					{
						ID = Guid.NewGuid(),
						Driver = w.Driver,
						DriverID = w.Driver.ID,
						OrderID = order1.ID,
						Order = order1,
					});
				});
				a.SaveChanges();
				PushDriverHelper.SendNewOrder(list.Select(w => w.Driver.ID).ToList(), order1.OrderNumber, user);
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				return Result.GetSuccess(order1);
			}
		}
		static object EnsureOrderDriverFlag = new object();

		/// <summary>
		/// 根据订单号,返回车主的User信息列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public ActionResult GetDriverUserListByOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			using (var a = new SYSContext())
			{
				var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
				//未找到货主或订单
				if (order == null) return (Result.GetError("未找到订单", 201));

				var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (user == null) return (Result.GetError("未找到人员", 201));
				//货主和订单对不上
				if (order.GuestUserID != user.ID) return (Result.GetError("无法操作该订单", 201));
				var driverRelOrders = order.DriverRelOrders.ToList();
				var ids = driverRelOrders.Select(w => w.DriverID).ToList();
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				var users = a.Users.Where(z => ids.Contains(z.ID)).ToList();
				users.ForEach(w => w.FilterInfo());
				return Result.GetSuccess(users);
			}
		}
        /// <summary>
        /// 获取所有的区域
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
       [AllowAnonymous]
	    public ActionResult GetAllArea()
	    {
	        using (var a = new SYSContext())
	        {
	            var areas = a.Areas;
	            return Result.GetSuccess(areas);
	        }
	    }
		/// <summary>
		/// 货主确定一个订单的司机,
		/// 订单操作
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public ActionResult EnsureOrderDriver(string phoneID, string phoneNumber, string orderNumber, Guid driverID)
		{
			lock (EnsureOrderDriverFlag)
			{
				Para.Required("phoneID", phoneID);
				Para.Required("phoneNumber", phoneNumber);
				Para.Required("driverID", driverID);
				Para.Required("orderNumber", orderNumber);
				using (var a = new SYSContext())
				{
					var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);

					//未找到货主或订单
					if (order == null) return (Result.GetError("未找到订单", 201));

					var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
					if (custom == null) return (Result.GetError("未找到人员", 201));
					//货主和订单对不上
					if (order.GuestUserID != custom.ID) return (Result.GetError("无法操作该订单", 201));
					if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
					var driverRelOrders = order.DriverRelOrders.ToList();
					var rel = driverRelOrders.FirstOrDefault(z => z.DriverID == driverID);
					//关联为空,或状态不为已接受
					if (rel == null) return (Result.GetError("未找到订单关联信息", 201));
					if (rel.OrderState == 2) return (Result.GetError("您已经选定车主", 201));
					if (rel.OrderState == 5) return (Result.GetError("订单已完成", 201));
					if (rel.OrderState != 1) return (Result.GetError("订单状态错误", 201));

					rel.OrderState = 5;
					order.OrderState = 5;
					rel.EnsureState = 1;
					rel.AddDetail(5, custom.UserName, 1, custom.PhoneNumber, custom.Gender);

					PushDriverHelper.EnsureOrderDriver(rel.DriverID, order.OrderNumber, custom);
					var driverUserInfo = a.Users.FirstOrDefault(z => z.ID == rel.DriverID);
					//当前订单中,未看到信息的车主和已经确定的车主列表
					var otherRels = driverRelOrders.Where(z => z.OrderState == 1 || z.OrderState == 0).ToList();
					var userids = otherRels.Select(z => z.DriverID).ToList();
					var users = a.Users.Where(w => userids.Contains(w.ID)).ToList();
					if (driverUserInfo != null) otherRels.ForEach(z =>
					{
						if (z.OrderState == 1)
						{
							//如果已抢单
							z.AddDetail(-3, custom.UserName, 1, custom.PhoneNumber, custom.Gender);
							z.OrderState = -3;
							var firstOrDefault = users.FirstOrDefault(x => x.ID == z.DriverID);
							if (firstOrDefault != null)
							{
								PushDriverHelper.EnsureOrderDriverFail(z.DriverID, order.OrderNumber, driverUserInfo.UserName, custom);
							}
						}
						else
						{
							//如果车主没有查看,也没有确定抢单
							if (z.OrderState == 0) z.OrderState = -1;
						}


					});
					a.SaveChanges();
					return Result.GetSuccess();
				}
			}
		}

		public ActionResult EnsureProcessOrderDriver2(string phoneID, string phoneNumber, string orderNumber, Guid driverID)
		{
			lock (EnsureOrderDriverFlag)
			{
				Para.Required("phoneID", phoneID);
				Para.Required("phoneNumber", phoneNumber);
				Para.Required("driverID", driverID);
				Para.Required("orderNumber", orderNumber);
				using (var a = new SYSContext())
				{
					var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);

					//未找到货主或订单
					if (order == null) return (Result.GetError("未找到订单", 201));

					var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
					if (custom == null) return (Result.GetError("未找到人员", 201));
					//货主和订单对不上
					if (order.GuestUserID != custom.ID) return (Result.GetError("无法操作该订单", 201));
					if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
					var driverRelOrders = order.DriverRelOrders.ToList();
					var rel = order.DriverRelOrders.FirstOrDefault(z => z.DriverID == driverID);
					//关联为空,或状态不为已接受
					if (rel == null) return (Result.GetError("未找到订单关联信息", 201));
					if (rel.OrderState == 2) return (Result.GetError("您已经选择了车主", 201));
					if (rel.OrderState != 1) return (Result.GetError("订单状态错误", 201));

					SqlParameter[] sqlParams = new SqlParameter[3];
					sqlParams[0] = new SqlParameter("@orderNumber", orderNumber);
					sqlParams[1] = new SqlParameter("@deriverId", driverID);
					sqlParams[2] = new SqlParameter("@phoneNumber", phoneNumber);
					a.Database.ExecuteSqlCommand("exec ProcessOrderDriver @orderNumber,@deriverId,@phoneNumber", sqlParams);
					return Result.GetSuccess();
				}
			}
		}

		public ActionResult EnsureProcessOrderDriver(string phoneID, string phoneNumber, string orderNumber, Guid driverID)
		{
			lock (EnsureOrderDriverFlag)
			{
				Para.Required("phoneID", phoneID);
				Para.Required("phoneNumber", phoneNumber);
				Para.Required("driverID", driverID);
				Para.Required("orderNumber", orderNumber);
				using (var a = new SYSContext())
				{
					var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);

					//未找到货主或订单
					if (order == null) return (Result.GetError("未找到订单", 201));

					var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
					if (custom == null) return (Result.GetError("未找到人员", 201));
					//货主和订单对不上
					if (order.GuestUserID != custom.ID) return (Result.GetError("无法操作该订单", 201));
					if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
					var driverRelOrders = order.DriverRelOrders.ToList();
					var rel = order.DriverRelOrders.FirstOrDefault(z => z.DriverID == driverID);
					//关联为空,或状态不为已接受
					if (rel == null) return (Result.GetError("未找到订单关联信息", 201));
					if (rel.OrderState == 2) return (Result.GetError("您已经选择了车主", 201));
					if (rel.OrderState != 1) return (Result.GetError("订单状态错误", 201));

					rel.OrderState = 2;
					order.OrderState = 2;
					rel.EnsureState = 1;
					//rel.AddDetail(2, custom.UserName, 1, custom.PhoneNumber, custom.Gender);

					PushDriverHelper.EnsureOrderDriver(rel.DriverID, order.OrderNumber, custom);
					var driverUserInfo = a.Users.FirstOrDefault(z => z.ID == rel.DriverID);
					//当前订单中,未看到信息的车主和已经确定的车主列表
					var otherRels = driverRelOrders.Where(z => z.OrderState == 1 || z.OrderState == 0).ToList();
					var userids = otherRels.Select(z => z.DriverID).ToList();
					var users = a.Users.Where(w => userids.Contains(w.ID)).ToList();
					if (driverUserInfo != null) otherRels.ForEach(z =>
					{
						if (z.OrderState == 1)
						{
							//如果已抢单
							z.AddDetail(-3, custom.UserName, 1, custom.PhoneNumber, custom.Gender);
							z.OrderState = -3;
							var firstOrDefault = users.FirstOrDefault(x => x.ID == z.DriverID);
							if (firstOrDefault != null)
							{
								PushDriverHelper.EnsureOrderDriverFail(z.DriverID, order.OrderNumber, driverUserInfo.UserName, custom);
							}
						}
						else
						{
							//如果车主没有查看,也没有确定抢单
							if (z.OrderState == 0) z.OrderState = -1;
						}


					});
					a.SaveChanges();
					return Result.GetSuccess();
				}
			}
		}
		/// <summary>
		/// 货主确定将货物送给车主
		/// 订单操作
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public ActionResult EnsureSendGoods(string phoneID, string phoneNumber, string orderNumber)
		{
			lock (EnsureOrderDriverFlag)
			{
				Para.Required("phoneID", phoneID);
				Para.Required("phoneNumber", phoneNumber);
				Para.Required("orderNumber", orderNumber);
				using (var a = new SYSContext())
				{
					var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
					//未找到货主或订单
					if (order == null) return (Result.GetError("未找到订单", 201));
					if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
					var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
					if (custom == null) return (Result.GetError("未找到人员", 201));

					//货主和订单对不上
					if (order.GuestUserID != custom.ID) return (Result.GetError("无法操作该订单", 201));
					var driverRelOrders = order.DriverRelOrders.ToList();
					var rel = driverRelOrders.FirstOrDefault(z => z.EnsureState == 1);

					//关联为空,或状态不为已接受
					if (rel == null) return (Result.GetError("未找到订单关联信息", 201));
					if (rel.OrderState != 2) return (Result.GetError(string.Format("订单状态错误[{0}]", rel.OrderState), 201));

					rel.OrderState = 7;
					order.OrderState = 7;
					rel.AddDetail(7, custom.UserName, 1, custom.PhoneNumber, custom.Gender);

					PushDriverHelper.EnsureSendGoods(rel.DriverID, order.OrderNumber, custom);

					a.SaveChanges();
					return Result.GetSuccess();
				}
			}
		}
		/// <summary>
		/// 注册
		/// </summary>
		/// <param name="phoneId"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="password"></param>
		/// <param name="validCode"></param>
		/// <returns>Bool</returns>
		[AllowAnonymous]
		public ActionResult Register([Required]string phoneId,
			string phoneNumber,
			string password,
			string validCode,
			string userName,
			string registerAddress,
			int? gender = 1,
			int? deviceType = 3)
		{
			lock (RegisterFlag)
			{
				Para.Required("phoneId", phoneId);
				Para.Required("phoneNumber", phoneNumber);
				string pk = new Random().Next(1000000, 9999999).ToString();
				DateTime pKeyOverDate = DateTime.Now.AddDays(1);
				int Register = 0;
				string sys_ValidCode = ValidCodeManage.GetCode(phoneNumber);
				if (string.IsNullOrEmpty(sys_ValidCode) ||
					string.IsNullOrEmpty(validCode) ||
					(!string.Equals(validCode, MvcApplication.PublicValidCode) && !string.Equals(sys_ValidCode, validCode, StringComparison.OrdinalIgnoreCase)))
				{
					return (Result.GetLoginResult(false, "验证码错误", 103, null, DateTime.Now));
				}
				var device = 3;
				if (deviceType != null)
				{
					device = deviceType.Value;
				}
				using (var a = new SYSContext())
				{
					var realAddress = string.Empty;
					var user = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (user != null)
					{
						if (user.Locked == 1) return (Result.GetLoginResult(false, "账号冻结", 102, null, DateTime.Now));
						user.PKey = pk;
						user.PKeyOverDate = pKeyOverDate;
						user.DeviceType = device;
						realAddress = user.RealAddress;
						if (user.CustomerInfo == null)
						{
							user.CustomerInfo = new Customer
							{
								ID = user.ID,
								UserID = user.ID,
							};
							Register = 1;
						}
						user.AddCustomer();
						a.SaveChanges();
						return Result.GetSuccess(new LoninResult { PK = pk, Register = Register, UserID = user.ID.ToString(), UserState = user.UserState, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 });
					}

					var newGuid = Guid.NewGuid();
					var entity = new User
					{
						ID = newGuid,
						Account = phoneNumber,
						PhoneNumber = phoneNumber,
						Password = password,
						PhoneID = phoneId,
						UserName = userName,
						RegisterAddress = registerAddress,
						RealAddress = realAddress,
						Gender = gender.Value,
						PKey = pk,
						PKeyOverDate = pKeyOverDate,
						DeviceType = device,
						CustomerInfo = new Customer
						{
							ID = newGuid,
							UserID = newGuid,
						}
					};

					var numbers = a.Users.Select(z => z.UserNumber).ToList();
					if (numbers.Any())
					{
						entity.UserNumber = numbers.Max() + 1;
					}
					else
					{
						entity.UserNumber = 10000;
					}
					entity.AddCustomer();
					a.Users.Add(entity);
					a.SaveChanges();
					return Result.GetSuccess(new LoninResult { PK = pk, Register = 1, UserID = entity.ID.ToString(), UserState = entity.UserState, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 });
				}

			}
		}
		/// <summary>
		/// 开通货主
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public ActionResult KaiTong(string phoneID, string phoneNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));
				var di = currentUser.CustomerInfo;
				if (di == null)
				{
					currentUser.CustomerInfo = new Customer
					{
						ID = currentUser.ID,
						UserID = currentUser.ID,
					};
					currentUser.AddCustomer();
					a.SaveChanges();
				}
			}
			return Result.GetSuccess();
		}
		/// <summary>
		/// 更新个人基本信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="image"></param>
		/// <param name="imageType">0:头像,1:身份证,2驾驶证,3行驶证,4车辆正面图,5车辆45度图</param>
		/// <returns>bool</returns>
		public ActionResult UploadImage(string phoneID, string phoneNumber, string image, int? imageType)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("image", image);
			Para.Required("imageType", imageType);
			using (var a = new SYSContext())
			{
				var imageType1 = imageType ?? 0;
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));
				ImageManage.SaveUserImage(currentUser.ID, image, imageType1);
				string userImagePath = ImageManage.GetUserImageName(imageType1);
				if (imageType == 0)
				{
					currentUser.ImagePath = userImagePath;
				}
				else
				{
					var di = currentUser.CustomerInfo;
					if (di == null)
					{
						currentUser.CustomerInfo = new Customer
						{
							ID = currentUser.ID,
							UserID = currentUser.ID,
						};
						di = currentUser.CustomerInfo;
					}
					if (imageType1 == 1)
					{
						currentUser.ShenFenZhengUpload = 1;
						di.ShenFenZhengUpload = 1;
						di.ShenFenZheng = userImagePath;
					}
				}
				currentUser.AddCustomer();
				a.SaveChanges();

			}
			return Result.GetSuccess();
		}
		/// <summary>
		/// 上传货物照片
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="orderNumber"></param>
		/// <param name="image"></param>
		/// <param name="imageType"></param>
		/// <returns></returns>
		public ActionResult UpLoadGoodImage(string phoneID, string phoneNumber, string orderNumber, string image, int imageType = 0)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			using (var a = new SYSContext())
			{
				var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
				//未找到货主或订单
				if (order == null) return (Result.GetError("未找到订单", 201));

				var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (user == null) return (Result.GetError("未找到人员", 201));
				//货主和订单对不上
				if (order.GuestUserID != user.ID) return (Result.GetError("无法操作该订单", 201));

				a.SaveChanges();
			}
			return Result.GetSuccess();
		}
		//        /// <summary>
		//        /// 获取个人基本信息
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult GetUser(string phoneID, string phoneNumber)
		//        {
		//            using (var a = new SYSContext())
		//            {
		//                var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		//                if (currentUser == null) return (Result.GetError("非法操作", 201));
		//                return Result.GetResult(true,null,currentUser.ToJson());
		//            }
		//        } 
		//        /// 获取个人string GetImage(string phoneID,string phoneNumber,int imageType)基本信息
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult GetImage(string phoneID, string phoneNumber,int imageType)
		//        {
		//            using (var a = new SYSContext())
		//            {
		//                var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		//                if (currentUser == null) return (Result.GetError("未找到人员", 201));
		//                return Result.GetResult(true, null, ImageManage.GetUserImage(currentUser.ID, imageType));
		//            }
		//        } 
		//        ///<summary>
		//        /// 获取司机需要的推送信息
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult GetNewOrders(string phoneID, string phoneNumber)
		//        {
		//            using (var a = new SYSContext())
		//            {
		//                var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		//                if (currentUser == null) return (Result.GetError("未找到人员", 201));
		//                var list = currentUser.DriverInfo.Orders.Where(w => w.OrderState == 0).Take(10).Select(w=>w.Order).ToList();
		//                return Result.GetResult(true, null, list.ToJson());
		//            }
		//        } 
		static object orderObject = new object();
		//        ///<summary>
		//        /// 抢单
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult EnsureOrder(string phoneID, string phoneNumber,string orderNumber)
		//        {
		//            lock (orderObject)
		//            {
		//                
		//                using (var a = new SYSContext())
		//                {
		//                    var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		//                    if (currentUser == null) return (Result.GetError("非法操作", 201));
		//
		//
		//                    var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
		//                    if (order == null) return (Result.GetError("非法操作", 201));
		//                    if (order.OrderState != 0) return (Result.GetError("非法操作", 201));
		//
		//                    var driverRelOrders = order.DriverRelOrders.ToList();
		//                    DriverRelOrder relOrder = driverRelOrders.FirstOrDefault(w=>w.DriverID == currentUser.ID);
		//                    if (relOrder == null) return (Result.GetError("非法操作", 201));
		//                    relOrder.OrderState = 1;
		//                    a.DriverRelOrders.AddOrUpdate(relOrder);
		//                    order.OrderState = 1;
		//                    //一个人接受后,把其他人的状态改为-1
		//                    driverRelOrders.Where(w=>w.ID != relOrder.ID).ForEach(
		//                        w =>
		//                        {
		//                            w.OrderState = -1;
		//                            a.DriverRelOrders.AddOrUpdate(w);
		//                        });
		//                    a.Orders.AddOrUpdate(order);
		//                    a.SaveChanges();
		//                    return Result.GetSuccess();
		//                }
		//            }
		//        } 
		//        ///<summary>
		//        /// 不接订单
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult DiscardOrder(string phoneID, string phoneNumber, string orderNumber)
		//        {
		//            lock (orderObject)
		//            {
		//                
		//                using (var a = new SYSContext())
		//                {
		//                    var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		//                    if (currentUser == null) return (Result.GetError("非法操作", 201));
		//
		//
		//                    var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
		//                    if (order == null) return (Result.GetError("非法操作", 201));
		//                    if (order.OrderState != 0) return (Result.GetError("非法操作", 201));
		//
		//                    DriverRelOrder relOrder = order.DriverRelOrders.FirstOrDefault(w=>w.DriverID == currentUser.ID);
		//                    if (relOrder == null) return (Result.GetError("非法操作", 201));
		//                    relOrder.OrderState = -2;
		//                    a.DriverRelOrders.AddOrUpdate(relOrder);
		//                    a.SaveChanges();
		//                    return Result.GetSuccess();
		//                }
		//            }
		//        } 
		//        ///<summary>
		//        /// 获取
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult GetOrder(string phoneID, string phoneNumber, string orderNumber)
		//        {
		//            lock (orderObject)
		//            {
		//                
		//                using (var a = new SYSContext())
		//                {
		//                    var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		//                    if (currentUser == null) return (Result.GetError("非法操作", 201));
		//
		//                    var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
		//                    if (order == null) return (Result.GetError("非法操作", 201));
		//                    if (order.OrderState != 0) return (Result.GetError("非法操作", 201));
		//
		//                    DriverRelOrder relOrder = order.DriverRelOrders.FirstOrDefault(w=>w.DriverID == currentUser.ID);
		//                    if (relOrder == null) return (Result.GetError("非法操作", 201));
		//                    return Result.GetSuccess(order);
		//                }
		//            }
		//        } 
		//        ///<summary>
		//        /// 取消
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult CancelOrder(string phoneID, string phoneNumber, string orderNumber)
		//        {
		//            lock (orderObject)
		//            {
		//                
		//                using (var a = new SYSContext())
		//                {
		//                    var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		//                    if (currentUser == null) return (Result.GetError("非法操作", 201));
		//
		//                    var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
		//                    if (order == null) return (Result.GetError("非法操作", 201));
		//                    if (order.DriverRelOrders != 0) return (Result.GetError("非法操作", 201));
		//
		//                    DriverRelOrder relOrder = order.DriverRelOrders.FirstOrDefault(w=>w.DriverID == currentUser.ID);
		//                    if (relOrder == null) return (Result.GetError("非法操作", 201));
		//                    return Result.GetSuccess();
		//                }
		//            }
		//        }  
		///<summary>
		/// 获取错过的订单
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetLostOrders(string phoneID, string phoneNumber, int skip = 0, int length = 10)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext(true, false))
				{
					IQueryable<Order> queryable = a.DriverRelOrders.Where(w => w.OrderState == -1).OrderByDescending(w => w.Order.PublishTime).Select(w => w.Order).Skip(skip).Take(length);
					//                    var werw = queryable.ToString();
					var orders = queryable.ToList();

					return Result.GetSuccess(orders);
				}
			}
		}

		/// <summary>
		/// 获取司机信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetDriver(string phoneID, string phoneNumber)
		{
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));
				return Result.GetResult(true, null, currentUser.DriverInfo.ToJson());
			}
		}
		/// <summary>
		/// 评价车主
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult AppraiseOrder(string phoneID, string phoneNumber, double? score, string orderNumber, int? address, int? complete, int? time, string content)
		{
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));

				var order = a.Orders.Include(z => z.DriverRelOrders).FirstOrDefault(w => w.OrderNumber == orderNumber);
				if (order == null) return (Result.GetError("未找到订单", 201));
				if (order.GuestUserID != currentUser.ID) return (Result.GetError("无法操作该订单", 201));
				if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
				order.PingJia1Content = content;
				order.PingJia1Score = score ?? 3;
				order.PingJia1_2 = address ?? 3;
				order.PingJia1_3 = complete ?? 3;
				order.PingJia1_4 = time ?? 3;
				order.GuestAppraised = 1;
				var rel = order.DriverRelOrders.FirstOrDefault(z => z.EnsureState == 1);
				if (rel != null)
				{
					rel.AddDetail(6, currentUser.UserName, 1, currentUser.PhoneNumber, currentUser.Gender);
				}
				a.SaveChanges();
				return Result.GetSuccess();
			}
		}
		///<summary>
		/// 获取他自己的订单
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return (Result.GetError("未找到人员", 201));
					a.Configuration.LazyLoadingEnabled = false;
					a.Configuration.ProxyCreationEnabled = false;
					var order = a.Orders.Include(z => z.DriverRelOrders).FirstOrDefault(w => w.OrderNumber == orderNumber);
					if (order == null) return (Result.GetError("未找到订单", 201));
					return Result.GetSuccess(order);
				}
			}
		}
		///<summary>
		/// 获取订单列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		//        [allow]
		public ActionResult GetOrderByState(string phoneID, string phoneNumber, string orderStates, int skip = 0, int take = 10, string lastUpdateTime = "")
		{
			lock (orderObject)
			{
				using (var a = new SYSContext(true, false))
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return Result.GetError("未找到人员", 201);

					var states = orderStates.Split(',').Select(w => w.ToInt()).ToList();
					if (states.Count == 0) return Result.GetError("未提供状态参数(OrderStates)", 201);
					IQueryable<Order> orders = null;
					if (!string.IsNullOrWhiteSpace(lastUpdateTime) && skip == 0)
					{
						DateTime dt = Convert.ToDateTime(lastUpdateTime);
						orders = a.Orders.Where(w => w.GuestUserID == currentUser.ID && states.Contains(w.OrderState) && w.PublishTime > dt);
					}
					else orders = a.Orders.Where(w => w.GuestUserID == currentUser.ID && states.Contains(w.OrderState));
					var queryable = orders.Include(z => z.DriverRelOrders).OrderByDescending(z => z.PublishTime).Skip(skip).Take(take);
					a.Configuration.ProxyCreationEnabled = false;
					a.Configuration.LazyLoadingEnabled = false;
					var order = queryable.ToList();
					var userids = order.SelectMany(z => z.DriverRelOrders).Select(z => z.DriverID).ToList();
					var users = a.Users.Include(z => z.DriverInfo).Where(z => userids.Contains
						(z.ID)).ToList();
					users.ForEach(z => z.FilterInfo());
					var s = new[] { 2, 3, 4, 5, 6, 7 };
					order.ForEach(z =>
					{
						if (s.Contains(z.OrderState))//如果状态是2 3 4 5 6 7那么只保留一条记录
						{
							var li = z.DriverRelOrders.Where(x => x.EnsureState != 1).ToList();
							li.ForEach(x => z.DriverRelOrders.Remove(x));
						}
					});
					order.ForEach(x => x.DriverRelOrders.ForEach(z =>
					{
						z.Order = null;
						z.Driver.Orders = null;
						z.Driver.User.DriverInfo = null;
					}));
					var orderByState = Result.GetSuccess(order);
					return orderByState;
				}
			}
		}
		///<summary>
		/// 获取订单列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		//        [allow]
		public ActionResult GetOrderCountByState(string phoneID, string phoneNumber, string orderStates)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext(true, false))
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return Result.GetError("未找到人员", 201);

					var states = orderStates.Split(',').Select(w => w.ToInt()).ToList();
					if (states.Count == 0) return Result.GetError("未提供状态参数(OrderStates)", 201);
					var orders = a.Orders.Where(w => w.GuestUserID == currentUser.ID && states.Contains(w.OrderState)).Count();

					return Result.GetSuccess(orders);
				}
			}
		}
		///<summary>
		/// 获取一个订单中已确定的车主列表.
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		//        [allow]
		public ActionResult GetOrderDriverInfoByNumber(string phoneID, string phoneNumber, string orderNumber)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext(true, false))
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return Result.GetError("未找到人员", 201);

					var orders = a.Orders.Include(z => z.DriverRelOrders).FirstOrDefault(w => w.GuestUserID == currentUser.ID && w.OrderNumber == orderNumber);
					if (orders == null) return Result.GetError("未找到订单", 201);
					var driverIDs = orders.DriverRelOrders.Where(z => z.OrderState == 1).Select(z => z.DriverID).ToList();
					var users = a.Users.Include(z => z.DriverInfo).Where(w => driverIDs.Contains(w.ID)).ToList();
					a.Configuration.ProxyCreationEnabled = false;
					a.Configuration.LazyLoadingEnabled = false;
					orders.DriverRelOrders = null;
					users.ForEach(z =>
					{
						z.FilterInfo();
						if (z.DriverInfo != null)
						{
							z.DriverInfo.FilterInfo();
							z.OrderQuotation = z.DriverInfo.Orders.FirstOrDefault(w => w.OrderID == orders.ID).OrderQuotation;
							z.DriverInfo.Orders = null;
						}
					});
					var re = new
					{
						OrderInfo = orders,
						UserList = users,
					};

					//                    order.ForEach(w =>
					//                    {
					//                        w.PKey = null;
					//                        w.DriverRelOrders.ForEach(z => z.Driver = null);
					//                        w.DriverRelOrders.ForEach(z => z.Order = null);
					//                    });

					return Result.GetSuccess(re);
				}
			}
		}
		/// <summary>
		/// 获取一个订单货主对我的评价
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetAppraiseOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			var re = JsonHelper.FastJsonDeserialize<pushResult>(@"{""request_id"":1094821082,""response_params"":{""success_amount"":1,""msgids"":[""7393267383658183963""]}}");
			if (re != null)
			{
				if (re.error_code == null)//发送成功
				{
					//                    z.SuccessPushTime = DateTime.Now;
					//                    z.Success = 1;
				}
			}

			using (var a = new SYSContext())
			{
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));

				var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
				if (order == null) return (Result.GetError("未找到订单", 201));

				if (currentUser.ID != order.GuestUserID) return (Result.GetError("未找到订单关联信息", 201));
				//                a.SaveChanges();
				return Result.GetSuccess(order);
			}
		}

		/// <summary>
		/// 获取订单货主对我的评价列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetAppraiseOrders(string phoneID, string phoneNumber, int skip = 0, int length = 10)
		{
			using (var a = new SYSContext(true, false))
			{

				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));

				var list =
					from order in a.Orders
					from rel in order.DriverRelOrders
					where rel.DriverID == currentUser.ID && order.PingJia1Score >= 0
					select order;
				//                var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
				//                if (order == null) return (Result.GetError("非法操作", 201));
				//
				//                DriverRelOrder relOrder = order.DriverRelOrders.FirstOrDefault(w => w.DriverID == currentUser.ID);
				//                if (relOrder == null) return (Result.GetError("非法操作", 201));
				//                a.SaveChanges();
				return Result.GetSuccess(list.ToList());
			}
		}
		///<summary>
		/// 取消
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult CancelOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return (Result.GetError("未找到人员", 201));

					var order = a.Orders.Include(z => z.DriverRelOrders).FirstOrDefault(w => w.OrderNumber == orderNumber);
					if (order == null) return (Result.GetError("未找到订单", 201));
					if (order.OrderState == 2) return (Result.GetError("您已经选定车主，不能取消订单", 201));
					if (order.OrderState == 5) return (Result.GetError("订单已经完成，不能取消订单", 201));
					if (order.OrderState != 0 &&
						order.OrderState != 1 &&
						order.OrderState != 2) return (Result.GetError("订单状态错误", 201));
					a.Configuration.LazyLoadingEnabled = false;
					a.Configuration.ProxyCreationEnabled = false;
					var relOrder = order.DriverRelOrders.Where(w => w.OrderID == order.ID).ToList();

					//需要通知的列表
					var relOrder1 = relOrder.Where(z => z.OrderState == 0 || z.OrderState == 1 || z.OrderState == 2).ToList();
					relOrder1.ForEach(w =>
					{
						w.AddDetail(-6, currentUser.UserName, 1, currentUser.PhoneNumber, currentUser.Gender);
						w.OrderState = -6;
						PushDriverHelper.OrderCancel(w.DriverID, order.OrderNumber, currentUser);
					});

					order.Canceled = 1;
					order.OrderState = -6;
					order.CancelDateTime = DateTime.Now;
					a.SaveChanges();
					var routeId = order.RouteId;
					string start = order.StartName;
					string end = order.EndName;
					if (routeId != null)
					{
						var route = a.Routes.FirstOrDefault(x => x.Id == routeId);
						if (route != null)
						{
							start = route.Start;
							end = route.End;
						}
					}
					//var message = string.Format(MvcApplication.M_CancelTemplate, currentUser.UserName, currentUser.Gender.GetGenderStr(), order.PublishTime.Value.ToString("yyyy-MM-dd"), start, end);
					//ValidCodeAPI.SendMessage(currentUser.PhoneNumber, message);
					return Result.GetSuccess();
				}
			}
		}
		public ActionResult GetDriverByRouteId(long RouteId)
		{
			using (var a = new SYSContext())
			{
				var driverIds = a.DriverRoutes.Where(x => x.RouteId == RouteId).Select(x => x.DriverId).ToList();
				var list = a.Drivers.Include(z => z.User).Where(x => driverIds.Contains(x.ID) && x.Valid == 1).ToList();

				List<object> drivers = new List<object>();
				foreach (var d in list)
				{
					drivers.Add(new { Id = d.ID, Name = d.User.UserName, Phone = d.User.PhoneNumber });
				}
				return Result.GetSuccess(drivers);
			}

		}

		public ActionResult GetEndRouteByStart(string start)
		{
			using (var a = new SYSContext())
			{
				var routes = a.Routes.Where(x => x.Start == start).ToList();


				List<object> list = new List<object>();
				foreach (var r in routes)
				{
					list.Add(new { Id = r.Id, Start = r.Start, End = r.End });
				}
				return Result.GetSuccess(list);
			}

		}

		public ActionResult RequestDrivers()
		{
			using (var a = new SYSContext())
			{
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				IQueryable<Driver> queryable = a.Drivers.Include(z => z.User);
				queryable = queryable.Where(z => z.Valid == 1);

				var list = queryable
					.OrderByDescending(z => z.User.JoinDate)
					.ToList();

				List<object> drivers = new List<object>();
				foreach (var d in list)
				{
					drivers.Add(new { Id = d.ID, Name = d.User.UserName, Gender = d.User.Gender, Phone = d.User.PhoneNumber });
				}
				return Result.GetSuccess(drivers);
			}
		}
		
		public ActionResult ClientRequestRouteAll(string phoneID, string phoneNumber, string startKey, string endKey)
        {
            var list = new List<Route>();
            try
            {
                using (var c = new SYSContext())
                {
                    var result = new List<Route>();
                    if (!string.IsNullOrWhiteSpace(startKey) && !string.IsNullOrWhiteSpace(endKey)) result = c.Routes.Where(x => x.Start.Contains(startKey) && x.End.Contains(endKey)).ToList();
                    else if (!string.IsNullOrWhiteSpace(startKey) && string.IsNullOrWhiteSpace(endKey))
                    {
                        var qr = (from r in c.Routes where r.Start.Contains(startKey) select r.Start).Distinct();
                        var tlist = qr.ToList();
                        foreach (var str in tlist)
                        {
                            result.Add(new Route() { Start = str,End="",City="",Id=0,AreaCode="" });
                        }
                    }
                    else if (string.IsNullOrWhiteSpace(startKey) && !string.IsNullOrWhiteSpace(endKey)) result = c.Routes.Where(x => x.End.Contains(endKey)).ToList();
                    else result = c.Routes.ToList();
                    list = result.Select(x => new Route() { Id = x.Id, Start = x.Start, End = x.End, City=x.City,AreaCode=x.AreaCode }).ToList();

                }
            }
            catch
            {

            }
            return Result.GetSuccess(list);
        }

	    public ActionResult ClientRequestRouteAllByArea(string phoneID, string phoneNumber, string startKey, string endKey,string area)
	    {
            var list = new List<Route>();
            try
            {
                using (var c = new SYSContext())
                {
                    var result = new List<Route>();
                    if (!string.IsNullOrWhiteSpace(startKey) && !string.IsNullOrWhiteSpace(endKey))
                    {
                        if (!string.IsNullOrWhiteSpace(area)) result = c.Routes.Where(x => x.Start.Contains(startKey) && x.End.Contains(endKey)&&x.AreaCode.Equals(area)).ToList();
                        else result = c.Routes.Where(x => x.Start.Contains(startKey) && x.End.Contains(endKey)).ToList();
                    }
                    else if (!string.IsNullOrWhiteSpace(startKey) && string.IsNullOrWhiteSpace(endKey))
                    {
                        if (!string.IsNullOrWhiteSpace(area))
                        {
                            var qr = (from r in c.Routes where r.Start.Contains(startKey) && r.AreaCode.Equals(area) select r.Start).Distinct();
                            var tlist = qr.ToList();
                            foreach (var str in tlist)
                            {
                                result.Add(new Route() { Start = str, End = "", City = "", Id = 0, AreaCode = "" });
                            }
                        }
                        else
                        {
                            var qr = (from r in c.Routes where r.Start.Contains(startKey) select r.Start).Distinct();
                            var tlist = qr.ToList();
                            foreach (var str in tlist)
                            {
                                result.Add(new Route() { Start = str, End = "", City = "", Id = 0, AreaCode = "" });
                            }
                        }
                        
                    }
                    else if (string.IsNullOrWhiteSpace(startKey) && !string.IsNullOrWhiteSpace(endKey))
                    {
                        if (!string.IsNullOrWhiteSpace(area))
                            result = c.Routes.Where(x => x.End.Contains(endKey) && x.AreaCode.Equals(area)).ToList();
                        else result = c.Routes.Where(x => x.End.Contains(endKey)).ToList();
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(area)) result = c.Routes.Where(x=>x.AreaCode.Equals(area)).ToList();
                        else result = c.Routes.ToList();
                    }
                    list = result.Select(x => new Route() { Id = x.Id, Start = x.Start, End = x.End, City = x.City, AreaCode = x.AreaCode }).ToList();

                }
            }
            catch
            {

            }
            return Result.GetSuccess(list);
	    }
		//        /// <summary>
		//        /// 更新个人基本信息
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <param name="line"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult UpdateUserInfo(string phoneID, string phoneNumber, User line)
		//        {
		//            Para.Required("driver", line);
		//            using (var a = new SYSContext())
		//            {
		//                bool isNew = true;
		//                if (line.ID == Guid.Empty)
		//                {
		//                    line.ID = Guid.NewGuid();
		//                }
		//                else
		//                {
		//                    var currentUser = a.Users.FirstOrDefault(w => w.ID == line.ID);
		//                    if (currentUser != null)
		//                    {
		//                        if (currentUser.PhoneNumber != phoneNumber)//操作别人的订单
		//                        {
		//                            return (Result.GetError("非法操作", 201));
		//                        }
		//                        isNew = false;
		//                        ReflectionHelper.SetAttributeValues(currentUser, line);
		//                        a.Users.AddOrUpdate(line);
		//                    }
		//
		//                }
		//                if (isNew)
		//                {
		//                    a.Users.AddOrUpdate(line);
		//                }
		//                a.SaveChanges();
		//            }
		//            return Result.GetSuccess();
		//        } 

	}

}