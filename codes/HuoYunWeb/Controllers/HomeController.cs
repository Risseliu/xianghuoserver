﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using EntityFramework.Extensions;
using Fw.Serializer;
using HuoYunBLL;
using HuoYunWeb.App_Code;
using HuoYunWeb.Models;

namespace HuoYunWeb.Controllers
{
	[WebAuthorize]
	public class HomeController : UserBaseController
	{
		//执行Action前
		protected override IAsyncResult BeginExecute(RequestContext requestContext, AsyncCallback callback, object state)
		{
			//            SessionCode = DateTime.Now.ToString("yyyyMMddHHmmss");
			//            StringBuilder sb = new StringBuilder();
			//            var request = requestContext.HttpContext.Request;
			//            sb.AppendLine("执行前" + SessionCode);
			//            sb.AppendLine(request.RawUrl);
			//            var allKeys = request.Form.AllKeys;
			//            var keys = allKeys.Concat(request.QueryString.AllKeys).ToList();
			//            keys.ForEach(w =>
			//            {
			//                var args = request[w];
			//                if (string.IsNullOrEmpty(args))
			//                    sb.AppendLine(string.Format("{0}:{1}", w, args));
			//            });
			//            LogHelper.WriteLog(GetType(), sb.ToString());
			return base.BeginExecute(requestContext, callback, state);
		}
		// 登陆页面
		[AllowAnonymous]
		public ActionResult WebLogin()
		{
			return View(new Admin { Account = "", Password = "" });
		}
		// 登陆页面
		[AllowAnonymous]
		public ActionResult DialogLogin()
		{

			return View(new Admin { Account = "", Password = "" });
		}


		// 登陆页面
		[HttpPost]
		[AllowAnonymous]
		public ActionResult DialogLogin(Admin user)
		{
			var acc = user.Account;
			var pwd = user.Password;
			using (var a = new SYSContext())
			{
				var item = a.Admins.FirstOrDefault(z => z.Account == acc);
				if (item != null)
				{
					if (item.Password == pwd)
					{
						UserHelper.SetLogin(item);
						//                        return View("WebLogin",item);
						return getDefaultJsonResult(null, null, "登陆成功");
					}
					return getDefaultJsonResult(null, null, "密码错误", 300);

				}
				else
				{
					return getDefaultJsonResult(null, null, "账号错误", 300);
				}
			}

			return View(user);
		}
		// 登陆页面
		[HttpPost]
		[AllowAnonymous]
		public ActionResult WebLogin(Admin user)
		{
			var acc = user.Account;
			var pwd = user.Password;
			if (string.IsNullOrEmpty(user.Account))
			{
				ViewBag.ErrorMessage = "账号不能为空";
			}
			else if (string.IsNullOrEmpty(user.Password))
			{
				ViewBag.ErrorMessage = "密码不能为空";
			}
			else
			{
				using (var a = new SYSContext())
				{
					var item = a.Admins.FirstOrDefault(z => z.Account == acc);
					if (item != null)
					{
						if (item.Password == pwd)
						{
							UserHelper.SetLogin(item);
                           
                            return RedirectToAction("Index", "Home");
						}
						ViewBag.ErrorMessage = "密码错误";
					}
					else
					{
						ViewBag.ErrorMessage = "账号错误";

					}
				}
			}
			//            Response.Redirect("~/");

			return View(user);
		}

		public ActionResult Logout()
		{

			UserHelper.SetLogout();
			return RedirectToAction("Index", "Home");
		}
		// [AllowAnonymous]
		public ActionResult Index()
		{
			using (var a = new SYSContext())
			{
				ViewBag.CustomCount = a.Customers.Count();
				ViewBag.DriverCount = a.Drivers.Count();
				ViewBag.OrderCount = a.Orders.Count();
				ViewBag.OrderPrice = a.Orders.Any() ? a.Orders.Sum(z => z.Price) : 0;
				var dateTime = DateTime.Now;
				CountDataByDay(a, dateTime);
				CountDataByWeek(a, dateTime);
				CountDataByMonth(a, dateTime);
				CountDataByYear(a, dateTime);
				ViewBag.DriverValidCount = a.Drivers.Count(w => w.ValidSubmit == 1 && w.Valid == 0);
			    var item = UserHelper.CurrentUser();
                if (item!=null&&item.RoleId != null && item.RoleId==new Guid("d1979690-cc42-4ac4-b73a-8bbd084648dc"))
                {
                    ViewBag.userRole = "agent";
                }
                else ViewBag.userRole = "admin";
			}
			ViewBag.ValidDriverEnabled = MyConfigManage.Current.ValidDriverEnabled;

			return View();

			ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
			return View();
		}
        /// <summary>
        /// 代理商首页
        /// </summary>
        /// <returns></returns>
        public ActionResult AgentIndex()
        {
            return View();
        }
		/// <summary>
		/// 获取当天的统计数据
		/// </summary>
		/// <param name="currentContext"></param>
		/// <param name="inputDateTime"></param>
		private void CountDataByDay(SYSContext currentContext, DateTime inputDateTime)
		{
			var st = new DateTime(inputDateTime.Year, inputDateTime.Month, inputDateTime.Day);
			var et = st.AddDays(1);
			ViewBag.CustomCount1 = currentContext.Customers.Count(z => z.UserInfo.JoinDate != null && z.UserInfo.JoinDate > st && z.UserInfo.JoinDate != null && z.UserInfo.JoinDate < et);
			ViewBag.DriverCount1 = currentContext.Drivers.Count(z => z.User.JoinDate != null && z.User.JoinDate > st && z.User.JoinDate != null && z.User.JoinDate < et);
			ViewBag.OrderCount1 = currentContext.Orders.Count(z => z.PublishTime > st && z.PublishTime < et);
			if (ViewBag.OrderCount1 > 0)
			{
				ViewBag.OrderPrice1 = currentContext.Orders.Where(z => z.PublishTime > st && z.PublishTime < et)
					.Sum(z => z.Price);
			}
			else
			{
				ViewBag.OrderPrice1 = 0;
			}
		}
		/// <summary>
		/// 获取本周的统计数据，按天汇总
		/// </summary>
		/// <param name="currentContext"></param>
		/// <param name="inputDateTime"></param>
		private void CountDataByWeek(SYSContext currentContext, DateTime inputDateTime)
		{
			DateTime weekStartDate = GetWeekFirstDayMon(inputDateTime);
			DateTime weekEndDate = GetWeekLastDaySun(inputDateTime);
			string weekStartDateString = weekStartDate.ToString("yyyy-MM-dd");
			string weekEndDateString = weekEndDate.ToString("yyyy-MM-dd");
			SqlParameter[] sqlParams = new SqlParameter[3];
			sqlParams[0] = new SqlParameter("@start", weekStartDateString);
			sqlParams[1] = new SqlParameter("@end", weekEndDateString);
			sqlParams[2] = new SqlParameter("@countType", 1);
			var result = currentContext.Database.SqlQuery<CustomerCountData>("exec GatherAllData @start,@end,@countType", sqlParams).ToList();
			ViewBag.WeekCountData = result;

		}

		/// <summary>
		/// 获取本月的统计数据，按周汇总
		/// </summary>
		/// <param name="currentContext"></param>
		/// <param name="inputDateTime"></param>
		private void CountDataByMonth(SYSContext currentContext, DateTime inputDateTime)
		{
			DateTime startMonth = inputDateTime.AddDays(1 - inputDateTime.Day); //本月月初
			DateTime endMonth = startMonth.AddMonths(1).AddDays(-1); //本月月末
			string startMonthString = startMonth.ToString("yyyy-MM-dd");
			string endMonthString = endMonth.ToString("yyyy-MM-dd");
			SqlParameter[] sqlParams = new SqlParameter[3];
			sqlParams[0] = new SqlParameter("@start", startMonthString);
			sqlParams[1] = new SqlParameter("@end", endMonthString);
			sqlParams[2] = new SqlParameter("@countType", 2);
			var result = currentContext.Database.SqlQuery<CustomerCountData>("exec GatherAllData @start,@end,@countType", sqlParams).ToList();
			ViewBag.MonthCountData = result;
		}
		/// <summary>
		/// 获取本年度的统计数据，按月汇总
		/// </summary>
		/// <param name="currentContext"></param>
		/// <param name="inputDateTime"></param>
		private void CountDataByYear(SYSContext currentContext, DateTime inputDateTime)
		{
			DateTime startYear = new DateTime(inputDateTime.Year, 1, 1); //本年年初
			DateTime startMonth = inputDateTime.AddDays(1 - inputDateTime.Day); //本月月初
			DateTime endYear = startMonth.AddMonths(1).AddDays(-1); //截止到目前为止月的最后一天
			string startYearString = startYear.ToString("yyyy-MM-dd");
			string endYearString = endYear.ToString("yyyy-MM-dd");
			SqlParameter[] sqlParams = new SqlParameter[3];
			sqlParams[0] = new SqlParameter("@start", startYearString);
			sqlParams[1] = new SqlParameter("@end", endYearString);
			sqlParams[2] = new SqlParameter("@countType", 3);
			var result = currentContext.Database.SqlQuery<CustomerCountData>("exec GatherAllData @start,@end,@countType", sqlParams).ToList();
			ViewBag.YearCountData = result;
		}
		/// <summary>
		/// 得到本周第一天(以星期一为第一天)
		/// </summary>
		/// <param name="datetime"></param>
		/// <returns></returns>
		private DateTime GetWeekFirstDayMon(DateTime datetime)
		{
			//星期一为第一天
			int weeknow = Convert.ToInt32(datetime.DayOfWeek);

			//因为是以星期一为第一天，所以要判断weeknow等于0时，要向前推6天。
			weeknow = (weeknow == 0 ? (7 - 1) : (weeknow - 1));
			int daydiff = (-1) * weeknow;

			//本周第一天
			string FirstDay = datetime.AddDays(daydiff).ToString("yyyy-MM-dd");
			return Convert.ToDateTime(FirstDay);
		}
		/// <summary>
		/// 得到本周最后一天(以星期天为最后一天)
		/// </summary>
		/// <param name="datetime"></param>
		/// <returns></returns>
		public DateTime GetWeekLastDaySun(DateTime datetime)
		{
			//星期天为最后一天
			int weeknow = Convert.ToInt32(datetime.DayOfWeek);
			weeknow = (weeknow == 0 ? 7 : weeknow);
			int daydiff = (7 - weeknow);

			//本周最后一天
			string LastDay = datetime.AddDays(daydiff).ToString("yyyy-MM-dd");
			return Convert.ToDateTime(LastDay);
		}
		/// <summary>1
		/// 
		/// </summary>
		/// <param name="validDriverEnabled">0:车主验证后才能收到推送,1不验证也能收到推送</param>
		/// <returns></returns>
		/// 
		//        [AllowAnonymous]
		public ActionResult Config1(int? validDriverEnabled)
		{
			if (validDriverEnabled == null) throw new Exception("validDriverEnabled不能为空");
			MyConfigManage.Current.ValidDriverEnabled = validDriverEnabled.Value;
			MyConfigManage.SaveConfig();
			return Result.GetSuccess();
		}
		public ActionResult About()
		{
			ViewBag.Message = "Your app description page.";
			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";
			return View();
		}
	}
	/// <summary>
	/// 客户端统计数据类
	/// </summary>
	public class CustomerCountData
	{
		/// <summary>
		/// 统计时间
		/// </summary>
		public String createDate { set; get; }
		/// <summary>
		/// 订单总数
		/// </summary>
		public int totalCount { set; get; }
		/// <summary>
		/// 订单总额
		/// </summary>
		public double totalPrice { set; get; }
		/// <summary>
		/// 车主数量
		/// </summary>
		public int driverCount { set; get; }
		/// <summary>
		/// 货主数量
		/// </summary>
		public int customerCount { set; get; }
	}
}
