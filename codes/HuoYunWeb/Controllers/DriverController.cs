﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Mvc;
using BaiDuAPICom;
using Fw.Extends;
using Fw.Reflection;
using Fw.Serializer;
using HuoYunBLL;
using HuoYunWeb.App_Code;
using HuoYunWeb.App_Code.Push;
using HuoYunWeb.Models;
using ServerFw.Extends;
using System.Data.SqlClient;
//using WebGrease.Css.Extensions;
using DriverLine = HuoYunWeb.Models.DriverLine;
using System.Data.Entity;
using HuoYunWeb;

namespace HuoYunWeb.Controllers
{
	//    [APIAuthorize]
	public class DriverController : UserBaseController
	{
		/// <summary>
		/// 检查版本更新
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		[AllowAnonymous]
		public ActionResult CheckVersion(string phoneID, string phoneNumber)
		{
			var v = MvcApplication.DriverVersion;
			var p = MvcApplication.DriverUpdatePath;
			var u = MvcApplication.DriverUpdateLog;
			return Result.GetSuccess(new
			{
				versionCode = v,
				downloadUrl = p,
				updateLog = u

			});
		}
        /// <summary>
        /// 获取所有的区域
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
       [AllowAnonymous]
        public ActionResult GetAllArea()
        {
            using (var a = new SYSContext())
            {
                var areas = a.Areas;
                return Result.GetSuccess(areas);
            }
        }
		/// <summary>
		/// 返回订单的执行流
		/// 订单操作
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public ActionResult GetOrderFlow(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			using (var a = new SYSContext())
			{
				var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
				//未找到货主或订单
				if (order == null) return (Result.GetError("未找到订单", 201));
				if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
				var driver = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (driver == null) return (Result.GetError("未找到人员", 201));

				//货主和订单对不上
				//                if (order.GuestUserID != custom.ID) return (Result.GetError("无法操作该订单", 201));
				var driverRelOrders = order.DriverRelOrders.ToList();
				var rel = driverRelOrders.FirstOrDefault(z => z.EnsureState == 1);
				if (rel.DriverID != driver.ID) return (Result.GetError("无法操作该订单", 201));
				var relIDs = driverRelOrders.Select(z => z.ID).ToList();
				a.Configuration.LazyLoadingEnabled = false;
				a.Configuration.ProxyCreationEnabled = false;
				var reldetails = a.DriverRelOrderDetails.Where(z => relIDs.Contains(z.DriverRelOrderID)).OrderBy(z => z.AtThatTime).ToList();
				reldetails.ForEach(z => z.DriverRelOrder = null);
				order.DriverRelOrders = null;
				return Result.GetSuccess(new
				{
					Order = order,
					Detail = reldetails,
				});
			}
		}

		//        /// <summary>
		//        /// 注册
		//        /// </summary>
		//        /// <param name="phoneId"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <param name="password"></param>
		//        /// <param name="validCode"></param>
		//        /// <returns>Bool</returns>
		//        [AllowAnonymous]
		//        public ActionResult Register([Required]string phoneId
		//            , string phoneNumber,
		//            string password,
		//            string validCode,
		//            string userName,
		//            int? gender)//Int 性别,(1男,2女)
		//        {
		//            Para.Required("phoneId", phoneId);
		//            Para.Required("phoneNumber", phoneNumber);
		//            gender = gender ?? 0;
		////            Para.Required("password", password);
		////            Para.Required("validCode", validCode);
		//            string pk = new Random().Next(1000000, 9999999).ToString();
		//            DateTime pKeyOverDate = DateTime.Now.AddDays(1);
		//            using (var a = new SYSContext())
		//            {
		//                var user = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		//                if (user != null)
		//                {
		//                    return (Result.GetResult(true, "手机号已经注册", "false",205));
		//                }
		//                var sys_ValidCode = ValidCodeManage.GetCode(phoneId);
		//                if (!string.Equals(sys_ValidCode, validCode, StringComparison.OrdinalIgnoreCase))
		//                {
		//                    return (Result.GetResult(true, "验证码错误", "false", 206));
		//                }
		//                a.Users.Add(new User
		//                {
		//                    ID = Guid.NewGuid(),
		//                    Account = phoneNumber,
		//                    PhoneNumber = phoneNumber,
		//                    Password = password,
		//                    PhoneID = phoneId,
		//                    UserName = userName,
		//                    Gender = gender.Value,
		//                    PKey = pk,
		//                    PKeyOverDate = pKeyOverDate,
		//                });
		//                a.SaveChanges();
		//            }
		//            return (Result.GetResult(true, null, new LoninResult { PK = pk, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 }.ToJson()));
		//        }
		//        /// <summary>
		//        /// 登陆
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <param name="password"></param>
		//        /// <returns>LoninResult</returns>
		//        [AllowAnonymous]
		//        public ActionResult Login(string phoneID, string phoneNumber, string validCode)
		//        {
		//            Para.Required("phoneId", phoneID);
		//            Para.Required("phoneNumber", phoneNumber);
		////            Para.Required("password", password);
		//            string pk = new Random().Next(1000000, 9999999).ToString();
		//            DateTime pKeyOverDate = DateTime.Now.AddDays(1);
		//
		//            using (var a = new SYSContext())
		//            {
		//                var user = a.Users.FirstOrDefault(w => w.Account == phoneNumber);
		//                if (user == null) return (Result.GetLoginResult(false,"账号未找到", 101 ,null,DateTime.Now));
		//                if (user.Locked == 1) return (Result.GetLoginResult(false, "账号冻结", 102, null, DateTime.Now));
		////                if (user.Password != password) return (Result.GetLoginResult(false, "密码错误", 103, null, DateTime.Now));
		//                var sys_ValidCode = ValidCodeManage.GetCode(phoneID);
		//                if (!string.Equals(sys_ValidCode, validCode, StringComparison.OrdinalIgnoreCase))
		//                {
		//                    return (Result.GetLoginResult(false, "验证码错误", 103, null, DateTime.Now));
		//                }
		//                user.PKey = pk;
		//                user.PKeyOverDate = pKeyOverDate;
		//                a.SaveChanges();
		//            }
		//            return (Result.GetSuccess(new LoninResult { PK = pk, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 }));
		//        }
		//        /// <summary>
		//        /// 生成验证码
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>void</returns>
		//        [AllowAnonymous]
		//        public ActionResult SendValidCode(string phoneID, string phoneNumber)
		//        {
		//            Para.Required("phoneID", phoneID);
		//            Para.Required("phoneNumber", phoneNumber);
		//            var code = ValidCodeManage.NewCode();
		//            ValidCodeManage.SetCode(phoneNumber,code);
		//            ValidCodeAPI.Send(phoneNumber,code);
		//            return (Result.GetSuccess(true));
		//        }

		/// <summary>
		/// 添加或修改线路
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="line"></param>
		/// <returns>bool</returns>
		public ActionResult CombineLine(string phoneID, string phoneNumber, string line)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			var line2 = JsonHelper.FastJsonDeserialize<DriverLine>(line);
			Para.Required("line", line2);
			using (var a = new SYSContext())
			{
				var user = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (line2.ID == Guid.Empty)
				{
					var exist = a.DriverLines.FirstOrDefault(z => z.DriverID == user.ID && z.StartName == line2.StartName
																  && z.EndName == line2.EndName);
					if (exist != null)
					{
						return (Result.GetError("线路已经存在", 201));
					}
				}
				if ((line2.StartPointX == 0 || line2.StartPointY == 0) && !string.IsNullOrEmpty(line2.StartName))
				{
					double x, y;
					BaiDuAPI.GetLatLngByName(line2.StartName, out x, out y, line2.StartCityName);
					line2.StartPointX = x;
					line2.StartPointY = y;
				}
				if ((line2.EndPointX == 0 || line2.EndPointY == 0) && !string.IsNullOrEmpty(line2.EndName))
				{
					double x, y;
					BaiDuAPI.GetLatLngByName(line2.EndName, out x, out y, line2.EndCityName);
					line2.EndPointX = x;
					line2.EndPointY = y;
				}
				if (user == null) return (Result.GetError("没有找到指定的人员", 201));
				line2.DriverID = user.ID;
				//                if (line2.DriverID != user.ID) return (Result.GetError("人员和线路不匹配", 201));
				bool isNew = true;
				if (line2.ID == Guid.Empty)
				{
					line2.ID = Guid.NewGuid();
				}
				else
				{
					var currentLine = a.DriverLines.FirstOrDefault(w => w.ID == line2.ID);
					if (currentLine != null)//线路已经存在
					{
						//操作别人的订单
						if (currentLine.DriverID != user.ID) return (Result.GetError("人员和线路不匹配", 201));
						isNew = false;
						var exist = a.DriverLines.FirstOrDefault(z => z.DriverID == user.ID && z.ID != currentLine.ID && z.StartName == line2.StartName
																 && z.EndName == line2.EndName);
						if (exist != null)
						{
							return (Result.GetError("线路已经存在", 201));
						}
						var id = currentLine.ID;
						ReflectionHelper.SetBaseAttributeValues(currentLine, line2);
						currentLine.ID = id;
					}
					else//线路不存在,需要添加
					{
						var exist = a.DriverLines.FirstOrDefault(z => z.DriverID == user.ID && z.StartName == line2.StartName
																 && z.EndName == line2.EndName);
						if (exist != null)
						{
							return (Result.GetError("线路已经存在", 201));
						}
					}
				}
				if (isNew)
				{
					a.DriverLines.Add(line2);
				}
				a.SaveChanges();
			}
			return Result.GetSuccess();
		}
		/// <summary>
		/// 添加或修改线路
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="line"></param>
		/// <returns>bool</returns>
		public ActionResult DeleteLine(string phoneID, string phoneNumber, Guid? lineID)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("lineID", lineID);

			using (var a = new SYSContext())
			{
				var user = a.Users.Include(z => z.DriverInfo).FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (user == null) return (Result.GetError("未找到人员", 201));
				var line = user.DriverInfo.DriverLines.FirstOrDefault(z => z.ID == lineID);
				if (line == null) return (Result.GetError("未找到路线", 201));
				a.DriverLines.Remove(line);
				a.SaveChanges();
			}
			return Result.GetSuccess();
		}

		/// <summary>
		/// 获取个人基本信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetDriverRoute(string phoneID, string phoneNumber)
		{
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);

				if (currentUser == null) return (Result.GetError("没有找到人员", 201));
				if (currentUser.DriverInfo == null) return (Result.GetError("未开通车主功能", 201));
				var routeIds = a.DriverRoutes.Where(x => x.DriverId == currentUser.ID).Select(x => x.RouteId).ToList();
				var routes = a.Routes.Where(x => routeIds.Contains(x.Id));

				return Result.GetResult(true, null, routes.ToJson());
			}
		}

		/// <summary>
		/// 获取个人基本信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetDriverLine(string phoneID, string phoneNumber)
		{
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);

				if (currentUser == null) return (Result.GetError("没有找到人员", 201));
				if (currentUser.DriverInfo == null) return (Result.GetError("未开通车主功能", 201));
				ICollection<DriverLine> driverLines = currentUser.DriverInfo.DriverLines;
				a.Configuration.ProxyCreationEnabled = false;
				a.Configuration.LazyLoadingEnabled = false;
				driverLines.ToList().ForEach(w =>
				{
					w.Driver = null;
				});
				return Result.GetResult(true, null, driverLines.ToJson());
			}
		}

		/// <summary>
		/// 注册
		/// </summary>
		/// <param name="phoneId"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="password"></param>
		/// <param name="validCode"></param>
		/// <returns>Bool</returns>
		[AllowAnonymous]
		public ActionResult Register([Required]string phoneId,
			string phoneNumber,
			string password,
			string validCode,
			string userName,
			string registerAddress,
			int? gender = 1,
			int? deviceType = 3)
		{
			lock (RegisterFlag)
			{
				Para.Required("phoneId", phoneId);
				Para.Required("phoneNumber", phoneNumber);
				string pk = new Random().Next(1000000, 9999999).ToString();
				DateTime pKeyOverDate = DateTime.Now.AddDays(1);
				int Register = 0;

				var sys_ValidCode = ValidCodeManage.GetCode(phoneNumber);
				if (string.IsNullOrEmpty(sys_ValidCode) == null ||
					string.IsNullOrEmpty(validCode) ||
					(!string.Equals(validCode, MvcApplication.PublicValidCode) && !string.Equals(sys_ValidCode, validCode, StringComparison.OrdinalIgnoreCase)))
				{
					return (Result.GetLoginResult(false, "验证码错误", 103, null, DateTime.Now));
				}

				var device = 3;
				if (deviceType != null)
				{
					device = deviceType.Value;
				}

				using (var a = new SYSContext())
				{
					var realAddress = string.Empty;
					var user = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (user != null)//有人员基本信息
					{
						if (user.Locked == 1) return (Result.GetLoginResult(false, "账号冻结", 102, null, DateTime.Now));
						user.PKey = pk;
						user.PKeyOverDate = pKeyOverDate;
						user.DeviceType = device;
						realAddress = user.RealAddress;
						if (user.DriverInfo == null)//没有车主信息
						{
							user.DriverInfo = new Driver
							{
								ID = user.ID,
							};
							Register = 1;
						}
						user.AddDriver();
						a.SaveChanges();
						return Result.GetSuccess(new LoninResult { PK = pk, Register = Register, UserID = user.ID.ToString(), UserState = user.UserState, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 });
					}

					var newGuid = Guid.NewGuid();
					var entity = new User
					{
						ID = newGuid,
						Account = phoneNumber,
						PhoneNumber = phoneNumber,
						Password = password,
						PhoneID = phoneId,
						UserName = userName,
						RegisterAddress = registerAddress,
						RealAddress = realAddress,
						Gender = gender.Value,
						PKey = pk,
						PKeyOverDate = pKeyOverDate,
						DeviceType = device,
						DriverInfo = new Driver
						{
							ID = newGuid,
						}
					};
					var numbers = a.Users.Select(z => z.UserNumber).ToList();
					if (numbers.Any())
					{
						entity.UserNumber = numbers.Max() + 1;
					}
					else
					{
						entity.UserNumber = 10000;
					}
					entity.AddDriver();
					a.Users.Add(entity);
					a.SaveChanges();
					return Result.GetSuccess(new LoninResult { PK = pk, Register = 1, UserID = entity.ID.ToString(), UserState = entity.UserState, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 });
				}

			}
		}

		/// <summary>
		/// 更新司机个人信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="driver"></param>
		/// <returns>bool</returns>
		public ActionResult UpdateDriverInfo(string phoneID, string phoneNumber, string driver)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("driver", driver);
			var driver1 = JsonHelper.FastJsonDeserialize<Driver>(driver);
			if (driver1 == null) throw new Exception("driver为空");
			//            if (driver1.ID == Guid.Empty) throw new Exception("driver1.ID为空");
			using (var a = new SYSContext())
			{
				var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (user == null) return Result.GetError("没有找到人员", 201);
				driver1.ID = user.ID;
				var currentDriver = user.DriverInfo;
				a.Configuration.ProxyCreationEnabled = false;
				if (currentDriver == null)
				{
					driver1.Valid = 0;
					driver1.ID = user.ID;
					driver1.User = user;
					a.Drivers.Add(driver1);
				}
				else
				{
					var id = currentDriver.ID;
					var ab = currentDriver.Valid;
					ReflectionHelper.SetBaseAttributeValues(currentDriver, driver1,
						new[] { "ID", "Valid", "ImagePath", "CheLiang2", "CheLiang1", "XingShiZheng", "JiaZhao", "ShenFenZheng", "Remark" });
					currentDriver.Valid = ab;
					currentDriver.ID = id;
				}
				user.AddDriver();
				a.SaveChanges();
			}
			return Result.GetSuccess();
		}

		/// <summary>
		/// 更新个人基本信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="image"></param>
		/// <param name="imageType">0:头像,1:身份证,2驾驶证,3行驶证,4车辆正面图,5车辆45度图</param>
		/// <returns>bool</returns>
		public ActionResult UploadImage(string phoneID, string phoneNumber, string image, int imageType = 0)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("image", image);
			Para.Required("imageType", imageType);
			using (var a = new SYSContext())
			{
				//                var imageType1 = imageType ?? 0;
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("没有找到人员", 201));
				ImageManage.SaveUserImage(currentUser.ID, image, imageType);
				string userImagePath = ImageManage.GetUserImageName(imageType);
				if (imageType == 0)
				{
					currentUser.ImagePath = userImagePath;
				}
				else
				{
					var di = currentUser.DriverInfo;
					if (di == null) return Result.GetError("没有开通车主功能.", 201);
					if (imageType == 1)
					{
						currentUser.ShenFenZhengUpload = 1;
						di.ShenFenZheng = userImagePath;
					}
					if (imageType == 2) di.JiaZhao = userImagePath;
					if (imageType == 3) di.XingShiZheng = userImagePath;
					if (imageType == 4) di.CheLiang1 = userImagePath;
					if (imageType == 5) di.CheLiang2 = userImagePath;
				}
				a.SaveChanges();
			}
			return Result.GetSuccess();
		}

		///<summary>
		/// 获取司机需要的推送信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetNewOrders(string phoneID, string phoneNumber)
		{
			using (var a = new SYSContext())
			{

				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));
				var list = currentUser.DriverInfo.Orders.Where(w => w.OrderState == 0).Take(10).Select(w => w.Order).ToList();

				a.Configuration.ProxyCreationEnabled = false;
				list.ForEach(w =>
				{
					w.DriverRelOrders = null;
				});
				return Result.GetResult(true, null, list.ToJson());
			}
		}
		///<summary>
		/// 提交认证
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult SubmitValid(string phoneID, string phoneNumber)
		{
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));
				var list = currentUser.DriverInfo;
				if (list.ValidSubmit == 0)
				{
					list.ValidSubmit = 1;
					list.ValidSubmitDateTime = DateTime.Now;
				}
				else
				{
					return (Result.GetError("已经通过验证", 201));
				}
				return Result.GetSuccess();
			}
		}

		static object orderObject = new object();
		///<summary>
		/// 抢单
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult EnsureOrder(string phoneID, string phoneNumber, string orderNumber, int orderQuotation = 0)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return (Result.GetError("未找到人员", 201));


					var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);

					if (order == null) return (Result.GetError("未找到订单", 201));
					if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
					var driverRelOrders = order.DriverRelOrders.ToList();
					DriverRelOrder relOrder = driverRelOrders.FirstOrDefault(w => w.DriverID == currentUser.ID);
					if (relOrder == null) return (Result.GetError("未找到订单关联", 201));
					if (order.OrderState >= 2) return (Result.GetError("订单车主已确定。", 201));
					if (order.OrderState == -6) return (Result.GetError("订单已被货主取消。", 201));
					if (relOrder.OrderState == 2) return (Result.GetError("你已经被选为车主", 201));
					if (relOrder.OrderState != 0) return (Result.GetError("抢单失败", 201));
					relOrder.OrderState = 2;
					relOrder.OrderQuotation = orderQuotation;
					order.OrderState = 2;
					relOrder.AddDetail(2, currentUser.UserName, 2, currentUser.PhoneNumber, currentUser.Gender);
					a.SaveChanges();

					SqlParameter[] sqlParams = new SqlParameter[3];
					sqlParams[0] = new SqlParameter("@orderNumber", orderNumber);
					sqlParams[1] = new SqlParameter("@deriverId", currentUser.ID);
					sqlParams[2] = new SqlParameter("@phoneNumber", phoneNumber);
					a.Database.ExecuteSqlCommand("exec ProcessOrderDriver @orderNumber,@deriverId,@phoneNumber", sqlParams);

					//通知货主,车主选定.
					var customer = a.Users.FirstOrDefault(w => w.ID == order.GuestUserID);
					if (customer != null)
					{
						var message = string.Format(MvcApplication.M_ConfirmDriverTemplate,customer.UserName,customer.Gender.GetGenderStr(), order.PublishTime.Value.ToString("yyyy-MM-dd"), currentUser.UserName, currentUser.PhoneNumber);
						ValidCodeAPI.SendMessage(customer.PhoneNumber, message);
					}
					return Result.GetSuccess();
				}
			}
		}
		/// <summary>
		/// 完成订单
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="orderNumber"></param>
		/// <param name="orderQuotation"></param>
		/// <returns></returns>
		public ActionResult CompleteOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			using (var a = new SYSContext())
			{
				var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
				//未找到货主或订单
				if (order == null) return (Result.GetError("未找到订单", 201));

				var driver = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (driver == null) return (Result.GetError("未找到车主", 201));
				var driverRelOrders = order.DriverRelOrders.ToList();
				var rel = driverRelOrders.FirstOrDefault(z => z.DriverID == driver.ID);
				//货主和订单对不上
				if (rel == null) return (Result.GetError("未找到订单关联或订单状态错误", 201));
				rel.OrderState = 5;
				order.OrderState = 5;
				rel.AddDetail(5, driver.UserName, 2, driver.PhoneNumber, driver.Gender);

				a.SaveChanges();
				var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == order.GuestUserPhoneNumber);
				//通知货主,车主已经确认接货
				PushCustomHelper.EnsureComplete(order.GuestUserID, order.OrderNumber, custom);

				if (custom != null)
				{
					var message = string.Format(MvcApplication.M_SendSuccessTemplate,custom.UserName,custom.Gender.GetGenderStr(), order.PublishTime.Value.ToString("yyyy-MM-dd"));
					ValidCodeAPI.SendMessage(custom.PhoneNumber, message);
				}
				return Result.GetSuccess();
			}
		}
		///<summary>
		/// 不接订单
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult DiscardOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return (Result.GetError("未找到人员", 201));
					var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
					if (order == null) return (Result.GetError("未找到订单", 201));
					if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));

					var rel = order.DriverRelOrders.FirstOrDefault(z => z.DriverID == currentUser.ID);
					if (rel == null || rel.OrderState != 0) return (Result.GetError("未找到订单关联或订单状态错误", 201));
					rel.OrderState = -2;
					rel.AddDetail(-2, currentUser.UserName, 2, currentUser.PhoneNumber, currentUser.Gender);
					a.SaveChanges();
					return Result.GetSuccess();
				}
			}
		}

		///<summary>
		/// 获取他自己的订单
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return (Result.GetError("未找到人员", 201));

					var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
					if (order == null) return (Result.GetError("未找到订单", 201));

					var rel = order.DriverRelOrders.FirstOrDefault(z => z.DriverID == currentUser.ID);
					a.Configuration.ProxyCreationEnabled = false;
					a.Configuration.LazyLoadingEnabled = false;
					if (rel == null) return (Result.GetError("未找到订单关联", 201));
					DriverRelOrder relOrder = order.DriverRelOrders.FirstOrDefault(w => w.DriverID == currentUser.ID);
					if (relOrder == null) return (Result.GetError("未找到订单关联", 201));
					var other = order.DriverRelOrders.Where(w => w.DriverID != currentUser.ID).ToList();
					other.ForEach(w => order.DriverRelOrders.Remove(w));
					order.DriverRelOrders.ToList().ForEach(z => z.Driver = null);
					order.DriverRelOrders.ToList().ForEach(z => z.Order = null);
					//                    order.DriverRelOrders = null;
					order.PKey = null;
					return Result.GetSuccess(order);
				}
			}
		}
		///<summary>
		/// 获取订单列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		//        [allow]
		public ActionResult GetOrderByState(string phoneID, string phoneNumber, string orderStates, int skip = 0, int take = 10, string lastUpdateTime = "")
		{
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return Result.GetError("未找到人员", 201);

					var states = orderStates.Split(',').Select(w => w.ToInt()).ToList();
					if (states.Count == 0) return Result.GetError("订单状态错误", 201);
					IQueryable<DriverRelOrder> driverRelOrders = a.DriverRelOrders.Where(w => w.DriverID == currentUser.ID && states.Contains(w.OrderState));
					IQueryable<Order> queryable = null;
					if (!string.IsNullOrWhiteSpace(lastUpdateTime) && skip == 0)
					{
						DateTime dt = Convert.ToDateTime(lastUpdateTime);
						queryable = driverRelOrders.Select(z => z.Order).Where(z => z.PublishTime >= dt).Include(z => z.DriverRelOrders).OrderByDescending(z => z.PublishTime).Skip(skip).Take(take);
					}
					else queryable = driverRelOrders.Select(z => z.Order).Include(z => z.DriverRelOrders).OrderByDescending(z => z.PublishTime).Skip(skip).Take(take);
					var order = queryable.ToList();
					a.Configuration.ProxyCreationEnabled = false;
					a.Configuration.LazyLoadingEnabled = false;
					order.ForEach(w =>
					{
						w.PKey = null;
						w.DriverRelOrders = new Collection<DriverRelOrder>(w.DriverRelOrders.Where(z => z.DriverID == currentUser.ID).ToList());
						w.DriverRelOrders.ToList().ForEach(z => z.Driver = null);
						w.DriverRelOrders.ToList().ForEach(z => z.Order = null);
						w.GuestUser = a.Users.FirstOrDefault(z => z.ID == w.GuestUserID);
					});
					return Result.GetSuccess(order);
				}
			}
		}
		/// <summary>
		/// 根据状态和地址获取订单列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="orderStates"></param>
		/// <param name="address"></param>
		/// <returns></returns>
		public ActionResult GetOrderByStateAndAddress(string phoneID, string phoneNumber, string orderStates, string address, string lastUpdateTime = "")
		{
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return Result.GetError("未找到人员", 201);

					var states = orderStates.Split(',').Select(w => w.ToInt()).ToList();
					if (states.Count == 0) return Result.GetError("订单状态错误", 201);
					IQueryable<DriverRelOrder> driverRelOrders = a.DriverRelOrders.Where(w => w.DriverID == currentUser.ID && states.Contains(w.OrderState));
					IQueryable<Order> queryable = null;
					if (!string.IsNullOrWhiteSpace(lastUpdateTime))
					{
						try
						{
							DateTime dt = Convert.ToDateTime(lastUpdateTime);
							queryable = driverRelOrders.Select(z => z.Order).Where(z => z.PublishTime >= dt && (z.StartName.Equals(address) || z.EndName.Equals(address))).Include(z => z.DriverRelOrders).OrderByDescending(z => z.PublishTime);
						}
						catch
						{
							queryable = driverRelOrders.Select(z => z.Order).Where(z => z.StartName.Equals(address) || z.EndName.Equals(address)).Include(z => z.DriverRelOrders).OrderByDescending(z => z.PublishTime);
						}
					}
					else
					{
						queryable = driverRelOrders.Select(z => z.Order).Where(z => z.StartName.Equals(address) || z.EndName.Equals(address)).Include(z => z.DriverRelOrders).OrderByDescending(z => z.PublishTime);
					}
					var order = queryable.ToList();
					a.Configuration.ProxyCreationEnabled = false;
					a.Configuration.LazyLoadingEnabled = false;
					order.ForEach(w =>
					{
						w.PKey = null;
						w.DriverRelOrders = new Collection<DriverRelOrder>(w.DriverRelOrders.Where(z => z.DriverID == currentUser.ID).ToList());
						w.DriverRelOrders.ToList().ForEach(z => z.Driver = null);
						w.DriverRelOrders.ToList().ForEach(z => z.Order = null);
						w.GuestUser = a.Users.FirstOrDefault(z => z.ID == w.GuestUserID);
					});
					return Result.GetSuccess(order);
				}
			}
		}
		/// <summary>
		/// 根据状态和地址统计每个地址的订单个数
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="orderStates"></param>
		/// <returns></returns>
		public ActionResult GetOrderCountByStateAndAddress(string phoneID, string phoneNumber, string orderStates)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return Result.GetError("未找到人员", 201);

					var states = orderStates.Split(',').Select(w => w.ToInt()).ToList();
					if (states.Count == 0) return Result.GetError("订单状态错误", 201);

					var query = from dr in a.DriverRelOrders
								join o in a.Orders on dr.OrderID equals o.ID
								where dr.DriverID == currentUser.ID && states.Contains(dr.OrderState)
								group o by new { o.StartName, o.EndName, o.OrderState } into g
								select new
								{
									OrderState = g.Key.OrderState,
									StartName = g.Key.StartName,
									EndName = g.Key.EndName,
									Count = g.Count()
								};

					//var result = a.Database.SqlQuery<DirverOrderCountByAddress>("select COUNT(DriverRelOrders.ID) as Count,Orders.StartName,Orders.EndName from DriverRelOrders inner join Orders on DriverRelOrders.OrderID=Orders.ID where DriverRelOrders.DriverID=@DriverID AND CHARINDEX(','+CAST(DriverRelOrders.OrderState as VARCHAR)+',',','+@orderStates+',')>0  GROUP BY Orders.StartName,Orders.EndName", sqlParams).ToList();
					var result = query.ToList();
					var resultList = new Dictionary<string, DriverOrderCountByAddressResult>();
					var listResult = new List<DriverOrderCountResult>();
					if (result != null && result.Count > 0)
					{
						foreach (var entity in result)
						{
							if (resultList.ContainsKey(entity.StartName) && entity.OrderState == 2)
							{
								resultList[entity.StartName].StartCount += entity.Count;
							}
							if (resultList.ContainsKey(entity.EndName) && entity.OrderState == 3)
							{
								resultList[entity.EndName].EndCount = resultList[entity.EndName].EndCount + entity.Count;
							}
							if (!resultList.ContainsKey(entity.StartName))
							{
								if (entity.OrderState == 2)
								{
									DriverOrderCountByAddressResult model = new DriverOrderCountByAddressResult { StartCount = entity.Count, EndCount = 0 };
									resultList.Add(entity.StartName, model);
								}

							}
							if (!resultList.ContainsKey(entity.EndName))
							{
								if (entity.OrderState == 3)
								{
									DriverOrderCountByAddressResult model = new DriverOrderCountByAddressResult { StartCount = 0, EndCount = entity.Count };
									resultList.Add(entity.EndName, model);
								}

							}

						}
						foreach (string key in resultList.Keys)
						{
							DriverOrderCountResult countResult = new DriverOrderCountResult
							{
								Address = key,
								StartCount = resultList[key].StartCount,
								EndCount = resultList[key].EndCount
							};
							listResult.Add(countResult);

						}

					}
					return Result.GetSuccess(listResult);
				}
			}
		}

		///<summary>
		/// 获取订单列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		//        [allow]
		public ActionResult GetOrderCountByState(string phoneID, string phoneNumber, string orderStates)
		{
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return Result.GetError("未找到人员", 201);

					var states = orderStates.Split(',').Select(w => w.ToInt()).ToList();
					if (states.Count == 0) return Result.GetError("订单状态错误", 201);
					var driverRelOrders = a.DriverRelOrders.Count(w => w.DriverID == currentUser.ID && states.Contains(w.OrderState));

					return Result.GetSuccess(driverRelOrders);
				}
			}
		}
		public ActionResult CompleteOrderList(string phoneID, string phoneNumber, string address)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return Result.GetError("未找到人员", 201);
				var driver = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (driver == null) return (Result.GetError("未找到车主", 201));

				var orderIds = a.Orders.Where(x => x.EndName == address).Select(x => x.ID).ToList();
				var relOrderIds = a.DriverRelOrders.Where(z => z.DriverID == driver.ID && z.OrderState == 3 && orderIds.Contains(z.OrderID)).Select(x => x.OrderID).ToList();

				////通知货主,车主已经确认接货
				//PushCustomHelper.EnsureComplete(order.GuestUserID, order.OrderNumber, custom);
				SqlParameter[] sqlParams = new SqlParameter[2];
				sqlParams[0] = new SqlParameter("@DriverID", currentUser.ID);
				sqlParams[1] = new SqlParameter("@Address", address);
				var result = a.Database.ExecuteSqlCommand("EXEC BatchUpdateDriverOrder @DriverID,@Address", sqlParams);
				if (result == 0) return Result.GetError("批量完成订单失败");

				//通知货主,交易完成.
				var orders = a.Orders.Where(x => relOrderIds.Contains(x.ID)).Select(x => new { OrderNumber = x.OrderNumber, GustId = x.GuestUserID, PublishTime=x.PublishTime }).ToList();
				var guestUserIds = orders.Select(x => x.GustId).ToList();
				var customers = a.Users.Where(x => guestUserIds.Contains(x.ID));
				foreach (var or in orders)
				{
					var customer = customers.FirstOrDefault(w => w.ID == or.GustId);
					if (customer != null)
					{
						var message = string.Format(MvcApplication.M_SendSuccessTemplate,customer.UserName, customer.Gender.GetGenderStr(),or.PublishTime.Value.ToString("yyyy-MM-dd"));
						ValidCodeAPI.SendMessage(customer.PhoneNumber, message);
					}
				}

				return Result.GetSuccess();
			}
		}
		/// <summary>
		/// 收货人把密码告诉车主,车主输入密码,完成交易,
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public ActionResult EnsureComplete(string phoneID, string phoneNumber, string orderNumber, string pKey)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("pKey", pKey);
			using (var a = new SYSContext())
			{
				var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
				//未找到货主或订单
				if (order == null) return (Result.GetError("未找到订单", 201));

				var driver = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (driver == null) return (Result.GetError("未找到车主", 201));
				var driverRelOrders = order.DriverRelOrders.ToList();
				var rel = driverRelOrders.FirstOrDefault(z => z.DriverID == driver.ID);
				//货主和订单对不上
				if (rel == null || rel.OrderState != 3 && rel.OrderState != 4) return (Result.GetError("未找到订单关联或订单状态错误", 201));
				if (order.PKey != pKey) return (Result.GetError("验证码错误", 202));
				rel.OrderState = 5;
				order.OrderState = 5;
				rel.AddDetail(5, driver.UserName, 2, driver.PhoneNumber, driver.Gender);

				a.SaveChanges();
				var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == order.GuestUserPhoneNumber);
				//通知货主,车主已经确认接货
				//PushCustomHelper.EnsureComplete(order.GuestUserID, order.OrderNumber, custom);
				//通知货主,交易完成.
				var customer = a.Users.FirstOrDefault(w => w.ID == order.GuestUserID);
				if (customer != null)
				{
					var message = string.Format(MvcApplication.M_SendSuccessTemplate, customer.UserName, customer.Gender.GetGenderStr(), order.PublishTime.Value.ToString("yyyy-MM-dd"));
					ValidCodeAPI.SendMessage(customer.PhoneNumber, message);
				}
				return Result.GetSuccess();
			}
		}

		/// <summary>
		/// 司机上路
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <param name="orderNumber"></param>
		/// <returns></returns>
		public ActionResult StartSendGoods(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			using (var a = new SYSContext())
			{
				var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
				var driver = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				//未找到货主或订单
				if (order == null) return Result.GetError("未找到订单", 201);
				if (driver == null) return Result.GetError("未找到人员", 201);
				var driverRelOrders = order.DriverRelOrders.ToList();
				var rel = driverRelOrders.FirstOrDefault(z => z.DriverID == driver.ID);
				//货主和订单对不上
				if (rel == null) return Result.GetError("未找到订单关联信息", 201);
				if (rel.OrderState != 3) return Result.GetError("订单状态错误", 201);
				rel.OrderState = 4;
				order.OrderState = 4;
				rel.AddDetail(4, driver.UserName, 2, driver.PhoneNumber, driver.Gender);

				a.SaveChanges();
				var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == order.GuestUserPhoneNumber);

				//通知货主,车主已经确认接货
				PushCustomHelper.DriverRecived(order.GuestUserID, order.OrderNumber, custom);
				return Result.GetSuccess();
			}
		}
		/// <summary>
		/// 车主确认收货,
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns></returns>
		public ActionResult EnsureRevicedGoods(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			using (var a = new SYSContext())
			{
				var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
				//未找到货主或订单
				if (order == null) return (Result.GetError("未找到订单", 201));

				var driver = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
				if (driver == null) return (Result.GetError("未找到人员", 201));

				var driverRelOrders = order.DriverRelOrders.ToList();
				var rel = driverRelOrders.FirstOrDefault(z => z.DriverID == driver.ID);
				//货主和订单对不上
				if (rel == null) return (Result.GetError("未找到订单关联信息", 201));
				if (rel.OrderState != 2 && rel.OrderState != 7) return (Result.GetError("订单状态错误", 201));
				rel.OrderState = 3;
				order.OrderState = 3;
				rel.AddDetail(3, driver.UserName, 2, driver.PhoneNumber, driver.Gender);

				a.SaveChanges();
				var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == order.GuestUserPhoneNumber);

				//通知货主,车主已经确认接货
				//发短信
				var str = MvcApplication.M_Template1;
				var customer = a.Users.FirstOrDefault(z => z.ID == order.GuestUserID);
				var userName = customer.UserName;
				if (customer.Gender == 1) userName += "先生";
				else userName += "女士";
				str = str.Replace("{CustomerUserName}", userName);
				str = str.Replace("{CustomerPhoneNumber}", customer.PhoneNumber);
				str = str.Replace("{StartName}", order.StartName);
				str = str.Replace("{GoodsName}", order.Lading);
				str = str.Replace("{PKey}", order.PKey);
				if (order.DeliverTimeEnd != null) str = str.Replace("{DeliverTimeEnd}", order.DeliverTimeEnd.Value.ToString("MM月dd号 hh点mm分"));
				ValidCodeAPI.SendMessage(order.ReciveUserPhoneNumber, str);

				PushCustomHelper.DriverRecived(order.GuestUserID, order.OrderNumber, custom);
				return Result.GetSuccess();
			}
		}
		///<summary>
		/// 取消
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult CancelOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			Para.Required("orderNumber", orderNumber);
			lock (orderObject)
			{
				using (var a = new SYSContext())
				{
					var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
					if (currentUser == null) return (Result.GetError("未找到人员", 201));

					var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
					if (order == null) return (Result.GetError("未找到订单", 201));
					var rel = order.DriverRelOrders.FirstOrDefault(z => z.DriverID == currentUser.ID);
					if (rel == null ||
						rel.OrderState != 0 &&
						rel.OrderState != 1 &&
						rel.OrderState != 2) return (Result.GetError("订单状态错误", 201));

					//                    DriverRelOrder relOrder = order.DriverRelOrders.FirstOrDefault(w=>w.DriverID == currentUser.ID);
					//                    if (relOrder == null) return (Result.GetError("非法操作", 201));
					if (rel.OrderState == 0) rel.OrderState = -2; //认为是不接受
					if (rel.OrderState == 1)
					{
						//已接受后取消
						rel.OrderState = -4; //取消
						rel.AddDetail(-4, currentUser.UserName, 2, currentUser.PhoneNumber, currentUser.Gender);
						var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == order.GuestUserPhoneNumber);

						PushCustomHelper.OrderCancel(order.GuestUserID, order.OrderNumber, rel.DriverID, custom);
					}

					if (rel.OrderState == 2)
					{
						rel.OrderState = -5; //取消
						order.OrderState = -5;
						rel.AddDetail(-5, currentUser.UserName, 2, currentUser.PhoneNumber, currentUser.Gender);
						rel.EnsureState = -2;
						var custom = a.Users.FirstOrDefault(z => z.PhoneNumber == order.GuestUserPhoneNumber);

						//TODO 车主在被货主选中后取消,那么后面的流程该怎么走
						PushCustomHelper.OrderCancel(order.GuestUserID, order.OrderNumber, rel.DriverID, custom);
					}
					a.SaveChanges();
					return Result.GetSuccess();
				}
			}
		}
		//        ///<summary>
		//        /// 获取错过的订单
		//        /// </summary>
		//        /// <param name="phoneID"></param>
		//        /// <param name="phoneNumber"></param>
		//        /// <returns>bool</returns>
		//        public ActionResult GetLostOrders(string phoneID, string phoneNumber,int skip = 0,int length = 10)
		//        {
		//            lock (orderObject)
		//            {
		//                using (var a = new SYSContext())
		//                {
		////                    var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
		////                    if (currentUser == null) return (Result.GetError("非法操作", 201));
		////w.DriverID == currentUser.ID
		//                    IQueryable<Order> queryable = a.DriverRelOrders.Where(w =>   w.OrderState == -1).OrderByDescending(w=>w.Order.PublishTime).Select(w=>w.Order).Skip(skip).Take(length);
		////                    var werw = queryable.ToString();
		//                    var orders = queryable.ToList();
		//
		//                    return Result.GetSuccess(orders);
		//                }
		//            }
		//        } 

		/// <summary>
		/// 获取司机信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetDriver(string phoneID, string phoneNumber)
		{
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.Include(z => z.DriverInfo).FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));
				Driver driverInfo = currentUser.DriverInfo;
				a.Configuration.ProxyCreationEnabled = false;
				a.Configuration.LazyLoadingEnabled = false;
				driverInfo.User = null;
				return Result.GetResult(true, null, driverInfo.ToJson());
			}
		}

		/// <summary>
		/// 返回车主的统计信息
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetDriverGather(string phoneID, string phoneNumber)
		{
			Para.Required("phoneID", phoneID);
			Para.Required("phoneNumber", phoneNumber);
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));
				a.Configuration.ProxyCreationEnabled = false;
				a.Configuration.LazyLoadingEnabled = false;
				SqlParameter[] sqlParams = new SqlParameter[1];
				sqlParams[0] = new SqlParameter("@DriverID", currentUser.ID);
				var result = a.Database.SqlQuery<DriverGatherData>("select DriverID," +
																"COUNT(*) as BargainTime," +
																"SUM(Price) as BargainMoney," +
																"case " +
																"when SUM(ISNULL(GuestAppraised,0))=0 then 0 " +
																"else  sum(PingJia1Score) / SUM( ISNULL(GuestAppraised,0)) " +
																"end  as Grade " +
																"From Drivers left join Users on Drivers.ID = Users.id " +
																"inner join DriverRelOrders on Drivers.ID = DriverRelOrders.DriverID " +
																"left join Orders on Orders.ID = DriverRelOrders.OrderID " +
																"where DriverRelOrders.OrderState >= 5 AND DriverRelOrders.DriverID=@DriverID " +
																"group by DriverID", sqlParams).FirstOrDefault();
				// var item = a.DriverGathers.FirstOrDefault(w => w.DriverID == currentUser.ID);
				return Result.GetSuccess(result);
			}
		}
		/// <summary>
		/// 评价货主
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult AppraiseOrder(string phoneID, string phoneNumber, string orderNumber, double? score, string content)
		{
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));

				var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
				if (order == null) return (Result.GetError("未找到订单", 201));

				DriverRelOrder relOrder = order.DriverRelOrders.FirstOrDefault(w => w.DriverID == currentUser.ID);
				if (relOrder == null) return (Result.GetError("未找到订单关联信息", 201));
				order.PingJia2Content = content;
				order.PingJia2Score = score ?? 3;
				relOrder.AddDetail(6, currentUser.UserName, 2, currentUser.PhoneNumber, currentUser.Gender);
				order.DriverAppraised = 1;
				a.SaveChanges();
				return Result.GetSuccess();
			}
		}
		/// <summary>
		/// 获取一个订单货主对我的评价
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetAppraiseOrder(string phoneID, string phoneNumber, string orderNumber)
		{
			using (var a = new SYSContext())
			{
				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));

				var order = a.Orders.FirstOrDefault(w => w.OrderNumber == orderNumber);
				if (order == null) return (Result.GetError("未找到订单", 201));

				DriverRelOrder relOrder = order.DriverRelOrders.FirstOrDefault(w => w.DriverID == currentUser.ID);
				if (relOrder == null) return (Result.GetError("未找到订单关联信息", 201));
				a.SaveChanges();
				a.Configuration.ProxyCreationEnabled = false;
				a.Configuration.LazyLoadingEnabled = false;
				order.DriverRelOrders = null;
				order.PKey = null;
				return Result.GetSuccess(order);
			}
		}

		/// <summary>
		/// 获取订单货主对我的评价列表
		/// </summary>
		/// <param name="phoneID"></param>
		/// <param name="phoneNumber"></param>
		/// <returns>bool</returns>
		public ActionResult GetAppraiseOrders(string phoneID, string phoneNumber, int skip = 0, int take = 10, string lastUpdateTime = "")
		{
			using (var a = new SYSContext(true, false))
			{

				var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
				if (currentUser == null) return (Result.GetError("未找到人员", 201));
				var ids = a.DriverRelOrders.Where(z => z.DriverID == currentUser.ID && z.EnsureState == 1).Select(z => z.OrderID).ToList();
				IQueryable<Order> queryable = null;
				if (!string.IsNullOrWhiteSpace(lastUpdateTime) && skip == 0)
				{
					DateTime dt = Convert.ToDateTime(lastUpdateTime);
					queryable = a.Orders.Where(z => ids.Contains(z.ID) && z.PublishTime > dt && z.GuestAppraised == 1).OrderByDescending(z => z.PublishTime).Skip(skip).Take(take);
				}
				else queryable = a.Orders.Where(z => ids.Contains(z.ID) && z.GuestAppraised == 1).OrderByDescending(z => z.PublishTime).Skip(skip).Take(take);
				var list = queryable.ToList();
				list.ForEach(z =>
				{
					z.PKey = null;
					z.GuestUser = a.Users.FirstOrDefault(w => w.ID == z.GuestUserID);
				}
					);
				//                var list =
				//                    (from order in a.Orders
				//                    from rel in order.DriverRelOrders
				//                    where rel.DriverID == currentUser.ID && order.PingJia1Score >= 0
				//                    select order).OrderByDescending(w=>w.PublishTime);

				return Result.GetSuccess(list.ToList());
			}
		}



	}
	/// <summary>
	/// 按地名查询结果类
	/// </summary>
	public class DirverOrderCountByAddress
	{
		public int Count { set; get; }
		public string StartName { set; get; }
		public string EndName { set; get; }
	}
	/// <summary>
	/// 按地名查询封装类
	/// </summary>
	public class DriverOrderCountByAddressResult
	{
		public int StartCount { set; get; }
		public int EndCount { set; get; }
	}
	public class DriverOrderCountResult
	{
		public string Address { set; get; }
		public int StartCount { set; get; }
		public int EndCount { set; get; }
	}
	public class DriverGatherData
	{
		public Guid DriverID { set; get; }
		public int BargainTime { set; get; }
		public double BargainMoney { set; get; }
		public double Grade { set; get; }
	}
}