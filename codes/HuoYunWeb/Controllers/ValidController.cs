﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using EntityFramework.Extensions;
using Fw.Entity;
using HuoYunBLL;
using HuoYunWeb.App_Code;
using HuoYunWeb.App_Code.Push;
using HuoYunWeb.Models;

namespace HuoYunWeb.Controllers
{
    [WebAuthorize]
    public class ValidController : UserBaseController
    {
        //
        // GET: /Valid/

        public ActionResult Drivers(
            string userName,
            string phoneNumber,
            int pageNum = 1,
                int numPerPage = 20,
                string orderField = "",
                int validSubmit = 1,
                int valid = -1,
                string status = "")
        {
            return SYSContext.Run(w =>
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                IQueryable<Driver> queryable = w.Drivers.Include(z => z.User).Include(z=>z.Orders);
//                    .Where(z => z.ValidSubmit == validSubmit);
                if (valid != -1) queryable = queryable.Where(z => z.Valid == valid);

                if (userName != null) queryable = queryable.Where(z => z.User.UserName.Contains(userName));
                if (!string.IsNullOrWhiteSpace(phoneNumber)) queryable = queryable.Where(z => z.User.PhoneNumber.Contains(phoneNumber));
                var list = queryable
                    .OrderByDescending(z => z.User.JoinDate)
                    .Skip((pageNum - 1) * numPerPage)
                    .Take(numPerPage)
                    .ToList();
                var total = w.Drivers.Count();//(z => z.ValidSubmit == 1);
                var re = new ListPageModel<Driver>();
                re.List = list;
                re.Page = new PageInfo
                {
                    TotalRecord = total,
                    PageIndex = pageNum,
                    PageSize = numPerPage,
                    TotalPage = (total + numPerPage - 1) / numPerPage
                };
                return View(re);
            });
        }
        /// <summary>
        /// 选择多个车主
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="pageNum"></param>
        /// <param name="numPerPage"></param>
        /// <param name="orderField"></param>
        /// <param name="validSubmit"></param>
        /// <param name="valid"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public ActionResult SelectMultiDrivers(
            string userName,
            string phoneNumber,
            int pageNum = 1,
                int numPerPage = 20,
                string orderField = "",
                int validSubmit = 1,
                int valid = -1,
                string status = "")
        {
            return SYSContext.Run(w =>
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                IQueryable<Driver> queryable = w.Drivers.Include(z => z.User).Include(z => z.Orders);
                //                    .Where(z => z.ValidSubmit == validSubmit);
                if (valid != -1) queryable = queryable.Where(z => z.Valid == valid);

                if (userName != null) queryable = queryable.Where(z => z.User.UserName.Contains(userName));
                if (!string.IsNullOrWhiteSpace(phoneNumber)) queryable = queryable.Where(z => z.User.PhoneNumber.Contains(phoneNumber));
                var list = queryable
                    .OrderByDescending(z => z.User.JoinDate)
                    .Skip((pageNum - 1) * numPerPage)
                    .Take(numPerPage)
                    .ToList();
                var total = w.Drivers.Count();//(z => z.ValidSubmit == 1);
                var re = new ListPageModel<Driver>();
                re.List = list;
                re.Page = new PageInfo
                {
                    TotalRecord = total,
                    PageIndex = pageNum,
                    PageSize = numPerPage,
                    TotalPage = (total + numPerPage - 1) / numPerPage
                };
                return View(re);
            });
        }
        public ActionResult SelectDrivers(
            string userName,
            int pageNum = 1,
                int numPerPage = 20,
                string orderField = "",
                int validSubmit = 1,
                int valid = -1,
                string status = "")
        {
            return SYSContext.Run(w =>
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                IQueryable<Driver> queryable = w.Drivers.Include(z => z.User).Include(z => z.Orders);
                //                    .Where(z => z.ValidSubmit == validSubmit);
                if (valid != -1) queryable = queryable.Where(z => z.Valid == valid);

                if (userName != null) queryable = queryable.Where(z => z.User.UserName.Contains(userName));
                var list = queryable
                    .OrderByDescending(z => z.User.JoinDate)
                    .Skip((pageNum - 1) * numPerPage)
                    .Take(numPerPage)
                    .ToList();
                var total = w.Drivers.Count();//(z => z.ValidSubmit == 1);
                var re = new ListPageModel<Driver>();
                re.List = list;
                re.Page = new PageInfo
                {
                    TotalRecord = total,
                    PageIndex = pageNum,
                    PageSize = numPerPage,
                    TotalPage = (total + numPerPage - 1) / numPerPage
                };
                return View(re);
            });
        }
        public ActionResult Customers(
            string userName,
            string phoneNumber,
            int pageNum = 1,
                int numPerPage = 20,
                string orderField = "",
                int validSubmit = 1,
                int valid = -1,
                string status = "")
        {
            return SYSContext.Run(w =>
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                var queryable = w.Customers.Include(z => z.UserInfo);
//                    .Where(z => z.ValidSubmit == validSubmit);
                if (valid != -1) queryable = queryable.Where(z => z.Valid == valid);

                if (userName != null) queryable = queryable.Where(z => z.UserInfo.UserName.Contains(userName));
                if (!string.IsNullOrWhiteSpace(phoneNumber)) queryable = queryable.Where(z => z.UserInfo.PhoneNumber.Contains(phoneNumber));
                var list = queryable
                    .OrderByDescending(z => z.UserInfo.JoinDate)
                    .Skip((pageNum - 1) * numPerPage)
                    .Take(numPerPage)
                    .ToList();
                var total = w.Customers.Count();//(z => z.ValidSubmit == 1);
                var re = new ListPageModel<Customer>();
                re.List = list;
                re.Page = new PageInfo { 
                    TotalRecord = total,
                    PageIndex = pageNum,
                    PageSize = numPerPage, 
                    TotalPage = (total + numPerPage - 1) / numPerPage 
                };
                return View(re);
            });
        }
        public ActionResult Delete(
            Guid? id)
        {
            return SYSContext.Run(w =>
            {
                var rels = w.DriverRelOrders.Where(z => z.ID == id || z.DriverID == id).ToList();
                if (rels.Any())
                {
                    var ids = rels.Select(z => z.ID).ToList();
                    w.DriverRelOrderDetails.Delete(z => ids.Contains(z.ID)|| ids.Contains(z.DriverRelOrderID));
                }
                w.Customers.Delete(z => z.ID == id || z.UserID == id);
                w.DriverRelOrders.Delete(z => z.ID == id || z.DriverID == id);
                w.DriverGathers.Delete(z => z.ID == id || z.DriverID == id);
                w.DriverLines.Delete(z => z.ID == id || z.DriverID == id);
                w.Drivers.Delete(z => z.ID == id);
                w.Orders.Delete(z => z.GuestUserID == id);
                w.Users.Delete(z => z.ID == id);
                w.SaveChanges();
                return getDefaultJsonResult();
            });
        }
        
        /// <summary>
        /// 查看详细信息
        /// </summary>
        /// <param name="driverID"></param>
        /// <returns></returns>
        public ActionResult DriverDetail(Guid driverID)
        {
            return SYSContext.Run(w =>
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                var item  = w.Drivers.Include(z=>z.User).FirstOrDefault(z => z.ID == driverID);
                return View(item);
            });
        }

        [HttpPost]
        public ActionResult DriverDetail(Guid driverID, bool isvalid, string Remark, string UserName, string PhoneNumber)
        {
            using (var a = new SYSContext())
            {
                a.Configuration.LazyLoadingEnabled = false;
                a.Configuration.ProxyCreationEnabled = false;
                var item = a.Drivers.Include(z => z.User).FirstOrDefault(z => z.ID == driverID);
                if (item != null)
                {
                    if (item.Valid == 0 && isvalid)
                    {
                        PushDriverHelper.ValidSuccess(item.ID,2);
                    }
                    item.Valid = isvalid ? 1 : 0;
                    item.Remark = Remark;
                    item.User.PhoneNumber = PhoneNumber;
                    item.User.UserName = UserName;

                }
                a.SaveChanges();
                return getDefaultJsonResult("Drivers");
            }
        }
        /// <summary>
        /// 查看详细信息
        /// </summary>
        /// <param name="customID"></param>
        /// <returns></returns>
        public ActionResult CustomDetail(Guid customID)
        {
            return SYSContext.Run(w =>
            {
                w.Configuration.LazyLoadingEnabled = false;
                w.Configuration.ProxyCreationEnabled = false;
                var item = w.Customers.Include(z => z.UserInfo).FirstOrDefault(z => z.ID == customID);
                return View(item);
            });
        }

        [HttpPost]
        public ActionResult CustomDetail(Guid? customID, bool isvalid, string Remark, string UserName, string PhoneNumber)
        {
            using (var a = new SYSContext())
            {
                a.Configuration.LazyLoadingEnabled = false;
                a.Configuration.ProxyCreationEnabled = false;
                var item = a.Customers.Include(z => z.UserInfo).FirstOrDefault(z => z.ID == customID);
                if (item != null)
                {
                    if (item.Valid == 0 && isvalid)
                    {
                        PushDriverHelper.ValidSuccess(item.ID,1);
                    }
                    item.Valid = isvalid ? 1 : 0;
                    item.Remark = Remark;
                    item.UserInfo.PhoneNumber = PhoneNumber;
                    item.UserInfo.UserName = UserName;

                }
                a.SaveChanges();
                return getDefaultJsonResult("Customers");
            }
        }
    }
}
