﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Fw.Extends;
using HuoYunWeb.SMSAPI;
using m=HuoYunWeb.MvcApplication;
namespace HuoYunWeb
{
    /// <summary>
    /// 发送短信
    /// </summary>
    public class ValidCodeAPI
    {
        private static string LN = "\r\n";
        public static bool Register()
        {
            var re = false;
            using (var client = new WebServiceSoapClient())
            {
                string log = "开始注册SMS" + LN;
                try
                {
                    var str = client.Register(m.M_SN, m.M_PWD, m.M_Province, m.M_City, m.M_Trade, m.M_Entname,
                        m.M_Linkman,
                        m.M_Phone,
                        m.M_Mobile, m.M_Email, m.M_Fax, m.M_Address, m.M_Postcode, m.M_Sign);
                    //-1 操作失败//说明已经注册成功一次
                    if (str.StartsWith("-1") || str.StartsWith("0")) re = true;
                    log += "结果:" + str + LN;
                }
                catch (Exception e)
                {
                    log += "结果:(失败)" + e.Message + LN;
                }
                LogHelper.WriteLog(typeof(ValidCodeAPI), log);
            }
            return re;
        }
        public static bool UnRegister()
        {
            bool re = false;
            using (var client = new WebServiceSoapClient())
            {
                string log = "开始取消注册SMS" + LN;
                try
                {
                    var str = client.UnRegister(m.M_SN, m.M_PWD);
                    //如果返回以下结果,说明多次取消注册
                    //-5 序列号密码不正确
                    //返回以下结果,说明操作成功
                    //0 成功
                    if (str.StartsWith("-5") || str.StartsWith("0")) re = true;
                    log += "结果:" + str + LN;
                }
                catch (Exception e)
                {
                    log += "结果:(失败)" + e.Message + LN;
                }
                LogHelper.WriteLog(typeof(ValidCodeAPI), log);
            }
            return re;
        }

        /// <summary>
        /// 查询余额
        /// </summary>
        public static double? Balance()
        {
            using (var client = new WebServiceSoapClient())
            {
                string log = "查询余额SMS" + LN;
                try
                {
                    var mPwd = (m.M_SN + m.M_PWD).MD5();
                    var str = client.balance(m.M_SN, mPwd).ToDoublt();
                    log += "结果:" + str + LN;
                    return str;
                }
                catch (Exception e)
                {
                    log += "结果:(失败)" + e.Message + LN;
                }
                LogHelper.WriteLog(typeof(ValidCodeAPI), log);
            }
            return null;
        }
        /// <summary>
        /// 发送API
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="code"></param>
        public static bool SendCode(string phoneNumber,string code)
        {
            string log = string.Format("SMS开始发送短信[{0}],[{1}]" + LN, phoneNumber, code);
            using (var client = new WebServiceSoapClient())
            {
                try
                {
//                    string str = code;
                    string str = string.Format(m.M_Template, code);
                    var mPwd = (m.M_SN+m.M_PWD).MD5();
                    //返回以下结果,说明错误有可能是: 1.序列号未注册2.密码加密不正确3.密码已被修改4.序列号已注销
                    //-2
                    var re = client.mdSmsSend(m.M_SN, mPwd, phoneNumber, str, "", "", "");
                    if (re.StartsWith("-2"))
                    {
                        if (Register())
                        {
                           re = client.mdSmsSend(m.M_SN, mPwd, phoneNumber, str, "", "", "");
                        }
                    }
                    log += re + LN;
                }
                catch (Exception eee)
                {
                    log += "错误:" + eee.Message;
                }
            }
            LogHelper.WriteLog(typeof(ValidCodeManage),log);
            return true;
        }
        /// <summary>
        /// 发送API
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="message"></param>
        public static bool SendMessage(string phoneNumber,string message)
        {
            string log = message;
            using (var client = new WebServiceSoapClient())
            {
                try
                {
					string str = message;
					var mPwd = (m.M_SN + m.M_PWD).MD5();
					//返回以下结果,说明错误有可能是: 1.序列号未注册2.密码加密不正确3.密码已被修改4.序列号已注销
					//-2
					var re = client.mdSmsSend(m.M_SN, mPwd, phoneNumber, str, "", "", "");
					if (re.StartsWith("-2"))
					{
						if (Register())
						{
							re = client.mdSmsSend(m.M_SN, mPwd, phoneNumber, str, "", "", "");
						}
					}
					log += re + LN;
                }
                catch (Exception eee)
                {
                    log += "错误:" + eee.Message;
                }
            }
            LogHelper.WriteLog(typeof(ValidCodeManage),log);
            return true;
        }
    }
    public class ValidCodeManage
    {
        static Random r = new Random();
        /// <summary>
        /// 生成新的手机验证码
        /// </summary>
        /// <returns></returns>
        public static string NewCode()
        {
            
            return r.Next(100000, 999999).ToString();
        }
        public static Dictionary<string, string> ValidCodes = new Dictionary<string, string>();

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="code"></param>
        public static void SetCode(string phoneID,string code)
        {
            lock (ValidCodes)
            {
                ValidCodes[phoneID] = code;
            }
        }
        public static string GetCode(string phoneID)
        {
            lock (ValidCodes)
            {
                if (ValidCodes.ContainsKey(phoneID))return ValidCodes[phoneID];
                return null;
            }
        }
    }
}