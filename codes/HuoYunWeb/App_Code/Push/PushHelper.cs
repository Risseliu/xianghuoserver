﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityFramework.Caching;
using Fw.Extends;
using HuoYunWeb.Models;

namespace HuoYunWeb.App_Code.Push
{
    public class PushCustomHelper
    {
        /// <summary>
        /// 有新的车主接单时,通知货主
        /// </summary>
        public static void SendNewDriverAccept(Guid customUserID, string orderNumber, Guid driverID, User driver)
        {
            using (var a = new SYSContext())
            {
                var pm = new PullMessage();
                pm.type = PullMessage.Custom_NewDriverAccept;
                pm.data = orderNumber;
                pm.content = driverID.ToString();
                pm.phoneNumber = driver.PhoneNumber;
                pm.gender = driver.Gender;
                pm.userName = driver.UserName;
                pm.userID = driver.ID;
                var content = pm.ToJson();
//                userID.ForEach(w =>
//                {
                    a.BPushs.Add(new BPush
                    {
                        ID = Guid.NewGuid(),
                        CreateTime = DateTime.Now,
                        MessageContent = content,
                        UserID = customUserID,
                    });
//                });
                a.SaveChanges();
            }
        }
        /// <summary>
        /// 通知货主,车主已经确认接货
        /// </summary>
        public static void DriverRecived(Guid customUserID, string orderNumber, User user)
        {
            using (var a = new SYSContext())
            {
                var pm = new PullMessage();
                pm.type = PullMessage.Custom_DriverRecived;
                pm.data = orderNumber;
                pm.phoneNumber = user.PhoneNumber;
                pm.userName = user.UserName;
                pm.gender = user.Gender;
                pm.userID = user.ID;
                var content = pm.ToJson();
                a.BPushs.Add(new BPush
                {
                    ID = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    MessageContent = content,
                    UserID = customUserID,
                });
                a.SaveChanges();
            }
        }  
        /// <summary>
        /// 通知货主,有车主取消了订单
        /// </summary>
        public static void OrderCancel(Guid customUserID, string orderNumber, Guid driverUserID, User phoneNumber)
        {
            using (var a = new SYSContext())
            {
                var pm = new PullMessage();
                pm.type = PullMessage.Custom_OrderCancel;
                pm.data = orderNumber;
                pm.title = driverUserID.ToString();
                pm.phoneNumber = phoneNumber.PhoneNumber;
                pm.userName = phoneNumber.UserName;
                pm.gender = phoneNumber.Gender;
                pm.userID = phoneNumber.ID;
                var content = pm.ToJson();
                a.BPushs.Add(new BPush
                {
                    ID = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    MessageContent = content,
                    UserID = customUserID,
                });
                a.SaveChanges();
            }
        } 
        /// <summary>
        /// 通知货主,收货人已经确认接货
        /// </summary>
        public static void EnsureComplete(Guid customUserID, string orderNumber, User phoneNumber)
        {
            using (var a = new SYSContext())
            {
                try
                {
                    var pm = new PullMessage();
                    pm.type = PullMessage.Custom_EnsureComplete;
                    pm.data = orderNumber;
                    pm.userName = phoneNumber.UserName;
                    pm.gender = phoneNumber.Gender;
                    pm.userID = phoneNumber.ID;
                    pm.phoneNumber = phoneNumber.PhoneNumber;
                    var content = pm.ToJson();
                    a.BPushs.Add(new BPush
                    {
                        ID = Guid.NewGuid(),
                        CreateTime = DateTime.Now,
                        MessageContent = content,
                        UserID = customUserID,
                    });
                }
                catch(Exception eee)
                {
                    LogHelper.WriteLog(null,eee);
                }
                a.SaveChanges();
            }
        }
    }

    public class NewOrderInfo
    {
        
    }
    public class PushDriverHelper
    {
        /// <summary>
        /// 通知车主,有货主取消了订单
        /// </summary>
        public static void OrderCancel(Guid driverUserID, string orderNumber, User phoneNumber)
        {
            using (var a = new SYSContext())
            {
                var pm = new PullMessage();
                pm.type = PullMessage.Driver_OrderCancel;
                pm.data = orderNumber;
                pm.phoneNumber = phoneNumber.PhoneNumber;
                pm.userName = phoneNumber.UserName;
                pm.gender = phoneNumber.Gender;
                pm.userID = phoneNumber.ID;
                var content = pm.ToJson();
                a.BPushs.Add(new BPush
                {
                    ID = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    MessageContent = content,
                    UserID = driverUserID,
                });
                a.SaveChanges();
            }
        }

        /// <summary>
        /// 发送新订单
        /// </summary>
        /// <param name="userID"></param>
        public static void SendAddPrice(List<Guid> userID, string orderNumber, double price, User phoneNumber)
        {
            using (var a = new SYSContext())
            {
                PullMessage pm = new PullMessage();
                pm.type = PullMessage.Driver_AddPrice;
                pm.data = orderNumber;
                pm.content = price.ToString();
                pm.phoneNumber = phoneNumber.PhoneNumber;
                pm.userName = phoneNumber.UserName;
                pm.gender = phoneNumber.Gender;
                pm.userID = phoneNumber.ID;
                var content = pm.ToJson();
                userID.ForEach(w =>
                {

                    a.BPushs.Add(new BPush
                    {
                        ID = Guid.NewGuid(),
                        CreateTime = DateTime.Now,
                        MessageContent = content,
                        UserID = w,
                    });
                });
                a.SaveChanges();
            }
        }

        /// <summary>
        /// 发送新订单
        /// </summary>
        /// <param name="userID"></param>
        public static void SendNewOrder(List<Guid> userID, string orderNumber, User phoneNumber)
        {
            using (var a = new SYSContext())
            {
                PullMessage pm = new PullMessage();
                pm.type = PullMessage.Driver_NewOrder;
                pm.data = orderNumber;
                pm.phoneNumber = phoneNumber.PhoneNumber;
                pm.userName = phoneNumber.UserName;
                pm.gender = phoneNumber.Gender;
                pm.userID = phoneNumber.ID;
                var content = pm.ToJson();
                userID.ForEach(w =>
                {
                    
                    a.BPushs.Add(new BPush
                    {
                        ID = Guid.NewGuid(),
                        CreateTime = DateTime.Now,
                        MessageContent = content,
                        UserID = w,
                    });
                });
                a.SaveChanges();
            }
        } 
        /// <summary>
        /// 通知车主,他已被确定为发货人
        /// </summary>
        /// <param name="userID"></param>
        public static void EnsureOrderDriver(Guid driverUserID, string orderNumber, User phoneNumber)
        {
            using (var a = new SYSContext())
            {
                PullMessage pm = new PullMessage();
                pm.type = PullMessage.Driver_EnsureOrderDriver;
                pm.data = orderNumber;
                pm.phoneNumber = phoneNumber.PhoneNumber;
                pm.userName = phoneNumber.UserName;
                pm.gender = phoneNumber.Gender;
                pm.userID = phoneNumber.ID;
                var content = pm.ToJson();
                a.BPushs.Add(new BPush
                {
                    ID = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    MessageContent = content,
                    UserID = driverUserID,
                });
                a.SaveChanges();
            }
        }
        /// <summary>
        /// 通知车主,货主已经把货主给他了
        /// </summary>
        /// <param name="userID"></param>
        public static void EnsureSendGoods(Guid driverUserID, string orderNumber, User phoneNumber)
        {
            using (var a = new SYSContext())
            {
                PullMessage pm = new PullMessage();
                pm.type = PullMessage.Driver_SendGoods;
                pm.data = orderNumber;
                pm.userName = phoneNumber.UserName;

                pm.phoneNumber = phoneNumber.PhoneNumber;
                pm.gender = phoneNumber.Gender;
                pm.userID = phoneNumber.ID;
                var content = pm.ToJson();
                a.BPushs.Add(new BPush
                {
                    ID = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    MessageContent = content,
                    UserID = driverUserID,

                });
                a.SaveChanges();
            }
        }

        /// <summary>
        /// 通知车主,已经通过审核
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="userType">1开通货主,2开通车主</param>
        public static void ValidSuccess(Guid driverUserID,int userType)
        {
            using (var a = new SYSContext())
            {
                PullMessage pm = new PullMessage();
                pm.type = PullMessage.Driver_ValidSuccess;
                pm.data = userType.ToString();
                var content = pm.ToJson();
                a.BPushs.Add(new BPush
                {
                    ID = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    MessageContent = content,
                    UserID = driverUserID,
                });
                a.SaveChanges();
            }
        }
        /// <summary>
        /// 通知车主,他未被确定为发货人
        /// </summary>
        /// <param name="userID"></param>
        public static void EnsureOrderDriverFail(Guid driverUserID, string orderNumber, string winDriverUserName, User user)
        {
            using (var a = new SYSContext())
            {
                PullMessage pm = new PullMessage();
                pm.type = PullMessage.Driver_EnsureOrderDriverFail;
                pm.data = orderNumber.ToString();
                pm.content = winDriverUserName;
                pm.phoneNumber = user.PhoneNumber;
                pm.userName = user.UserName;
                pm.gender = user.Gender;
                pm.userID = user.ID;
                var content = pm.ToJson();
                a.BPushs.Add(new BPush
                {
                    ID = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    MessageContent = content,
                    UserID = driverUserID,
                });
                a.SaveChanges();
            }
        }
    }

    public class PullMessage
    {
        public const int Driver_NewOrder = 101;
        public const int Driver_EnsureOrderDriver = 102;
        public const int Driver_EnsureOrderDriverFail = 103;
        public const int Driver_OrderCancel = 104;
        public const int Driver_ValidSuccess = 105;
        public const int Driver_AddPrice= 106;
        public const int Driver_SendGoods = 107;

        public const int Custom_NewDriverAccept = 201;
        public const int Custom_DriverRecived = 202;
        public const int Custom_EnsureComplete = 203;
        public const int Custom_OrderCancel = 204;
        /// <summary>
        /// 100-200司机
        /// 101:有新的送货信息;
        /// 
        /// 201-300货主
        /// 201订单被新的司机接受.
        /// </summary>
        public int type { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public string data { get; set; }
        public string phoneNumber { get; set; }
        public string userName { get; set; }
        public int gender { get; set; }
        public Guid userID { get; set; }
    }
}