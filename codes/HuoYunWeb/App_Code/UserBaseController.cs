﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Fw.Extends;
using Fw.Reflection;
using Fw.Serializer;
using HuoYunBLL;
using HuoYunWeb.App_Code.Push;
using HuoYunWeb.Models;
using Newtonsoft.Json;
//using NPOI.SS.Formula.Functions;
using ServerFw.Extends;
using WebGrease.Css.Extensions;


namespace HuoYunWeb.App_Code
{
    public class JsonNetResult : JsonResult
    {
        public JsonSerializerSettings Settings { get; private set; }

        public JsonNetResult()
        {
            Settings = new JsonSerializerSettings
            {
                //这句是解决问题的关键,也就是json.net官方给出的解决配置选项.                 
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (this.JsonRequestBehavior == JsonRequestBehavior.DenyGet && string.Equals(context.HttpContext.Request.HttpMethod, "GET", StringComparison.OrdinalIgnoreCase))
                throw new InvalidOperationException("JSON GET is not allowed");
            HttpResponseBase response = context.HttpContext.Response;
            response.ContentType = string.IsNullOrEmpty(this.ContentType) ? "application/json" : this.ContentType;
            if (this.ContentEncoding != null)
                response.ContentEncoding = this.ContentEncoding;
            if (this.Data == null)
                return;
            var scriptSerializer = JsonSerializer.Create(this.Settings);
           response.Write(JsonHelper.FastJsonSerializer(this.Data));
//            using (var sw = new StringWriter())
//            {
//                scriptSerializer.Serialize(sw, this.Data);
//                response.Write(sw.ToString());
//            }
        }

        public static string ToJson<T>(T t)
        {
            using (var sw = new StringWriter())
            {
                JsonSerializer js = new JsonSerializer();
                js.Serialize(sw, t);
                return sw.ToString();
            }
        }
    }
    public class UserBaseController : Controller
    {
       
        /// <summary>
        /// 生成验证码
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <returns>void</returns>
        [AllowAnonymous]
        public ActionResult SendValidCode(string phoneID, string phoneNumber)
        {
            var code = ValidCodeManage.NewCode();
            ValidCodeManage.SetCode(phoneNumber, code);
            ValidCodeAPI.SendCode(phoneNumber, code);
            return Result.GetSuccess();
        }

         
        [AllowAnonymous]
        public ActionResult PostError(string phoneID, string phoneNumber,string errorStr)
        {
            if (errorStr == null) return Result.GetSuccess();
            //var v = MvcApplication.Version;
            //var p = MvcApplication.UpdatePath;
            LogHelper.WriteClientErrorLog(phoneNumber+"\r\n"+errorStr);
            return Result.GetSuccess();
        }

//        /// <summary>
//        /// 生成验证码
//        /// </summary>
//        /// <param name="phoneID"></param>
//        /// <param name="phoneNumber"></param>
//        /// <returns>void</returns>
//        [AllowAnonymous]
//        public ActionResult SendCompleteValidCode(string phoneID, string phoneNumber,string orderNumber)
//        {
//            Para.Required("phoneID", phoneID);
//            Para.Required("phoneNumber", phoneNumber);
//            Para.Required("orderNumber", orderNumber);
//            using (var a = new SYSContext())
//            {
//                var order = a.Orders.FirstOrDefault(z => z.OrderNumber == orderNumber);
//                //未找到货主或订单
//                if (order == null) return (Result.GetError("未找到订单", 201));
//                if (order.Canceled == 1) return (Result.GetError("不能操作已经取消的订单", 201));
//                var driver = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
//                if (driver == null) return (Result.GetError("未找到人员", 201));
//
//                //货主和订单对不上
////                if (order.GuestUserID != custom.ID) return (Result.GetError("无法操作该订单", 201));
//                var driverRelOrders = order.DriverRelOrders.ToList();
//                var rel = driverRelOrders.FirstOrDefault(z => z.EnsureState == 1);
//                if (rel.DriverID != driver.ID) return (Result.GetError("无法操作该订单", 201));
//                var relIDs = driverRelOrders.Select(z => z.ID).ToList();
//                a.Configuration.LazyLoadingEnabled = false;
//                a.Configuration.ProxyCreationEnabled = false;
//                var reldetails = a.DriverRelOrderDetails.Where(z => relIDs.Contains(z.DriverRelOrderID)).OrderBy(z => z.AtThatTime).ToList();
//                reldetails.ForEach(z => z.DriverRelOrder = null);
//                order.DriverRelOrders = null;
//                return Result.GetSuccess();
//            }
//        }
        public JsonResult getDefaultJsonResult(string navTabId = null, string callbackType = null, string message = "保存成功", int statusCode = 200)
        {
            var c = navTabId ?? Request["NT"] ?? RouteData.GetController();
            return new JsonResult
            {
                Data = new
                {
                    statusCode = statusCode,
                    message = message,
                    callbackType = callbackType ?? "closeCurrent",
                    navTabId = c
                }
            };
        }


        /// 获取个人string GetImage(string phoneID,string phoneNumber,int imageType)基本信息
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <returns>bool</returns>
        public ActionResult GetImage(string phoneID, string phoneNumber, int imageType)
        {
            using (var a = new SYSContext())
            {
                var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
                if (currentUser == null) return (Result.GetError("未找到人员", 201));
                return Result.GetResult(true, null, ImageManage.GetUserImage(currentUser.ID, imageType));
            }
        }

        /// 获取其他人的头像信息string GetImage(string phoneID,string phoneNumber,int imageType)基本信息
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <returns>bool</returns>
        public ActionResult GetOtherUserImage(string phoneID, string phoneNumber,string otherPhoneNumber,int imageType = 0)
        {
            if (imageType != 0) return (Result.GetError("图片类型错误", 201));
            using (var a = new SYSContext())
            {

                var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == otherPhoneNumber);
                if (currentUser == null) return (Result.GetError("未找到人员", 201));
                return Result.GetResult(true, null, ImageManage.GetUserImage(currentUser.ID, imageType));
            }
        }
        /// <summary>
        /// 获取其他人的头像信息string GetImage(string phoneID,string phoneNumber,int imageType)基本信息
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <returns>bool</returns>
        public ActionResult GetOtherUserBaseInfo(string phoneID, string phoneNumber,string otherPhoneNumber)
        {
            using (var a = new SYSContext(true,false))
            {
                var currentUser = a.Users.Include(w=>w.CustomerInfo).FirstOrDefault(w => w.PhoneNumber == otherPhoneNumber);
                if (currentUser == null) return (Result.GetError("未找到人员", 201));

                return Result.GetSuccess(currentUser.FilterInfo());
            }
        }
        protected static object RegisterFlag = new object();
       

        /// <summary>
        /// 登陆
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="password"></param>
        /// <returns>LoninResult</returns>
        [AllowAnonymous]
        public ActionResult Login(string phoneID, string phoneNumber, string validCode)
        {
            Para.Required("phoneId", phoneID);
            Para.Required("phoneNumber", phoneNumber);
            //            Para.Required("password", password);
            string pk = new Random().Next(1000000, 9999999).ToString();
            DateTime pKeyOverDate = DateTime.Now.AddDays(1);
            var UserState = 0;
            string userID = "";
            using (var a = new SYSContext(true,false))
            {
                var user = a.Users.FirstOrDefault(w => w.Account == phoneNumber);
                if (user == null) return (Result.GetLoginResult(false, "账号未找到", 101, null, DateTime.Now));
                if (user.Locked == 1) return (Result.GetLoginResult(false, "账号冻结", 102, null, DateTime.Now));
                //                if (user.Password != password) return (Result.GetLoginResult(false, "密码错误", 103, null, DateTime.Now));
                var sys_ValidCode = ValidCodeManage.GetCode(phoneNumber);
                if (string.IsNullOrEmpty(sys_ValidCode) == null || 
                    string.IsNullOrEmpty(validCode)||
					(!string.Equals(validCode, MvcApplication.PublicValidCode) && !string.Equals(sys_ValidCode, validCode, StringComparison.OrdinalIgnoreCase)))
                {
                    return (Result.GetLoginResult(false, "验证码错误", 103, null, DateTime.Now));
                }
                user.PKey = pk;
                user.PKeyOverDate = pKeyOverDate;
                UserHelper.AddUser(user);
                a.SaveChanges();
                UserState = user.UserState;
                userID = user.ID.ToString();
                UserHelper.RemoveUserRandomHistory(user.ID);
            }
            return (Result.GetSuccess(new LoninResult { PK = pk, UserID = userID, UserState = UserState, PKOverDate = pKeyOverDate.ToLongString(), Success = 1 }));
        }


        /// <summary>
        /// 获取个人基本信息
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <returns>bool</returns>
        public ActionResult GetUser(string phoneId, string phoneNumber)
        {
            Para.Required("phoneId", phoneId);
            Para.Required("phoneNumber", phoneNumber);
            User currentUser;
            using (var a = new SYSContext(true,false))
            {
                currentUser = a.Users.Include(z=>z.CustomerInfo).Include(z => z.DriverInfo).FirstOrDefault(w => w.PhoneNumber == phoneNumber);
                if (currentUser == null) return (Result.GetError("未找到人员信息", 204));
                if (currentUser.Locked == 1) return (Result.GetError("人员信息被锁定", 207));
                return Result.GetSuccess(currentUser);
            }
        }

//        /// <summary>
//        /// 获取个人基本信息
//        /// </summary>
//        /// <param name="phoneID"></param>
//        /// <param name="phoneNumber"></param>
//        /// <returns>bool</returns>
//        public ActionResult GetOtherUser(string phoneId, string phoneNumber, string otherPhoneNumber)
//        {
//            Para.Required("phoneId", phoneId);
//            Para.Required("phoneNumber", phoneNumber);
//            Para.Required("otherPhoneNumber", otherPhoneNumber);
//            User currentUser;
//            using (var a = new SYSContext(true,false))
//            {
//                currentUser = a.Users.Include(z => z.DriverInfo).FirstOrDefault(w => w.PhoneNumber == otherPhoneNumber);
//                if (currentUser == null) return (Result.GetError("非法操作", 201));
//
//                return Result.GetSuccess(currentUser);
//            }
//        }
        /// <summary>
        /// 更新个人基本信息
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="user1"></param>
        /// <returns>bool</returns>
        public ActionResult UpdateUserInfo(string phoneID, string phoneNumber, string user)
        {
            var user1 = JsonHelper.FastJsonDeserialize<User>(user);
            Para.Required("user", user);
            using (var a = new SYSContext())
            {
                bool isNew = true;
                if (user1.ID == Guid.Empty)
                {
                    user1.ID = Guid.NewGuid();
                }
                else
                {
                    var currentUser = a.Users.FirstOrDefault(w => w.ID == user1.ID);
                    if (currentUser != null)
                    {
                        if (currentUser.PhoneNumber != phoneNumber)//操作别人的订单
                        {
                            return (Result.GetError("非法操作", 201));
                        }
                        isNew = false;
                        ReflectionHelper.SetBaseAttributeValues(currentUser, user1, new[] { "ID", "PKey", "MessageValidCode", "PhoneNumber", "PhoneID", "Account", "PKeyOverDate", "ImagePath", "B_UserID", "B_Channel", });
//                        a.Users.AddOrUpdate(user1);
                    }
                }
                if (isNew)
                {
                    return (Result.GetError("非法操作", 201));
//                    a.Users.AddOrUpdate(user1);
                }
                a.SaveChanges();
            }
            return Result.GetSuccess();
        }

        /// <summary>
        /// 更新人员的推送信息
        /// </summary>
        /// <param name="phoneID"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="user1"></param>
        /// <returns>bool</returns>
        public ActionResult UpdateUserID(string phoneID, string phoneNumber, string userID, string channel)
        {
            Para.Required("driver", phoneID);
            Para.Required("phoneNumber", phoneNumber);
            Para.Required("userID", userID);
            Para.Required("channel", channel);
            using (var a = new SYSContext())
            {
                var currentUser = a.Users.FirstOrDefault(w => w.PhoneNumber == phoneNumber);
                if (currentUser != null)
                {
                    currentUser.B_UserID = userID;
                    currentUser.B_Channel = channel;
                    a.SaveChanges();
                }
                else
                {
                    Result.GetError("未找到人员", 208);
                }
                
            }
            return Result.GetSuccess();
        } 
        //出错处理
        protected override void OnException(ExceptionContext filterContext)
        {
            StringBuilder sb = new StringBuilder();
            var request = filterContext.HttpContext.Request;
            sb.AppendLine("出错 " + SessionCode);
            sb.AppendLine(request.RawUrl);
            LogHelper.WriteLog(GetType(), sb.ToString());
            LogHelper.WriteLog(GetType(), filterContext.Exception);
            base.OnException(filterContext);
        }

        private string SessionCode;
        static Random r = new Random();
        //执行Action前
        protected override IAsyncResult BeginExecute(RequestContext requestContext, AsyncCallback callback, object state)
        {
            SessionCode = DateTime.Now.ToString("yyyyMMddHHmmss") + r.Next(10000,99999);
            StringBuilder sb = new StringBuilder();
            var request = requestContext.HttpContext.Request;
            sb.AppendLine("执行前" + SessionCode);
            sb.AppendLine(request.RawUrl);
            var allKeys = request.Form.AllKeys;
            var keys = allKeys.ToList();
            keys.ForEach(w =>
            {
                var args = request[w];
                if (!string.IsNullOrEmpty(args))sb.AppendLine(string.Format("{0}:{1}", w, args));
            });
            keys = request.QueryString.AllKeys.ToList();
            keys.ForEach(w =>
            {
                var args = request[w];
                if (!string.IsNullOrEmpty(args))sb.AppendLine(string.Format("{0}:{1}", w, args));
            }); 
            LogHelper.WriteLog(GetType(),sb.ToString());
            return base.BeginExecute(requestContext, callback, state);
        }
        //执行Action后
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                filterContext.Result = Result.GetError(filterContext.Exception.Message, 999);
                filterContext.ExceptionHandled = true;
                LogHelper.WriteLog(GetType(), filterContext.Exception);

            }
            var ere = filterContext.Result as JsonResult;
            if (ere != null)
            {
                
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("执行后" + SessionCode);
                var request = filterContext.HttpContext.Request;
                sb.AppendLine(request.RawUrl);
                var str = ere.Data.ToJson();
                sb.AppendLine(str);

                LogHelper.WriteLog(GetType(), sb.ToString());
            }
//            else//运行有错误
//            {
//                
//            }
            base.OnActionExecuted(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }

        protected override void Execute(RequestContext requestContext)
        {
            base.Execute(requestContext);
        }
    }
}
