﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using HuoYunWeb.Controllers;

namespace HuoYunWeb
{
    public static class Helpers
    {
        public static HtmlString Get是否(this int owner)
        {
            return new HtmlString(owner == 1 ? "是" : "<font color=\"red\">否</font>");
        }

        public static HtmlString GetOrderStateStr(this int owner)
        {
            /// Int OrderState 状态(0新订单,1已接受,2货主选中车主,3车主已接货,4发货中,5完成,6已评价,7货主已发货(货主确定把货给车主),-1错过,-2不接受,-3没被选中,-4(1)已接受后取消,-5(2)货主选中车主后取消)
            /// 0 1 2 7 3 4 5 6
            var state = "--";
            switch (owner)
            {
                case 0: state= "新订单";break;
                case 1: state = "已接受"; break;
                case 2: state = "选定车主"; break;
                case 3: state = "车主已接货"; break;
                case 4: state = "发货中"; break;
                case 5: state = "交易完成"; break;
                case 7: state = "车主已接货"; break;
                case -6: state = "取消"; break;
                case -7: state = "失效"; break;
            }
            return new HtmlString(state);
        }

        
        public static HtmlString GetOrderFromStr(this int owner)
        {
            var state = "--";
            switch (owner)
            {
                case 1: state = "手机下单"; break;
                case 2: state = "微信下单"; break;
                case 3: state = "电话下单"; break;
            }
            return new HtmlString(state);
        }

        public static HtmlString GetCurrentDriver(this List<OrderDriverItem> owner)
        {
            var item = owner.FirstOrDefault(z => z.EnsureState == 1);
            if (item == null) item = owner.FirstOrDefault(z => z.OrderState >=2);
            if (item != null)
            {
                return new HtmlString("<font color=\"red\">" + item.UserName + "(" + item.PhoneNumber + ")</font>");
            }
            return new HtmlString("--");
        }

        public static HtmlString GetDriverListText1(this List<OrderDriverItem> owner)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item1 in owner)
            {
                var str = item1.OrderState.GetOrderStateStr().ToString();
                var str1 = string.Format("{0}({1}):{2}", item1.UserName, item1.PhoneNumber, str);
                sb.AppendLine(str1);
            }
            var re = sb.ToString();
            if (string.IsNullOrEmpty(re)) re = "无信息";
            return new HtmlString(re);
        }

        public static HtmlString ReadableTimerStr(this DateTime owner)
        {
            return ReadableTimerStr((DateTime?)owner);
        }
        public static HtmlString GetGenderStr(this int owner)
        {
            string userName = "";
            if (owner == 1) userName += "先生";
            else userName += "女士";
            return new HtmlString(userName);
        }
        public static HtmlString ReadableTimerStr(this DateTime? owner)
        {
            if(owner == null)return new HtmlString("");
            var now = DateTime.Now;
            TimeSpan sp = now - owner.Value;
            string re = null;
            if (sp.TotalDays > 365) re = "一年前";
            else if (sp.TotalDays > 180) re = "半年前";
            else if (sp.TotalDays > 150) re = "5个月前";
            else if (sp.TotalDays > 120) re = "4个月前";
            else if (sp.TotalDays > 90) re = "3个月前";
            else if (sp.TotalDays > 60) re = "2个月前";
            else if (sp.TotalDays > 30) re = "1个月前";
            else if (sp.TotalDays > 21) re = "3周前";
            else if (sp.TotalDays > 14) re = "3周前";
            else if (sp.TotalDays > 7) re = "3周前";
            else if (sp.TotalDays > 1) re = (int)sp.TotalDays + "天前";
            else if (sp.TotalHours > 1) re = (int)sp.TotalHours + "小时前";
            else if (sp.TotalMinutes > 1) re = (int)sp.TotalMinutes + "分钟前";
            return new HtmlString(re);
        }
    }
}