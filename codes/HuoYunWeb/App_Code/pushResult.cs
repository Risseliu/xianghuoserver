﻿namespace HuoYunWeb
{
    /// <summary>
    /// 百度推送结果
    /// </summary>
    public class pushResult
    {
        public long request_id { get; set; }
        public string error_code { get; set; }
        public string error_msg { get; set; }
    }
}