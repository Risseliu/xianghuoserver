﻿using System;
using System.Web;
using Fw.Extends;

namespace HuoYunWeb
{
    public class CookieHelper
    {
        //        public static void Add(string name, string value)
        //        {
        //            Add(name, value, null);
        //        }
        public static void Add<T>(string name, T value, TimeSpan? overTime = null) where T : class
        {
            Add(name, value.ToJson().ToBase64(), overTime);
        }

        public static void Remove(string name)
        {
            HttpContext.Current.Request.Cookies.Remove(name);
            HttpContext.Current.Response.AppendCookie(new HttpCookie(name));
        }
        public static void Add(string name, string value, TimeSpan? overTime = null)
        {
            var httpCookie = new HttpCookie(name, value);
            httpCookie.Path = "/";
            //            httpCookie["a"] = value;
            if (overTime != null)
            {
                httpCookie.Expires = DateTime.Now + overTime.Value;
            }
            HttpContext.Current.Response.AppendCookie(httpCookie);
        }

        public static T Get<T>(string name) where T : class
        {
            return Get(name).FromBase64().FromJson<T>();
        }
        public static string Get(string name)
        {
            var c = HttpContext.Current.Request.Cookies[name];
            if (c != null)
            {
                return c.Value;
            }
            return null;
        }
    }
}