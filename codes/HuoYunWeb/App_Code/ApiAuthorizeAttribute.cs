﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
//using HuoYunBLL;
//
//namespace HuoYunWeb.App_Code
//{
//    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
//    public class ApiAuthorizeAttribute : ApiBaseAuthorizeAttribute
//    {
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="filterContext"></param>
//        /// <param name="c">随机字符串</param>
//        /// <returns></returns>
//        protected override string GetKey(AuthorizationContext filterContext,string c)
//        {
//            var request = filterContext.HttpContext.Request;
//
//            var phoneNumber = request["phoneNumber"]; //加密结果
//            if (string.IsNullOrEmpty(phoneNumber))return null;
//            var user = UserHelper.GetUserByPhoneNumber(phoneNumber);
//            if (user == null) return null;
//            if (user.Locked == 1 || user.PKeyOverDate == null || DateTime.Now > user.PKeyOverDate) return null;
//            UserHelper.AddRandomHistory(user.ID,c);
//            return user.PKey;
//        }
//    }
//}