﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Fw.Extends;
using Fw.IO;
using HuoYunBLL;
using HuoYunWeb.Models;

namespace HuoYunWeb
{
    public static class UserHelper
    {

        const string UserCookieName = "U";
        public static void SetLogin(Admin user)
        {
            CookieHelper.Add(UserCookieName, user, TimeSpan.FromMinutes(60));
        }

        public static Admin CurrentUser()
        {
            return CookieHelper.Get<Admin>(UserCookieName);
        }

        public static void SetLogout()
        {
            CookieHelper.Remove(UserCookieName);
        }
        static List<User> users = new List<User>();

        /// <summary>
        /// 返回人员,先从缓存中找,如果没有,就从数据库中找.
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public static User GetUserByPhoneNumber(string phoneNumber)
        {
            lock (users)
            {
                var user = users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
                if (user != null) return user;
            }
            using (var a = new SYSContext())
            {
                var user = a.Users.FirstOrDefault(z => z.PhoneNumber == phoneNumber);
                return user;
            }
        }
        public static void AddUser(User user)
        {
            lock (users)
            {
                if (users.FirstOrDefault(z => z.ID == user.ID)!=null)
                {
                    users.Remove1(z => z.ID == user.ID);
                    
                }
                users.Add(user);
            }
        }
        #region 手机端功能

        /// <summary>
        /// 返回指定的随机数据是否用过
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="rKey"></param>
        /// <returns></returns>
        public static bool CheckHaveRandom(Guid userID,string rKey)
        {
            lock (RandomHistory)
            {
                if (RandomHistory.ContainsKey(userID))
                {
                    var list = RandomHistory[userID];
                    return list.Contains(rKey);
                }
            }
            return false;
        }
        /// <summary>
        /// 返回指定的随机数据是否用过
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="rKey"></param>
        /// <returns></returns>
        public static List<string> GetUserRandomHistoryCopy(Guid userID)
        {
            lock (RandomHistory)
            {
                if (RandomHistory.ContainsKey(userID))
                {
                    var list = RandomHistory[userID];
                    return list.ToList();
                }
            }
            return new List<string>();
        }

        /// <summary>
        /// 人员发送的随机数集合(人员ID,人员的数据)
        /// </summary>
        private static Dictionary<Guid, List<string>> RandomHistory = new Dictionary<Guid,List<string>>();

        /// <summary>
        /// 最后一次保存临时数据到硬盘的时间
        /// </summary>
        private static DateTime lastSaveRandomHistoryTime = DateTime.MinValue;
        /// <summary>
        /// 添加一个已经使用的Key
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="rKey"></param>
        public static void AddRandomHistory(Guid userID,string rKey)
        {
            lock (RandomHistory)
            {
                //10分钟一次保存到硬盘
                TimeSpan timeSpan = DateTime.Now - lastSaveRandomHistoryTime;
                if (timeSpan.TotalMinutes > 10)
                {
                    lastSaveRandomHistoryTime = DateTime.Now;
                    new Thread(w =>
                    {
                        SaveHistoryToDisk();
                    }).Start();
                }

                if (!RandomHistory.ContainsKey(userID))
                {
                    var diskList = ReadUserHistoryKeyFromDisk(userID);
                    RandomHistory.Add(userID, diskList);
                }

                var list = RandomHistory[userID];
                if(!list.Contains(rKey))list.Add(rKey);
            }
        }

        /// <summary>
        /// 删除人员的历史key
        /// </summary>
        /// <param name="userID"></param>
        public static void RemoveUserRandomHistory(Guid userID)
        {
            lock (RandomHistory)
            {
                if (RandomHistory.ContainsKey(userID))//清空缓存
                {
                    RandomHistory[userID].Clear();
                }
            }
            var path = GetUserRandomHistoryFilePathAndCreateFolder(userID);
            lock (_randomHistoryFileFlag)//清除文件
            {
                FileHelper.WriteAllLines(path, new string[]{});
            }
        }

        static List<string> ReadUserHistoryKeyFromDisk(Guid userID)
        {
            var path = GetUserRandomHistoryFilePathAndCreateFolder(userID);
            lock (_randomHistoryFileFlag)
            {
                var list = FileHelper.ReadAllLines(path)??new string[0];
                return list.ToList();
            }
        }
        /// <summary>
        /// 保存所有的人员历史Key到硬盘中.
        /// </summary>
        private static void SaveHistoryToDisk()
        {
            List<Guid> users = new List<Guid>();
            lock (RandomHistory)
            {
                users = RandomHistory.Keys.ToList();
            }
            foreach (var user in users)
            {
                var keys = GetUserRandomHistoryCopy(user);
                var path = GetUserRandomHistoryFilePathAndCreateFolder(user);
                var str = keys.ToArray();
                lock (_randomHistoryFileFlag)
                {
                    FileHelper.WriteAllLines(path, str);
                }
            }
        }
        static object _randomHistoryFileFlag = new object();
        /// <summary>
        /// 返回人员的历史Key文件,并创建目录
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        static string GetUserRandomHistoryFilePathAndCreateFolder(Guid userID)
        {
            string path = Path.Combine(MvcApplication.RootPath, "UserData", userID.ToString(), "pkeys.config");
            FileHelper.CreateFolderByFilePath(path);
            return path;
        }

        #endregion
    }
}