using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using Fw.Serializer;
using HuoYunWeb.Models;
using PushAspxDemo;

namespace HuoYunWeb
{
    public class PushService
    {
        /// <summary>
        /// 添加一条消息到队列中.
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="message"></param>
        public static void AddMessge(Guid  userid,string message)
        {
            SYSContext.Run1(w =>
            {
                w.BPushs.Add(new BPush
                {
                    ID = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    MessageContent = message,
                    UserID = userid,
                });
                w.SaveChanges();
            });
        }
        public string ApiKey { get; set; }
        public string SecretKey { get; set; }
        private Thread thread;
        public static PushService Current { get; private set; }

        public static void Start(string apiKey, string secretKey)
        {
            Current = new PushService();
            Current.ApiKey = apiKey;
            Current.SecretKey = secretKey;
            Current.Start();
        }
        public void Start()
        {
            try
            {
                if (thread != null) thread.Abort();
            }
            catch (Exception)
            {
                
            }
            thread = new Thread(w =>
            {
                _start();
            });
            thread.Start();
        }

        private void _start()
        {
            while (true)
            {
                
                try
                {
                    Thread.Sleep(5000);
                    StartTask();
                }
                catch
                {
                    
                }
            }
        }

        private uint device_type = 3;//android 3,ios 4
        private void StartTask()
        {
            using(var w = new SYSContext(false))
            {
                w.BPushs.Include(z => z.UserInfo);
                //找出列表中最近3分钟没有发送过,且没有成功的消息
                DateTime addMinutes = DateTime.Now.AddMinutes(-3);

                var orderedQueryable = w.BPushs.Include(z => z.UserInfo)
                    .Where(z => z.PushCount < 10 && z.Success == 0 && z.LastPushTime < addMinutes || z.LastPushTime == null)
                    .OrderBy(z => z.CreateTime).Take(50);
                var list = orderedQueryable.ToList();
                list.ForEach(z =>
                {
                    Thread.Sleep(1);//10毫秒推送一个消息
                    PushMessage(z);
                    w.SaveChanges();
                });
            };
        }

        public BPush PushMessage(BPush z)
        {
            TimeSpan ts = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));
            uint device_type = 3;
            uint unixTime = (uint) ts.TotalSeconds;

            var bUserId = z.UserInfo.B_UserID;
			var device = z.UserInfo.DeviceType;
			var apikey = ApiKey;
			var secretKey = SecretKey;

			if (device != null)
			{
 				device_type=(uint)device;
				if (device_type != 4)
				{
					device_type = 3;
				}
			}

			if (device_type == 4)
			{
				apikey = MvcApplication.IOSApiKey;
				secretKey = MvcApplication.IOSSecretKey;
			}
			

            var channelId = z.UserInfo.B_Channel;
			var pOpts = new PushOptions("push_msg", apikey, bUserId, channelId, device_type,
                z.MessageContent, "aa", unixTime);
			if (device_type == 4)
			{
				pOpts.deploy_status = 2;
			}
            pOpts.message_type = 0; //0为通知,1是文本信息.

			BaiduPush Bpush = new BaiduPush("POST", secretKey);
            z.PushCount++;
            z.LastPushTime = DateTime.Now;
            if (bUserId != null && bUserId.ToLower() != "null" &&
                channelId != null && channelId.ToLower() != "null"
                )
            {
                z.PushResult = Bpush.PushMessage(pOpts);
                var re = JsonHelper.FastJsonDeserialize<pushResult>(z.PushResult);
                if (re != null)
                {
                    if (re.error_code == null) //发送成功
                    {
                        z.SuccessPushTime = DateTime.Now;
                        z.Success = 1;
                    }
                }
            }
            else
            {
                z.PushResult = "人员的B_UserID为空";
            }
            return z;
        }
    }
}