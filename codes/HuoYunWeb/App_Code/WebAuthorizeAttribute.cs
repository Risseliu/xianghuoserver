﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Fw.Extends;
using Fw.Serializer;

namespace HuoYunWeb
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class WebAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var admin = UserHelper.CurrentUser();
            if (admin == null)
            {
                filterContext.RequestContext.HttpContext.Response.Redirect("/Home/WebLogin/");
//                filterContext.Result = new JsonResult { Data = new { statusCode = "301", message ="登陆超时",},JsonRequestBehavior = JsonRequestBehavior.AllowGet}; ;
            }
//            var c = request["C"];//随机字符串
//            var r = request["R"];//加密结果
//            var success = true;
//            var p = "123";//私钥
//            var result = FormsAuthentication.HashPasswordForStoringInConfigFile(c + p, "MD5");
//            if (result != null) success = result.Equals(r, StringComparison.OrdinalIgnoreCase);
//            if (!success)
//            {
//                filterContext.Result = Result.GetResult(false, "未验证的数据", null);
//            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return false;
            return base.AuthorizeCore(httpContext);
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //            filterContext.Result = 
            base.OnAuthorization(filterContext);
        }
    }

    public static class MyConfigManage
    {
        /// <summary>
        /// 返回配置文件的绝对地址
        /// </summary>
        public static string FilePath
        {
            get { return Path.Combine(MvcApplication.RootPath, "myConfig.config"); }
        }

        private static MyConfig _current;
        private static object flag = new object();

        static DateTime lastReadTime = DateTime.MinValue;
        public static MyConfig Current
        {
            get
            {
                lock (flag)
                {
                    if ((DateTime.Now - lastReadTime).TotalSeconds > 10)
                    {
                        _current = null;
                        lastReadTime = DateTime.Now;
                    }
                    if (_current == null)
                    {
                        if (File.Exists(FilePath))
                        {
                            _current = JsonHelper.FastJsonDeserializeFile<MyConfig>(FilePath);
                        }
                    }
                    if (_current == null)
                    {
                        _current = new MyConfig();
                    }
                    return _current;
                }
            }
        }

        public static void SaveConfig()
        {
            lock (flag)
            {
                var c = Current;
                if (c != null)
                {
                    JsonHelper.FastJsonSerializerToFile(FilePath,c);
                }
            }
        }
    }

    public class MyConfig
    {
        public MyConfig()
        {
//            Tables = new Dictionary<string, string>();
        }
        /// <summary>
        /// 0:车主验证后才能收到推送,1不验证也能收到推送
        /// </summary>
        public int ValidDriverEnabled { get; set; }
        public Dictionary<string, string> Tables { get; set; }
    }
}