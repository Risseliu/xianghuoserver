﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Web;
using HuoYunWeb.Models;

namespace HuoYunWeb.App_Code
{

    public class MyDropCreateDatabaseAlways : DropCreateDatabaseAlways<SYSContext>
    {
        protected override void Seed(SYSContext context)
        {
            base.Seed(context);
            MyDropCreateDatabaseIfModelChanges.Init(context);
        }
    }
    public class MyDropCreateDatabaseIfModelChanges : DropCreateDatabaseIfModelChanges<SYSContext>
    {
        public override void InitializeDatabase(SYSContext context)
        {
            base.InitializeDatabase(context);
            
        }

        protected override void Seed(SYSContext context)
        {
            base.Seed(context);
            Init(context);
        }

        public static void Init(SYSContext context)
        {
            var user = new User
            {
                UserName = "tht",
                Account = "123456789",
                DriverInfo = new Driver
                {
                    CheXing = "面包车",
                    ChePai = "JI123456",
                    Valid = 0,
                    ValidSubmit = 1,
                    RecivedPush = 1,
                    DriverLines = new Collection<DriverLine>
                    {
                        new DriverLine
                        {
                            StartCityName = "北京",
                            StartName = "植物园",
                            StartPointX = 116.243292,
                            StartPointY = 39.996569,
                            EndCityName = "北京",
                            EndName = "三里屯",
                            EndPointX = 116.459173,
                            EndPointY = 39.942655,
                            ReturnAble = 1,
                            IsEnabled = 1,
                        }
                    }
                },
            };
            context.Users.Add(user);
            context.SaveChanges();
            context.Orders.Add(new Order
            {
                GuestUserID = user.ID,
                OrderNumber = DateTime.Now.ToString("yyyyMMddHHmmss"),
                StartCityName = "北京",
                StartName = "植物园",
                StartPointX = 116.243292,
                StartPointY = 39.996569,
                EndCityName = "北京",
                EndName = "三里屯",
                EndPointX = 116.459173,
                EndPointY = 39.942655,
                OrderState = 0,
                PKey = "123456",
                Lading = "书籍",
                Weight = "20kg",
                PublishTime = DateTime.Now,
                DeliverTimeStart = DateTime.Now,
                Price = 35,
                ReciveUserName = "xxx",
                ReciveUserPhoneNumber = "123456789",
                DriverRelOrders = new Collection<DriverRelOrder>
                {
                    new DriverRelOrder
                    {
                        DriverID = user.ID,
                        OrderState = 0,
                    }
                }
            });
            context.SaveChanges();
        }
    }
}