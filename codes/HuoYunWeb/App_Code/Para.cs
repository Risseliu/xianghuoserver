﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BaiDuAPICom;
using Fw.Extends;
using Fw.IO;
using HuoYunWeb.Models;
using System.Text;
using System.Net;

//[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace HuoYunWeb
{
    /// <summary>
    /// 参数验证
    /// </summary>
    public class Para
    {
        public static void Required(string name, Guid value)
        {
            if (value == Guid.Empty) throw new Exception(name + "不能为空");

        }
        public static void Required<T>(string name, T value) 
        {
            if (value == null) throw new Exception(name + "不能为空");
        }
    }
    public class BaiduData
    {
        /// <summary>
        /// 请求返回状态
        /// </summary>
        public int status { set; get; }
        /// <summary>
        /// 请求结果
        /// </summary>
        public string message { set; get; }
        public List<BaiDuAddress> result { set; get; }
    }
    public class BaiDuAddress
    {
        public string name { set; get; }
        public string city { set; get; }
        public string district { set; get; }
        public string business { set; get; }
        public string cityid { set; get; }
    }
    public class BaiDuMapAPI
    {
        /// <summary>
        /// 执行HTTP GET请求。
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="parameters">请求参数</param>
        /// <returns>HTTP响应</returns>
        public static string DoGet(string url, IDictionary<string, string> parameters)
        {
            if (parameters != null && parameters.Count > 0)
            {
                if (url.Contains("?"))
                {
                    url = url + "&" + BuildPostData(parameters);
                }
                else
                {
                    url = url + "?" + BuildPostData(parameters);
                }
            }

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.ServicePoint.Expect100Continue = false;
            req.Method = "GET";
            req.KeepAlive = true;
            req.UserAgent = "Test";
            req.ContentType = "application/x-www-form-urlencoded;charset=utf-8";

            HttpWebResponse rsp = null;
            try
            {
                rsp = (HttpWebResponse)req.GetResponse();
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.Timeout)
                {
                    rsp = null;
                }
            }

            if (rsp != null)
            {
                if (rsp.CharacterSet != null)
                {
                    Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
                    return GetResponseAsString(rsp, encoding);
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 把响应流转换为文本。
        /// </summary>
        /// <param name="rsp">响应流对象</param>
        /// <param name="encoding">编码方式</param>
        /// <returns>响应文本</returns>
        private static string GetResponseAsString(HttpWebResponse rsp, Encoding encoding)
        {
            StringBuilder result = new StringBuilder();
            Stream stream = null;
            StreamReader reader = null;

            try
            {
                // 以字符流的方式读取HTTP响应
                stream = rsp.GetResponseStream();
                reader = new StreamReader(stream, encoding);

                // 每次读取不大于256个字符，并写入字符串
                char[] buffer = new char[256];
                int readBytes = 0;
                while ((readBytes = reader.Read(buffer, 0, buffer.Length)) > 0)
                {
                    result.Append(buffer, 0, readBytes);
                }
            }
            catch (WebException webEx)
            {
                if (webEx.Status == WebExceptionStatus.Timeout)
                {
                    result = new StringBuilder();
                }
            }
            finally
            {
                // 释放资源
                if (reader != null) reader.Close();
                if (stream != null) stream.Close();
                if (rsp != null) rsp.Close();
            }

            return result.ToString();
        }

        /// <summary>
        /// 组装普通文本请求参数。
        /// </summary>
        /// <param name="parameters">Key-Value形式请求参数字典。</param>
        /// <returns>URL编码后的请求数据。</returns>
        private static string BuildPostData(IDictionary<string, string> parameters)
        {
            StringBuilder postData = new StringBuilder();
            bool hasParam = false;

            IEnumerator<KeyValuePair<string, string>> dem = parameters.GetEnumerator();
            while (dem.MoveNext())
            {
                string name = dem.Current.Key;
                string value = dem.Current.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                {
                    if (hasParam)
                    {
                        postData.Append("&");
                    }

                    postData.Append(name);
                    postData.Append("=");
                    postData.Append(Uri.EscapeDataString(value));
                    hasParam = true;
                }
            }
            return postData.ToString();
        }
        /// <summary>
        /// 返回50km以内的车主列表
        /// </summary>
        /// <param name="drivers"></param>
        /// <param name="startX"></param>
        /// <param name="startY"></param>
        /// <param name="endX"></param>
        /// <param name="endY"></param>
        /// <returns></returns>
        public List<DriverResult> GetRangeResult50KM(List<Driver> drivers, double startX, double startY, double endX, double endY, double range)
        {
            var list = GetRangeResult(drivers, startX, startY, endX, endY);
            //            int range = 50 * 1000;//50km
            var re = list.Where(w => w.StartRange < range && w.EndRange < range).OrderBy(z=>z.StartRange + z.EndRange).Take(100).ToList();
            return re;
        }
        public List<DriverResult> GetRangeResult(List<Driver> drivers, double startX, double startY, double endX, double endY)
        {
            return drivers.Select(w =>
            {
                double startRange = double.MaxValue / 4, endRange = double.MaxValue / 4;
                w.DriverLines.Where(z=>z.IsEnabled == 1).ToList().ForEach(z =>
                {
                    var sr = BaiDuAPI.GetDistance(z.StartPointX, z.StartPointY, startX, startY);
                    var er = BaiDuAPI.GetDistance(z.EndPointX, z.EndPointY, endX, endY);
                    if (sr + er < startRange + endRange)
                    {
                        startRange = sr;
                        endRange = er;
                    }
                });
                return new DriverResult { Driver = w, StartRange = startRange, EndRange = endRange };
            }).OrderBy(z => z.EndRange + z.StartRange).ToList();
        }
    }

    public class DriverResult
    {
        public Driver Driver { get; set; }
        /// <summary>
        /// 开始点离货主的距离
        /// </summary>
        public double StartRange { get; set; }
        /// <summary>
        /// 结束点离收货人的距离
        /// </summary>
        public double EndRange { get; set; }
    }
    /// <summary>
    /// 图片管理
    /// </summary>
    public class ImageManage
    {
        public static string ImagePath
        {
            get
            {
                return Path.Combine(MvcApplication.RootPath, "UserImage");
            }
        }

        public static string GetUserImageName(int imageType)
        {
            return imageType + ".jpg";
        }

        public static string GetUserImageUrl(Guid userID, int imageType)
        {
            return Path.Combine(GetUserImageUrl(userID),GetUserImageName(imageType));
        }
        public static string GetUserImagePath(Guid userID, int imageType)
        {
            var fileName = imageType + ".jpg";
            return Path.Combine(GetUserImagePath(userID), fileName);
        }

        public static string GetUserImageUrl(Guid userID)
        {
            return Path.Combine("UserImage", userID.ToString());
            
        }
        public static string GetUserImagePath(Guid userID)
        {
            return Path.Combine(ImagePath, userID.ToString());
        }
        /// <summary>
        /// 保存图片
        /// </summary>
        /// <param name="userID">人员ID</param>
        /// <param name="imageByte">图片内容</param>
        /// <param name="imageType">图片类型</param>
        public static void SaveUserImage(Guid userID, string imageByte, int imageType)
        {
            var bytes = imageByte.ByteFromBase64();
            string userImagePath = GetUserImagePath(userID,imageType);
            FileHelper.CreateFolderByFilePath(userImagePath);
            File.WriteAllBytes(userImagePath,bytes);
        }
        /// <summary>
        /// 返回图片
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <param name="imageByte"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetUserImage(Guid userID, int imageType)
        {
            string userImagePath = GetUserImagePath(userID, imageType);
            if (File.Exists(userImagePath))
            {
                var bytes = File.ReadAllBytes(userImagePath);
                return Convert.ToBase64String(bytes);
            }
            return "";
        }
    }
    public class LogHelper
    {
        /// <summary>
        /// 输出日志到Log4Net
        /// </summary>
        /// <param name="t"></param>
        /// <param name="ex"></param>
        #region static void WriteLog(Type t, Exception ex)

        public static void WriteLog(Type t, Exception ex)
        {
            log4net.ILog log = log4net.LogManager.GetLogger("RollingLogFile");
            log.Error("Error", ex);
        }
        public static void WriteClientErrorLog(string ex)
        {
            log4net.ILog log = log4net.LogManager.GetLogger("ClientErrorLog");
            log.Info(ex);
        }

        #endregion

        /// <summary>
        /// 输出日志到Log4Net
        /// </summary>
        /// <param name="t"></param>
        /// <param name="msg"></param>
        #region static void WriteLog(Type t, string msg)

        public static void WriteLog(Type t, string msg)
        {
            log4net.ILog log = log4net.LogManager.GetLogger("RollingLogFile");
            log.Info(msg);
        }

        #endregion


    }

}