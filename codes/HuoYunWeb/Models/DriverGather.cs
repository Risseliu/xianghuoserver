﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Fw.UserAttributes;

namespace HuoYunWeb.Models
{
    /// <summary>
    /// 司机统计信息
    /// </summary>
    public class DriverGather
    {
        public DriverGather()
        {
            ID = Guid.NewGuid();
        }
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        [Key, ForeignKey("Driver")]
        public Guid ID { get; set; }

        public Guid DriverID { get; set; }

        /// <summary>
        ///     Int BargainTime送货次数
        /// </summary>
        public int BargainTime { get; set; }

        /// <summary>
        ///     double BargainMoney 累计收入
        /// </summary>
        public double BargainMoney { get; set; }

        /// <summary>
        ///     double Grade货主评价
        /// </summary>
        public double Grade { get; set; }

        /// <summary>
        ///     double 新订单
        /// </summary>
        public double NewOrderCount { get; set; }

        /// <summary>
        ///     double 进行中的订单
        /// </summary>
        public double ProcessOrderCount { get; set; }




        [ForeignKey("DriverID")]
        public Driver Driver { get; set; }


    }
}