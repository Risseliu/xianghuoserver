﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Fw.UserAttributes;

namespace HuoYunWeb.Models
{
    /// <summary>
    /// 司机基本信息表
    /// </summary>
    public class Driver
    {
        private int _recivedPush;

        public Driver()
        {
//            ID = Guid.NewGuid();
            RecivedPush = 1;
        }
        
        [Key]
        [ForeignKey("User")]
        public Guid ID { get; set; }

        public int UserNumber { get; set; }

//        public Guid UserID { get; set; }

        [StringLength(200)]
        public string ShenFenZheng { get; set; }

        [StringLength(200)]
        public string JiaZhao { get; set; }

        [StringLength(200)]
        public string XingShiZheng { get; set; }

        /// <summary>
        /// 车辆正面照图片地址
        /// </summary>
        [StringLength(200)]
        public string CheLiang1 { get; set; }

        /// <summary>
        /// 车辆45度照图片地址
        /// </summary>
        [StringLength(200)]
        public string CheLiang2 { get; set; }

        /// <summary>
        /// Varchar(200) ImagePath,头像图片地址
        /// </summary>
        [StringLength(200)]
        public string ImagePath { get; set; }

        /// <summary>
        /// 车型
        /// </summary>
        [StringLength(200)]
        public string CheXing { get; set; }

        /// <summary>
        /// 车牌
        /// </summary>
        [StringLength(200)]
        public string ChePai { get; set; }

        /// <summary>
        /// 车长
        /// </summary>
        [StringLength(200)]
        public string CheChang { get; set; }

        /// <summary>
        /// 载重
        /// </summary>
        [StringLength(200)]
        public string CheWeight { get; set; }


        [StringLength(200)]
        public string Remark { get; set; }

        [ForeignKey("UserID")]
        public virtual User User { get; set; }
        public virtual ICollection<DriverLine> DriverLines { get; set; }
        public virtual ICollection<DriverRelOrder> Orders { get; set; }

        /// <summary>
        /// 是否提交验证
        /// </summary>
        public int ValidSubmit { get; set; }

        /// <summary>
        /// 是否验证
        /// </summary>
        public int Valid{ get; set; }

        /// <summary>
        /// 接受推送
        /// </summary>
        public int RecivedPush
        {
            get { return 1; }
            set { _recivedPush = value; }
        }

        public DateTime? ValidSubmitDateTime { get; set; }
    }

    public static class DriverEx
    {
        public static string GetImagePath(this Driver owner,int type)
        {
            return ImageManage.GetUserImageUrl(owner.ID, type);
        }
        public static string GetImagePath(this Customer owner,int type)
        {
            return ImageManage.GetUserImageUrl(owner.ID, type);
        }
    }
    
}