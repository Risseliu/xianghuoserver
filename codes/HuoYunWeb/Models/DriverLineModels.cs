﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuoYunWeb.Models
{
    public class ABook
    {
        [Key]
        public Guid ID { get; set; }
        public Guid UserID { get; set; }
        public string Name { get; set; }
        [ForeignKey("UserID")]
        public virtual AUser User { get; set; }
    }
//    [Flags]
    public enum UserGroup
    {
        Admin = 1,
        Guest = 2,
        CCCCC = 3,
        DDDDD = 4,
        EEEEE = 5,
    }
    public class AUser
    {
        [Key]
        public Guid ID { get; set; }
        public String Name { get; set; }
        public int Age { get; set; }
        public UserGroup UserGroup { get; set; }
        public Guid? AUserType { get; set; }
        public Guid? AUserRole { get; set; }
        public int Sex { get; set; }

        [ForeignKey("AUserType")]
        public virtual AUserType UserType { get; set; }

        [ForeignKey("AUserRole")]
        public virtual AUserRole UserRole { get; set; }

        public virtual Collection<ABook> Books { get; set; }


    }

    public class AUserType
    {
        [Key]
        public Guid ID { get; set; }
        public string Name { get; set; }
        public virtual Collection<AUser> Users { get; set; }
    }
    public class AUserRole
    {
        [Key]
        public Guid ID { get; set; }
        public string Name { get; set; }
        public virtual Collection<AUser> Users { get; set; }
    }
}