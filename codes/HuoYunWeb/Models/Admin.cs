using System;
using System.ComponentModel.DataAnnotations;
using Fw.UserAttributes;

namespace HuoYunWeb.Models
{
    /// <summary>
    /// ����Ա
    /// </summary>
    public class Admin
    {
        public Admin()
        {
            ID = Guid.NewGuid();
        }

        [LevcnColumn(DisplayName = "���", IsPrimaryKey = true)]
        [Key]
        public Guid ID { get; set; }

        [StringLength(200)]
        public string UserName { get; set; }

        [StringLength(200)]
        public string Account { get; set; }

        [StringLength(200)]
        public string Password { get; set; }

		public Guid? RoleId { get; set; }

        public int IsEnabled { get; set; }
    }
}