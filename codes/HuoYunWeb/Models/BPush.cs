﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Fw.UserAttributes;

namespace HuoYunWeb.Models
{
    /// <summary>
    /// 推送表
    /// </summary>
    public class BPush
    {
        public BPush()
        {
            ID = Guid.NewGuid();
        }
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        [Key]
        public Guid ID { get; set; }

        public Guid UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual User UserInfo { get; set; }

        public int PushCount { get; set; }
        public int Success { get; set; }

        public DateTime CreateTime { get; set; }
        public DateTime? LastPushTime { get; set; }
        public DateTime? SuccessPushTime { get; set; }



        [StringLength(300)]
        public string MessageContent { get; set; }


        /// <summary>
        /// 请求结果的字符串数据
        /// </summary>
        [StringLength(500)]
        public string PushResult { get; set; }

        

    }
}