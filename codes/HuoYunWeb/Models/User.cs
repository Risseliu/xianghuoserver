﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Fw.UserAttributes;

namespace HuoYunWeb.Models
{
    /// <summary>
    /// 所有人员基本信息表
    /// </summary>
    public class User
    {
        public User()
        {
            ID = Guid.NewGuid();
            JoinDate = DateTime.Now;
        }
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        [Key]
        public Guid ID { get; set; }

        [StringLength(200)]
        public string UserName { get; set; }

        public int UserNumber { get; set; }

        /// <summary>
        ///短信的验证码
        /// </summary>
        public string MessageValidCode { get; set; }

        [StringLength(200)]
        [Index("IX_UsreNumber", IsClustered = false, Order = 1)]
        public string PhoneNumber { get; set; }

        [StringLength(200)]
        public string PhoneID { get; set; }

        [StringLength(200)]
        public string Account { get; set; }

		[StringLength(200)]
		public string RegisterAddress { get; set; }

		[StringLength(200)]
		public string RealAddress { get; set; }
        /// <summary>
        /// 1开通货主,2开通车主,3开通货主和车主
        /// </summary>
        public int UserState { get; set; }

        [StringLength(200)]
        public string Password { get; set; } 

        /// <summary>
        /// 人员锁定,0未锁定,1已锁定
        /// </summary>
        public int Locked { get; set; }

        [StringLength(200)]
        public string PKey{ get; set; }

        public DateTime? PKeyOverDate { get; set; }

        /// <summary>
        /// Varchar(200) ImagePath,头像图片地址
        /// </summary>
        [StringLength(200)]
        public string ImagePath { get; set; }

        /// <summary>
        /// Int Gender性别,(1男,2女)
        /// </summary>
        public int Gender { get; set; }

        /// <summary>
        /// 身份证是否上传
        /// </summary>
        public int ShenFenZhengUpload { get; set; }
        /// <summary>
        /// 用户的订单报价用于车主列表接口使用
        /// </summary>
        [NotMapped]
        public int OrderQuotation { get; set; }
        /// <summary>
        /// 百度推送UserID
        /// </summary>
        [StringLength(200)]
        public string B_UserID { get; set; }
        [StringLength(200)]
        public string OpenID { get; set; }
        /// <summary>
        /// 百度推送Channel
        /// </summary>
        [StringLength(200)]
        public string B_Channel { get; set; }

        public DateTime? JoinDate{ get; set; }
        public virtual Driver DriverInfo { get; set; }

        public virtual Customer CustomerInfo { get; set; }

		/// <summary>
		/// android 3, IOS 4
		/// </summary>
		public int? DeviceType { get; set; }
    }

    public static class UserEx
    {
        /// <summary>
        /// 人员开通货主
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static User AddCustomer(this User user)
        {
            if (user.UserState == 0) user.UserState = 1;
            if (user.UserState == 2) user.UserState = 3;
            return user;
        }
        /// <summary>
        /// 人员开通车主
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static User AddDriver(this User user)
        {
            if (user.UserState == 0) user.UserState = 2;
            if (user.UserState == 1) user.UserState = 3;
            return user;
        }
        public static User FilterInfo(this User user)
        {
            user.B_UserID = null;
            user.PKey = null;
            user.PKeyOverDate = null;
//            user.UserState = 0;
            user.MessageValidCode = "";
//            user.PhoneNumber = null;
            user.PhoneID = null;
            user.B_Channel = "";
            user.B_UserID = "";
            return user;
        }
        public static Driver FilterInfo(this Driver user)
        {
            user.ShenFenZheng = null;
            user.JiaZhao = null;
            user.XingShiZheng = null;
            user.CheLiang1 = null;
            user.CheLiang2 = "";
            return user;
        }
    }

}