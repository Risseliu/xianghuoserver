﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HuoYunWeb.Models
{
	public class Route
	{
		public Route()
		{
			CreateTime = DateTime.Now;
		}

		[Key]
		public long Id{ get; set; }

		[StringLength(80)]
		public string Start { get; set; }

		[StringLength(80)]
		public string End { get; set; }

		[StringLength(100)]
		public string City { get; set; }

		[StringLength(50)]
		public string AreaCode { get; set; }

		public DateTime? CreateTime { get; set; }
	}
}