﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DAL.DB;

namespace HuoYunWeb.Models
{
    public class Area
	{
		public int Id { get; set; }

		[StringLength(50)]
		public string Code { get; set; }

		[StringLength(200)]
		public string Name { get; set; }

		[StringLength(200)]
		public string Remark { get; set; }

		public DateTime? CreateTime { get; set; }
	}

	public class AgentArea
	{
		public int Id { get; set; }

		public Guid AccountId { get; set; }

		[StringLength(50)]
		public string AreaCode { get; set; }
	}
}