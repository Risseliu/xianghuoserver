﻿using System.Data.Entity;

namespace HuoYunWeb.Models
{
    public class CommContext<T> : DbContext where T :class
    {
        public CommContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }
        public DbSet<T> Entity { get; set; }
    }
}