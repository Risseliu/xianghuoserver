﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Web.Mvc;
using log4net;

namespace HuoYunWeb.Models
{
    public class SYSContext : DbContext
    {
        public SYSContext():this(true,true)
        {
        }

        public static ActionResult Run(Func<SYSContext, ActionResult> content)
        {
            using (var a = new SYSContext())
            {
                return content(a);
            }
        }
        public static void Run1(Action<SYSContext> content)
        {
            using (var a = new SYSContext())
            {
                content(a);
            }
        }
        public SYSContext(bool log , bool lazyAndProxyEnabled = true)
            : this("DefaultConnection", log, lazyAndProxyEnabled)
        {
        }
        public SYSContext(string connstr,bool log = true, bool lazyAndProxyEnabled = true)
            : base("DefaultConnection")
        {
            if (!lazyAndProxyEnabled)
            {
                Configuration.LazyLoadingEnabled = false;
                Configuration.ProxyCreationEnabled = false;
            }
            if (log) Database.Log = writeLog;

        }
        static ILog log = LogManager.GetLogger("SqlLog");
        void writeLog(string logStr)
        {
            log.Info(logStr);
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Properties<string>().Configure(z => z.HasMaxLength(200));
//            modelBuilder.Conventions.Add<DefaultTableConvention>();
            //添加表前缀
//            modelBuilder.Types().Configure(entity => entity.ToTable("Shop_" + entity.ClrType.Name));

            //默认给外键加索引,可以移除
//            modelBuilder.Conventions.Remove<ForeignKeyIndexConvention>();  
            base.OnModelCreating(modelBuilder);
//            modelBuilder.Entity<Driver>().HasRequired(p => p.UserInfo).WithOptional(p => p.DriverInfo);
//            modelBuilder.Ignore<DriverLine>()
//                .Ignore<Driver>()
//                .Ignore<User>()
//                .Ignore<Customer>()
//                .Ignore<Order>()
//                .Ignore<Admin>()
//                .Ignore<DriverRelOrder>();
        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<AgentArea> AgentAreas { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<DriverRelOrder> DriverRelOrders { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<DriverGather> DriverGathers { get; set; }
        public DbSet<DriverLine> DriverLines { get; set; }
        public DbSet<DriverRelOrderDetail> DriverRelOrderDetails { get; set; }

        public DbSet<AUser> AUsers { get; set; }
        public DbSet<AUserType> AUserTypes{ get; set; }
        public DbSet<BPush> BPushs { get; set; }

		public DbSet<Route> Routes { get; set; }
		public DbSet<DriverRoute> DriverRoutes { get; set; }
    }
    
}