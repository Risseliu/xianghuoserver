﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using DotNetOpenAuth.OpenId.Extensions.SimpleRegistration;
using Fw.UserAttributes;
/*
 
Delete From DriverRelOrderDetails
delete from DriverRelOrders
delete from ORders
delete from DriverLines
delete from Drivers
delete from Customers
delete from users
 
*/
//todo 根据订单取状态 输入状态
//http://124.193.154.36:8051/Customer/AddNewOrder/
//http://124.193.154.36:8051/Customer/Register?
//http://124.193.154.36:8051/Customer/AddNewOrder?phoneId1=&phoneNumber=123456789&password=123456&driver={"UserNumber":123}&orderNumber=123456789&orderStates=0,1,2,3,-2&pKey=123&line={"DriverID":"bb483f1b-04f7-4d59-a00c-e64005296bbe","StartCityName":"%E5%8C%97%E4%BA%AC","StartName":"%E5%85%86%E4%B8%B0%E5%AE%B6%E5%9B%AD+%E8%A5%BF%E5%8C%BA"}&order={"StartName":"%E5%85%86%E4%B8%B0%E5%AE%B6%E5%9B%AD+%E8%A5%BF%E5%8C%BA","EndName":"%E5%85%86%E4%B8%B0%E5%AE%B6%E5%9B%AD+%E8%A5%BF%E5%8C%BA","Lading":"asdfasdd"}
//UpdateUserInfo
//可以取所有人的基本信息.头像.
//通过地址名,取经纬度
//发布后,地址//验证其它属性为空时,是否会删除
//更新数据库 Update-Database -Force -Verbose
//生成更新数据库脚本 Update-Database -Script
// Add-Migration User_IsDeleted -IgnoreChanges 从原有数据库中改为CodeFirst,需要添加-IgnoreChanges 参数
//生成更新类文件 Add-Migration AddPostClass
//更新到指定版本 Update-Database -TargetMigration 201412281107039_AddPostClass_AutomaticMigration


//要做的事情
//删除线路
//生动人员ID
namespace HuoYunWeb.Models
{ 
    /// <summary>
    ///     司机订单关联表
    /// </summary> 
    public class DriverRelOrder
    {
        public DriverRelOrder()
        {
            ID = Guid.NewGuid();
            
        }
//        [NotMapped]
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        [Key]
//        [ForeignKey("Order")]
        public Guid ID { get; set; }

        /// <summary>
        ///     司机ID
        /// </summary>
        public Guid DriverID { get; set; }

        /// <summary>
        /// Int OrderState 状态(0新订单,1已接受,2货主选中车主,3车主已接货,4发货中,5完成,6已评价,7货主已发货(货主确定把货给车主),-1错过,-2不接受,-3没被选中,-4(1)已接受后取消,-5(2)货主选中车主后取消)
        /// 0 1 2 7 3 4 5 6
        /// </summary>
        public int OrderState { get; set; }

        /// <summary>
        /// 0未被货主确定,1货主已选定,2确定后订单取消
        /// </summary>
        public int EnsureState { get; set; }

        /// <summary>
        ///     GUID OrderID 订单ID
        /// </summary>
        public Guid OrderID { get; set; }
        /// <summary>
        /// 订单报价
        /// </summary>
        public int OrderQuotation { get; set; }
//        [InverseProperty("DriverRelOrders")]
       [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }

//        [InverseProperty("Orders")]
        [ForeignKey("DriverID")]
        public virtual Driver Driver { get; set; }

        public virtual ICollection<DriverRelOrderDetail> DriverRelOrderDetails { get; set; }
    }

    public static class DriverRelOrderEx
    {
        public static void AddDetail(this DriverRelOrder owner, int OrderState, string userName, int changedUser, string phoneNumber, int gender)
        {
            if (owner.DriverRelOrderDetails == null)
                owner.DriverRelOrderDetails = new Collection<DriverRelOrderDetail>();
            owner.DriverRelOrderDetails.Add(
                new DriverRelOrderDetail
                {
                    OrderState = OrderState,
                    UserName = userName,
                    Gender = gender,
                    UserPhoneNumber = phoneNumber,
                    ChangedUser = changedUser,
                });
            
        }
    }
    /// <summary>
    /// 订单执行日志
    /// </summary> 
    public class DriverRelOrderDetail
    {
        public DriverRelOrderDetail()
        {
            ID = Guid.NewGuid();
            AtThatTime = DateTime.Now;
        }

        [Key]
        public Guid ID { get; set; }

        public int OrderState { get; set; }
        /// <summary>
        /// 货主1,车主2,系统3
        /// </summary>
        public int ChangedUser { get; set; }
        public string UserName { get; set; }
        public string UserPhoneNumber { get; set; }
        public DateTime? AtThatTime { get; set; }
        public Guid DriverRelOrderID { get; set; }

        /// <summary>
        /// Int Gender性别,(1男,2女)
        /// </summary>
        public int Gender { get; set; }

        [ForeignKey("DriverRelOrderID")]
        public DriverRelOrder DriverRelOrder { get; set; }
    }
}