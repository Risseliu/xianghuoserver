﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HuoYunWeb.Models
{
	public class StatisticArea
	{
		public string Area { get; set; }
		public long SendCount { get; set; }
		public long ReceiveCount { get; set; }
	    public string Route { get; set; }
	}

	public class Activity
	{
		public string UserID { get; set; }
		public string UserName { get; set; }
		public string PhoneNumber { get; set; }
		public int ActivityCount { get; set; }
	}

	public class IntrouduceGroupCount
	{
		public string GroupName { get; set; }
		public int IntroduceCount { get; set; }
	}

	public class IntrouducePersonCount
	{
		public string UserName { get; set; }
		public string PhoneNumber { get; set; }
		public int IntroduceCount { get; set; }
	}

	public class AreaTrendCount
	{
		public string Date { get; set; }
		public int SendCount { get; set; }
		public int ReceiveCount { get; set; }
	}
}