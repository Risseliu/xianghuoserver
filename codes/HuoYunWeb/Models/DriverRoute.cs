﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HuoYunWeb.Models
{
	public class DriverRoute
	{
		[Key]
		public long Id { get; set; }

		public long RouteId { get; set; }

		public Guid DriverId { get; set; }
	}
}