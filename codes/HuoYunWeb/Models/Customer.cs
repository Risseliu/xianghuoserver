﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Fw.UserAttributes;

namespace HuoYunWeb.Models
{
    /// <summary>
    ///     货主信息表
    /// </summary>
    public class Customer
    {
        public Customer()
        {
            ID = Guid.NewGuid();
        }
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        [Key, ForeignKey("UserInfo")]
        public Guid ID { get; set; }

        public int Valid { get; set; }

        public Guid UserID { get; set; }
        /// <summary>
        /// 身份证是否上传
        /// </summary>
        public int ShenFenZhengUpload { get; set; }

        [StringLength(200)]
        public string ShenFenZheng { get; set; }

        [StringLength(500)]
        public string Remark { get; set; }

		public Guid? IntroducePersonId { get; set; }

        /// <summary>
        /// 是否提交验证
        /// </summary>
        public int ValidSubmit { get; set; }
        
        public DateTime? ValidSubmitDateTime { get; set; }

        [ForeignKey("UserID")]
        public virtual User UserInfo { get; set; }
    }
    /// <summary>
    ///     货主统计信息表
    /// </summary>
    public class CustomerGather
    {
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        [Key, ForeignKey("UserInfo")]
        public Guid ID { get; set; }

        public int Valid { get; set; }
        public Guid UserID { get; set; }
       
        [ForeignKey("UserID")]
        public virtual User UserInfo { get; set; }

//        public int 
    }
}