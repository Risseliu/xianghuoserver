﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HuoYunWeb.Models
{
	public class IntroduceGroup
	{
		public Guid ID { get; set; }

		  [StringLength(200)]
		public string GroupName { get; set; }

		public DateTime? CreateTime { get; set; }
	}

	public class IntroducePerson
	{
		public Guid ID { get; set; }

		[StringLength(200)]
		public string UserName { get; set; }

		[StringLength(200)]
		public string PhoneNumber { get; set; }

		public int Gender { get; set; }

		public Guid GroupId { get; set; }

		public DateTime? CreateTime { get; set; }
	}
}