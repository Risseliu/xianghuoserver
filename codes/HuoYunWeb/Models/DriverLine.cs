﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HuoYunWeb.Models
{
    /// <summary>
    /// 司机行驶路线
    /// </summary>
    public class DriverLine
    {
        public DriverLine()
        {
            ID = Guid.NewGuid();
            PublishTime = DateTime.Now;
        }
        [Key]
        public Guid ID { get; set; }
        public Guid DriverID { get; set; }
        [StringLength(300)]
        public string StartCityName { get; set; }
        [StringLength(300)]
        public string StartName { get; set; }
        public double StartPointX { get; set; }
        public double StartPointY { get; set; }
        [StringLength(300)]
        public string EndCityName { get; set; }
        [StringLength(300)]
        public string EndName { get; set; }
        public double EndPointX { get; set; }
        public double EndPointY { get; set; }
        public DateTime? PublishTime { get; set; }
        /// <summary>
        /// 往返
        /// </summary>
        public int ReturnAble { get; set; }
        /// <summary>
        /// 是否启使用
        /// </summary>
        public int IsEnabled { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int OrderID { get; set; }

        [ForeignKey("DriverID")]
        public Driver Driver { get; set; }
        
    }
}