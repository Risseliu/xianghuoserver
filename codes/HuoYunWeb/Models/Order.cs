﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DAL.DB;
using Fw.UserAttributes;

namespace HuoYunWeb.Models
{
    /// <summary>
    ///     订单表
    /// </summary>
    public class Order : BaseEntity
    {
        public Order()
        {
            ID = Guid.NewGuid();
        }
        [LevcnColumn(DisplayName = "编号", IsPrimaryKey = true)]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 订单号
        /// </summary>
        [StringLength(200)]
        public string OrderNumber { get; set; }

        /// <summary>
        /// 开始地点
        /// </summary>
        [StringLength(200)]
        public string StartName { get; set; }
        [StringLength(300)]
        public string StartCityName { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public int IsEnabled { get; set; }
        public double StartPointX { get; set; }
        public double StartPointY { get; set; }
        public double EndPointX { get; set; }
        public double EndPointY { get; set; }
        /// <summary>
        /// -6货主取消
        /// </summary>
        public int OrderState { get; set; }

        /// <summary>
        /// 订单是否被取消
        /// </summary>
        public int Canceled { get; set; }
        public DateTime? CancelDateTime { get; set; }

        [StringLength(200)]
        public string EndName { get; set; }

        [StringLength(300)]
        public string EndCityName { get; set; }

        /// <summary>
        /// 生成为5位数字,只有货主知道,送到货到,
        /// 货主把这个密码发给收货人,收货人告诉车主,车主输入后,确定收货,完成交易
        /// </summary>
        [StringLength(200)]
        public string PKey { get; set; }

        /// <summary>
        ///     Varchar(200) Lading 货物名
        /// </summary>
        [StringLength(200)]
        public string Lading { get; set; }

        /// <summary>
        ///     Varchar(200) Weight重量
        /// </summary>
        [StringLength(200)]
        public string Weight { get; set; }

        /// <summary>
        /// 货主是否评价
        /// </summary>
        public int GuestAppraised { get; set; }

        /// <summary>
        /// 车主是否评价
        /// </summary>
        public int DriverAppraised { get; set; }

        /// <summary>
        /// 失效时间
        /// </summary>
        public DateTime? OverTime { get; set; }
        public DateTime? PublishTime { get; set; }

        /// <summary>
        /// 开始送货时间
        /// </summary>
        public DateTime? DeliverTimeStart { get; set; }
        /// <summary>
        /// 开始送货时间2
        /// </summary>
        public DateTime? DeliverTimeStart2 { get; set; }

        /// <summary>
        /// 送到货的时间
        /// </summary>
        public DateTime? DeliverTimeEnd { get; set; }

        /// <summary>
        /// 送到货的时间2
        /// </summary>
        public DateTime? DeliverTimeEnd2 { get; set; }

//        [IgnoreDataMember]
        [ForeignKey("GuestUser")]

        public Guid GuestUserID { get; set; }
//        [Column("GuestUserID")]
//        public Guid UserID { get; set; }

        public string GuestUserPhoneNumber { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// 接收人
        /// </summary>
        [StringLength(200)]
        public string ReciveUserName { get; set; }

        [StringLength(200)]
        public string ReciveUserPhoneNumber { get; set; }

        /// <summary>
        ///  PingJia1Score 货主对司机的评分
        /// </summary>
        public double PingJia1Score { get; set; }

        /// <summary>
        ///  PingJia1_2指定地点接送(货主对司机的)
        /// </summary>
        public int PingJia1_2 { get; set; }

        /// <summary>
        ///  PingJia1_3货物完好无损(货主对司机的)
        /// </summary>
        public int PingJia1_3 { get; set; }

        /// <summary>
        ///  PingJia1_4规定时间送到(货主对司机的)
        /// </summary>
        public int PingJia1_4 { get; set; }

        /// <summary>
        ///  PingJia1Content 评价内容(货主对司机的)
        /// </summary>
        public string PingJia1Content { get; set; }


        /// <summary>
        ///  PingJia2Score司机对货主的评分
        /// </summary>
        public double PingJia2Score { get; set; }

        /// <summary>
        /// PingJia2Content 评价内容(司机对货主的)
        /// </summary>
        [StringLength(200)]
        public string PingJia2Content { get; set; }

        /// <summary>
        ///  OrderRemark(货主编写的备注);
        /// </summary>
        [StringLength(200)]
        public string OrderRemark { get; set; }
        /// <summary>
        ///  OrderRemark(货物照片);
        /// </summary>
        [StringLength(200)]
        public string OrderImagePath { get; set; }
        [StringLength(200)]
        public string GoodImage { get; set; }


        public DateTime? OrderSendtime { get; set; }

		public long? RouteId { get; set; }

		[StringLength(50)]
		public string AreaCode { get; set; }

		//下单类型订单 1：手机端 2：微信端 3：电话下单
		public int? From { get; set; }
        /// <summary>
        /// 运输价格是否面议 1是 0 否
        /// </summary>
        public int IsMianYi { get; set; }
        public virtual Collection<DriverRelOrder> DriverRelOrders { get; set; }

        [ForeignKey("GuestUserID")]
        public virtual User GuestUser { get; set; }

	
    }
    public static class OrderEx
    {
        public static string GetImagePath(this Order owner, int type)
        {
            return ImageManage.GetUserImageUrl(owner.ID, type);
        }
    }
}