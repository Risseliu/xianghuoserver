﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using DAL.DB;
using EntityFramework.Extensions;
using Fw.Entity;
using Fw.Reflection;
using Fw.Extends;

namespace DAL.Control
{
//    [Authorization]
    public class BaseControllerx<T> : Controller where T :  BaseEntity, new()
    {

        //
        // GET: /Material/
        public ActionResult Create(Guid? id = null)
        {
            using (var a = new CommContext<T>())
            {
                T sMaterial = new T {};
                sMaterial = NewAfter(sMaterial);
                if (id.HasValue && id != Guid.Empty)
                {
                    sMaterial = a.Entity.FirstOrDefault(w => w.ID == id);
                    if (object.Equals(sMaterial, null))
                    {
                        sMaterial = new T();
                    }
                }
                return View(sMaterial);
            }
        }
        protected virtual T NewAfter(T t)
        {
            return t;
        }
        protected virtual T PostSaveAfter(T t)
        {
            return t;
        }
        [HttpPost]
        public JsonResult Create(T ma)
        {

            ma = PostSaveAfter(ma);
            using (var a = new CommContext<T>())
            {
                if (ma.ID != Guid.Empty)
                {
                    var r = a.Entity.FirstOrDefault(w => w.ID == ma.ID);
                    if (r != null)
                    {
                        ma = UpdateBefore(ma);
                        ReflectionHelper.SetBaseAttributeValues(r, ma,new []{"ID"});
                    }
                }
                else
                {
                    ma.ID = Guid.NewGuid();
                    a.Entity.Add(ma);
                }
                a.SaveChanges();
                return getDefaultJsonResult();
            }
        }

        protected virtual T UpdateBefore(T ma)
        {
            return ma;
        }

        public ActionResult Index(
                int pageNum = 0,
                int numPerPage = 20,
                string orderField = "",
                string status = "")
        {
            var p =  GetPagedList(pageNum, numPerPage, orderField);
            p = ListViewAfter(p);
            return View(p);
        }
        /// <summary>
        /// 通用分页方法
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="pageNum"></param>
        /// <param name="numPerPage"></param>
        /// <param name="orderField"></param>
        /// <returns></returns>
        protected ListPageModel<T> GetPagedList(int pageNum, int numPerPage, string orderField)
        {
            PageInfo pi = new PageInfo();
            pi.PageIndex = pageNum;
            if (pi.PageIndex <= 0) pi.PageIndex = 1;
            pi.PageSize = numPerPage;

            if (pi.PageSize <= 1) pi.PageSize = 1;
            using (var a = new CommContext<T>())
            {
                var iq = GetIQ(a.Entity);
                var count = iq.FutureCount();
                var list = iq.Skip((pi.PageIndex - 1) * pi.PageSize).Take(pi.PageSize).Future();

                pi.EndRecord = count.Value;
                pi.TotalPage = (count + numPerPage - 1)/numPerPage;
                var listPageModel = new ListPageModel<T> {List = list.ToList(), Page = pi};
                return listPageModel;
            }

        }

        protected virtual List<SearchEntry> GetSearchEntity(List<SearchEntry> ses)
        {
            return ses;
        }

        protected virtual ListPageModel<T> ListViewAfter(ListPageModel<T> t)
        {
            return t;
        }
        
        #region 排序

        public IQueryable<T> GetOrderBy(IQueryable<T> data,
            OrderEntity[] orderByExpression)
        {
            //创建表达式变量参数
            var parameter = Expression.Parameter(typeof(T), "o");

            if (orderByExpression != null && orderByExpression.Length > 0)
            {
                for (int i = 0; i < orderByExpression.Length; i++)
                {
                    //根据属性名获取属性
                    var property = typeof(T).GetProperty(orderByExpression[i].PropName);
                    //创建一个访问属性的表达式
                    var propertyAccess = Expression.MakeMemberAccess(parameter, property);
                    var orderByExp = Expression.Lambda(propertyAccess, parameter);

                    string OrderName = "";
                    if (i > 0)
                    {
                        OrderName = orderByExpression[i].OrderType == OrderType.Asc ? "ThenByDescending" : "ThenBy";
                    }
                    else
                        OrderName = orderByExpression[i].OrderType == OrderType.Desc ? "OrderByDescending" : "OrderBy";

                    MethodCallExpression resultExp = Expression.Call(typeof(Queryable), OrderName, new[] { typeof(T), property.PropertyType },
                  data.Expression, Expression.Quote(orderByExp));

                    data = data.Provider.CreateQuery<T>(resultExp);
                }
            }
            return data;
        }
        #endregion
        #region 搜索条件

        /// <summary>
        /// 返回基本属性列表的搜索条件
        /// </summary>
        /// <param name="getBaseTypeProperty"></param>
        /// <param name="uiElement"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public  IEnumerable<SearchEntry> GetSearchEntryByBaseProperty(List<PropertyInfo> getBaseTypeProperty, string tableName)
        {
            return getBaseTypeProperty.Select(w => GetGetSearchEntryItem(w,  tableName)).Where(w => w != null).ToList();
        }
        private  SearchEntry GetGetSearchEntryItem(PropertyInfo propertyInfo, string tableName)
        {
            SearchEntry re = null;
            var val = GetUIElementValue(propertyInfo.Name) ?? "";
            val = val.Replace("'", "").Replace(";", "");
            if (!string.IsNullOrEmpty(val))
            {
                re = new SearchEntry
                {
                    ColumnName = tableName + "." + ReflectionHelper.GetPropertyField(propertyInfo),
                    Flag = "=",
                    value = val
                };
                if (propertyInfo.PropertyType == typeof(string) || propertyInfo.PropertyType == typeof(Guid) ||
                    propertyInfo.PropertyType == typeof(Guid?))
                {
                    re.value = string.Format("'{0}'", re.value);
                }

            }
            val = GetUIElementValue( propertyInfo.Name + "_Like");
            if (!string.IsNullOrEmpty(val))
            {
                re = new SearchEntry
                {
                    ColumnName = tableName + "." + ReflectionHelper.GetPropertyField(propertyInfo),
                    Flag = "=",
                    value = val
                };
                if (propertyInfo.PropertyType == typeof(string))
                {
                    re.Flag = "like";
                    re.value = string.Format("'%{0}%'", re.value);
                }
            }
            val = GetUIElementValue( propertyInfo.Name + "_Start");
            if (!string.IsNullOrEmpty(val))
            {
                re = new SearchEntry
                {
                    ColumnName = ReflectionHelper.GetPropertyField(propertyInfo),
                    Flag = ">=",
                    value = val
                };

            }
            val = GetUIElementValue(propertyInfo.Name + "_End");
            if (!string.IsNullOrEmpty(val))
            {
                re = new SearchEntry
                {
                    ColumnName = ReflectionHelper.GetPropertyField(propertyInfo),
                    Flag = "<=",
                    value = val
                };
            }
            if (re != null && propertyInfo.PropertyType == typeof(DateTime))
            {
                re.value = string.Format("'{0}'", re.value);
            }
            val = GetUIElementValue(propertyInfo.Name + "_In");
            if (!string.IsNullOrEmpty(val))
            {
                var values = val.Split(',');
                re = new SearchEntry
                {
                    ColumnName = ReflectionHelper.GetPropertyField(propertyInfo),
                    Flag = "in",
                    //value = val
                };
                if (propertyInfo.PropertyType == typeof(string) || propertyInfo.PropertyType == typeof(DateTime))
                {
                    re.value = string.Format("({0})", values.Select(w => string.Format("'{0}'", w)).ToList().Serialize(","));
                }
            }
            return re;
        }
        private string GetUIElementValue( string name)
        {
            return Request[name] ?? "";
        }
        #endregion
        public JsonResult Delete(Guid id)
        {
            using (var a = new CommContext<T>())
            {
                var item = a.Entity.FirstOrDefault(w => w.ID == id);
                if (item != null)
                {
                    a.Entity.Remove(item);
                    a.SaveChanges();
                }
                return getDefaultJsonResult(callbackType: "123");
            }
//            var c = RouteData.GetController();
//            return new JsonResult { Data = new { statusCode = 200, message = "保存成功", callbackType = "closeCurrent1", navTabId = c } };
        }

        JsonResult getDefaultJsonResult(string navTabId = null,string callbackType = null)
        {
            var c = navTabId ?? Request["NT"] ?? RouteData.GetController();
            return new JsonResult
            {
                Data = new {statusCode = 200, 
                    message = "保存成功", 
                    callbackType = callbackType ?? "closeCurrent", 
                    navTabId = c}
            };
        }
        #region aaa

        public IQueryable<T> GetIQ<T>(IQueryable<T> custs)
        {
            var keys = Request.Form.AllKeys.Concat(Request.QueryString.AllKeys);
            var ps = ReflectionHelper.GetBaseTypeProperty<T>(PropertyType.BaseType);
            foreach (PropertyInfo p in ps)
            {
                Type propertyType = p.PropertyType;
                string s = Request[p.Name];
                if (s != null)
                {

                    if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                    {
                        custs = GetDateTimeEq(custs, p, s);
                    }
                    else if (propertyType == typeof(string))
                    {
                        custs = GetStringEq(custs, p, s);
                    }
                    else if (propertyType == typeof(byte) || propertyType == typeof(byte?) ||
                             propertyType == typeof(short) || propertyType == typeof(short?) ||
                             propertyType == typeof(ushort) || propertyType == typeof(ushort?) ||
                             propertyType == typeof(int) || propertyType == typeof(int?) ||
                             propertyType == typeof(uint) || propertyType == typeof(uint?) ||
                             propertyType == typeof(long) || propertyType == typeof(long?) ||
                             propertyType == typeof(ulong) || propertyType == typeof(ulong?) ||
                             propertyType == typeof(double) || propertyType == typeof(double?) ||
                             propertyType == typeof(float) || propertyType == typeof(float?)
                        )
                    {
                        custs = GetNumberEq(custs, p, s);
                    }
                    else if (propertyType == typeof(bool) || propertyType == typeof(bool?))
                    {
                        custs = GetBoolEq(custs, p, s);
                    }
                    //                    else if (propertyType.IsPrimitive)
                    //                    {
                    //                        custs = GetNumberEq(custs, p, s);
                    //                    }
                    //                    else if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>) && propertyType.GetGenericArguments()[0].IsPrimitive)
                    //                    {
                    //                        custs = GetNumberEq(custs, p, s);
                    //                    }
                }
                else
                {
                    s = Request[p.Name + "_Like"];
                    if (s != null && p.PropertyType == typeof(string))
                    {
                        custs = GetStringLike(custs, p, s);
                    }
                    else
                    {
                        s = Request[p.Name + "_Start"];
                        if (s != null)
                        {
                            if ((propertyType == typeof(byte) || propertyType == typeof(byte?) ||
                                 propertyType == typeof(short) || propertyType == typeof(short?) ||
                                 propertyType == typeof(ushort) || propertyType == typeof(ushort?) ||
                                 propertyType == typeof(int) || propertyType == typeof(int?) ||
                                 propertyType == typeof(uint) || propertyType == typeof(uint?) ||
                                 propertyType == typeof(long) || propertyType == typeof(long?) ||
                                 propertyType == typeof(ulong) || propertyType == typeof(ulong?) ||
                                 propertyType == typeof(double) || propertyType == typeof(double?) ||
                                 propertyType == typeof(float) || propertyType == typeof(float?)
                                ))
                            {
                                custs = GetNumberDaYu(custs, p, s);
                            }
                            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                            {
                                custs = GetDateTimeDaYu(custs, p, s);
                            }
                        }
                        s = Request[p.Name + "_End"];
                        if (s != null)
                        {
                            if ((propertyType == typeof(byte) || propertyType == typeof(byte?) ||
                                 propertyType == typeof(short) || propertyType == typeof(short?) ||
                                 propertyType == typeof(ushort) || propertyType == typeof(ushort?) ||
                                 propertyType == typeof(int) || propertyType == typeof(int?) ||
                                 propertyType == typeof(uint) || propertyType == typeof(uint?) ||
                                 propertyType == typeof(long) || propertyType == typeof(long?) ||
                                 propertyType == typeof(ulong) || propertyType == typeof(ulong?) ||
                                 propertyType == typeof(double) || propertyType == typeof(double?) ||
                                 propertyType == typeof(float) || propertyType == typeof(float?)
                                ))
                            {
                                custs = GetNumberXiaoYu(custs, p, s);
                            }
                            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                            {
                                custs = GetDateTimeXiaoYu(custs, p, s);
                            }
                        }
                    }
                }
            }

            return custs;
        }

        private static IQueryable<T> GetStringEq<T>(IQueryable<T> custs, PropertyInfo p, string s)
        {
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param,
                typeof(T).GetProperty(p.Name));
            Expression right = Expression.Constant(s);
            Expression filter = Expression.Equal(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        private static IQueryable<T> GetDateTimeEq<T>(IQueryable<T> custs, PropertyInfo p, string s)
        {
            var time = s.ToDateTime();
            if (!time.HasValue) return custs;
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param,
                typeof(T).GetProperty(p.Name));
            Expression right = Expression.Constant(p.PropertyType == typeof(DateTime) ? time.Value : time);
            Expression filter = Expression.Equal(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        private static IQueryable<T> GetDateTimeDaYu<T>(IQueryable<T> custs, PropertyInfo p, string s)
        {
            var time = (DateTime?)s.ToDateTime();
            if (!time.HasValue) return custs;
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param,
                typeof(T).GetProperty(p.Name));
            //            if (p.PropertyType == typeof (DateTime?))
            //            {
            //                NullableConverter nullableConverter = new NullableConverter(p.PropertyType);
            //                nullableConverter.;
            //            }
            //
            //            object changeType = Convert.ChangeType(time,p.PropertyType);
            object value = p.PropertyType == typeof(DateTime) ? time.Value : time;
            Expression right = Expression.Constant(value, p.PropertyType);
            Expression filter = Expression.GreaterThanOrEqual(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        private static IQueryable<T> GetDateTimeXiaoYu<T>(IQueryable<T> custs, PropertyInfo p, string s)
        {
            var time = s.ToDateTime();
            if (!time.HasValue) return custs;
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param,
                typeof(T).GetProperty(p.Name));
            Expression right = Expression.Constant(p.PropertyType == typeof(DateTime) ? time.Value : time, p.PropertyType);
            Expression filter = Expression.LessThanOrEqual(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        private static IQueryable<T> GetNumberEq<T>(IQueryable<T> custs, PropertyInfo p, string ss)
        {
            var s = ss.ToDoublt();
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param, typeof(T).GetProperty(p.Name));
            var sss = Convert.ChangeType(s, p.PropertyType);
            Expression right = Expression.Constant(sss);
            Expression filter = Expression.Equal(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        private static IQueryable<T> GetNumberDaYu<T>(IQueryable<T> custs, PropertyInfo p, string ss)
        {
            var s = ss.ToDoublt();
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param, typeof(T).GetProperty(p.Name));
            var sss = Convert.ChangeType(s, p.PropertyType);
            Expression right = Expression.Constant(sss);
            Expression filter = Expression.GreaterThanOrEqual(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        private static IQueryable<T> GetNumberXiaoYu<T>(IQueryable<T> custs, PropertyInfo p, string ss)
        {
            var s = ss.ToDoublt();
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param, typeof(T).GetProperty(p.Name));
            var sss = Convert.ChangeType(s, p.PropertyType);
            Expression right = Expression.Constant(sss);
            Expression filter = Expression.LessThanOrEqual(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        private static IQueryable<T> GetBoolEq<T>(IQueryable<T> custs, PropertyInfo p, string ss)
        {
            var s = "true".Equals(ss, StringComparison.OrdinalIgnoreCase);
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param, typeof(T).GetProperty(p.Name));
            var sss = Convert.ChangeType(s, p.PropertyType);
            Expression right = Expression.Constant(sss);
            Expression filter = Expression.Equal(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        private static IQueryable<T> GetStringLike<T>(IQueryable<T> custs, PropertyInfo p, string s)
        {
            //创建一个参数c
            ParameterExpression param = Expression.Parameter(typeof(T), "c");
            //c.City=="London"
            Expression left = Expression.Property(param,
                typeof(T).GetProperty(p.Name));
            Expression right = Expression.Constant(s);
            Expression filter = Expression.Equal(left, right);
            Expression pred = Expression.Lambda(filter, param);
            //Where(c=>c.City=="London")
            Expression expr = Expression.Call(typeof(Queryable), "Where",
                new[] { typeof(T) },
                Expression.Constant(custs), pred);
            //生成动态查询
            IQueryProvider queryProvider = custs.Provider;
            custs = queryProvider.CreateQuery<T>(expr);
            return custs;
        }
        #endregion
    }
    public class ListPageModel<T>
    {
        public List<T> List { get; set; }
        public PageInfo Page { get; set; }
        public object Tag1 { get; set; }
    }
    public static class ViewContextEx
    {
        public static string GetController(this ViewContext owner)
        {
            return owner.RouteData.Values["controller"].ToString();
        }
        public static string GetAction(this ViewContext owner)
        {
            return owner.RouteData.Values["action"].ToString();
        }
        public static string GetControllerAndAction(this ViewContext owner)
        {
            return owner.RouteData.Values["controller"] + "/" + owner.RouteData.Values["action"];
        }
    }
    public static class RouteDataEx
    {
        public static string GetController(this RouteData owner)
        {
            return owner.Values["controller"].ToString();
        }
    }

    public class OrderEntity
    {
        public string PropName { get; set; }
        public OrderType OrderType { get; set; }
    }

    public enum OrderType
    {
        Desc,
        Asc,
    }
}
