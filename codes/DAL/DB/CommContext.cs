﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DB
{
    public class CommContext<T> : DbContext where T : class
    {
        public static string connstr = "DefaultConnection";
        public CommContext()
            : this(connstr)
        {
        }

        public CommContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }
        public DbSet<T> Entity { get; set; }
    }
    public interface IEntity
    {
        Guid ID { get; set; }
    }

    public class BaseEntity : IEntity
    {
        [Key]
        public Guid ID { get; set; }
    }
}
