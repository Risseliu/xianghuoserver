USE[tqtest]

if Not exists(select * from syscolumns where id=object_id('Admins') and name='RoleId') 
ALTER TABLE [Admins] ADD  RoleId uniqueidentifier null

if Not exists(select * from syscolumns where id=object_id('Routes') and name='AreaCode') 
ALTER TABLE [Routes] ADD  AreaCode nvarchar(50) null

if Not exists(select * from syscolumns where id=object_id('Orders') and name='AreaCode') 
ALTER TABLE [Orders] ADD  AreaCode nvarchar(50) null

if Not exists(select * from syscolumns where id=object_id('Orders') and name='From') 
ALTER TABLE [Orders] ADD  [From] int null
if Not exists(select * from syscolumns where id=object_id('Orders') and name='RouteId') 
ALTER TABLE [Orders] ADD  RouteId bigint null

if Not exists(select * from syscolumns where id=object_id('Users') and name='RegisterAddress') 
ALTER TABLE Users ADD  RegisterAddress nvarchar(200) null

if Not exists(select * from syscolumns where id=object_id('Users') and name='RealAddress') 
ALTER TABLE Users ADD  RealAddress nvarchar(200) null


IF EXISTS ( select * FROM  sysobjects WHERE name = 'Area' and type = 'U')
DROP TABLE Area
CREATE TABLE [dbo].[Area](
	[Id] [int] NOT NULL,
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Remark] [nvarchar](200) NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_Area] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


IF EXISTS ( select * FROM  sysobjects WHERE name = 'AgentArea' and type = 'U')
DROP TABLE AgentArea
CREATE TABLE [dbo].[AgentArea](
	[Id] [int] NOT NULL,
	[ACCOUNTID] [uniqueidentifier] NOT NULL,
	[AreaCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_AgentArea] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



IF EXISTS ( select * FROM  sysobjects WHERE name = 'IntroducePerson' and type = 'U')
DROP TABLE IntroducePerson
CREATE TABLE [IntroducePerson](
	[ID] [uniqueidentifier] NOT NULL DEFAULT ,
	[UserName] [nvarchar](200) NULL,
	[PhoneNumber] [nvarchar](200) NULL,
	[Gender] [int] NOT NULL,
	[GroupId]  [uniqueidentifier] NULL,
	[CreateTime] [datetime] NULL,
) ON [PRIMARY]
ALTER TABLE [dbo].[IntroducePerson] ADD  CONSTRAINT [DF_IntroducePerson_ID]  DEFAULT (newid()) FOR [ID]

IF EXISTS ( select * FROM  sysobjects WHERE name = 'IntroduceGroup' and type = 'U')
DROP TABLE IntroduceGroup
CREATE TABLE [IntroduceGroup](
	[ID] [uniqueidentifier] NOT NULL,
	[GroupName] [nvarchar](200) NULL,
	[CreateTime] [datetime] NULL,
) ON [PRIMARY]

ALTER TABLE [IntroduceGroup] ADD  CONSTRAINT [DF_IntroduceGroup_ID]  DEFAULT (newid()) FOR [ID]